package com.example.android.day4task3.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.example.android.day4task3.fragments.Fragment1;
import com.example.android.day4task3.fragments.Fragment2;
import com.example.android.day4task3.fragments.Fragment3;
import com.example.android.day4task3.fragments.Fragment4;
import com.example.android.day4task3.fragments.Fragment5;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

    }



    @Override
    public Fragment getItem(int position) {
        switch(position) {

             case 0:return new  Fragment1(position);
            case 1: return new  Fragment2(position);
            case 2: return new  Fragment3(position);
            case 3: return new  Fragment4(position);
            case 4: return new  Fragment5(position);

            default: return null;
        }
    }


    @Override
    public int getCount() {
        return 5;
    }


}