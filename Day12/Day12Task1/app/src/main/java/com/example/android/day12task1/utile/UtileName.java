package com.example.android.day12task1.utile;

import java.io.File;

/**
 * Created by android on 11.10.15.
 */
public class UtileName {
    public static String KEY_DEBTOR_ID = "DebtorId";
    public static String KEY_CREDIT_ID = "CreditId";
    public static String SET_TYPE = "image/*";
    public static String RETURN_DATE = "return-data";
    public static String DATE_FORMAT = "dd/MM/yyyy";
    public static String TYPE_CONTACTS = "content://contacts";
    public final static String KEY_IS_DIALOG_SHOW = "isDialogShow";
    public final static String KEY_CHOICE = "isChoice";
}
