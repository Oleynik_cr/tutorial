package com.example.android.day12task1.dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentManager;
import com.example.android.day12task1.R;
import com.example.android.day12task1.fragments.OnHeadlineSelectedListener;
import com.example.android.day12task1.fragments.list_fragment.DebtorListFragment;
import com.example.android.day12task1.network.SynchronizationUpLoadService;
import com.example.android.day12task1.utile.UtileName;
import com.github.lzyzsd.circleprogress.DonutProgress;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class DialogSynchronization extends DialogFragment {

    @Bind(R.id.donut_progress)
    DonutProgress donutProgress;
    private SynchronizationUpLoadService synchronizationUpLoadService;
    private boolean mBound = false;
    private boolean ischoice;
    private OnHeadlineSelectedListener onHeadlineSelectedListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            Intent intent = new Intent(getActivity(), SynchronizationUpLoadService.class);
            getActivity().bindService(intent, connection, getActivity().BIND_AUTO_CREATE);
            ischoice = getArguments().getBoolean(UtileName.KEY_CHOICE);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        getDialog().setTitle("Attention");
        View view = inflater.inflate(R.layout.dialog_get_synchronization, container, false);
        ButterKnife.bind(this, view);
        donutProgress.setMax(100);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }
    public void closeDialog() {
        dismissAllowingStateLoss();
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mBound) {
            getActivity().unbindService(connection);
            mBound = false;
            onHeadlineSelectedListener.updateListDebtor();
        }
    }
    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            SynchronizationUpLoadService.LocalBinder binder = (SynchronizationUpLoadService.LocalBinder) service;
            synchronizationUpLoadService = binder.getService();
            synchronizationUpLoadService.setLocalBinding(DialogSynchronization.this);
            mBound = true;
            synchronizationUpLoadService.startSynchronization(ischoice);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public void setUpdate(int update){
        donutProgress.setProgress(update);
    }
    public void setMaxProgres(int i) {
        donutProgress.setMax(i);

    }
    public void failed(){
        closeDialog();
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(synchronizationUpLoadService.getString(R.string.oops))
                .setContentText(synchronizationUpLoadService.getString(R.string.something_went_wrong))
                .show();
    }
    public static DialogSynchronization newInstance(boolean ischoice) {
        DialogSynchronization fragment = new DialogSynchronization();
        Bundle args = new Bundle();
        args.putBoolean(UtileName.KEY_CHOICE,ischoice);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener = null;
    }

}
