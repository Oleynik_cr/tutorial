package com.example.android.day12task1.fragments.list_fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.android.day12task1.R;
import com.example.android.day12task1.adaptor.AbstractListAdaptor;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;

import butterknife.Bind;


public abstract class AbstractListFragment extends Fragment implements SearchView.OnQueryTextListener {
    protected AbstractListAdaptor adapter;
    protected SwingRightInAnimationAdapter animationAdapter;
    @Bind(R.id.tvToolbarTitle)
    TextView tvToolbarTitle;

    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getContext().getResources().getDisplayMetrics());
    }
    public abstract void upDateList();
    public abstract void confirmationDelete();

    @Override
    public void onResume() {
        super.onResume();
        upDateList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("search");
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.filter(newText);
        return false;
    }
}
