package com.example.android.day12task1.active_dao.qwery;

import android.os.AsyncTask;
import android.widget.ListView;

import com.activeandroid.Model;
import com.activeandroid.query.From;
import com.example.android.day12task1.adaptor.AbstractListAdaptor;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;

import java.util.List;


public class AsyncSelectAll<T extends Model> extends AsyncTask<From, Void, List<T>> {
     AbstractListAdaptor adapter;
    SwingRightInAnimationAdapter animationAdapter;
      ListView listView;

    public AsyncSelectAll(AbstractListAdaptor adapter, ListView listView, SwingRightInAnimationAdapter animationAdapter) {
        this.adapter = adapter;
        this.listView = listView;
        this.animationAdapter = animationAdapter;
    }
    @Override
    protected List<T> doInBackground(From... params) {
        return params[0].execute();
    }
    @Override
    protected void onPostExecute(List<T> items) {
        adapter.setDate(items);
        listView.setAdapter(animationAdapter);
    }
}

