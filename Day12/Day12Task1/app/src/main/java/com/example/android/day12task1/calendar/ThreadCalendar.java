package com.example.android.day12task1.calendar;

import android.content.Context;

import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;


public class ThreadCalendar implements Runnable {

    public static final String UPDATE_CALENDAR ="update";
    public static final String DELETE_CALENDAR ="delete";
    public static final String CREATE_CALENDAR ="create";
    private Context context;
    private String  choice;
    private Calendar calendar;
    private DebtorTable debtorTable;
    private CreditTable creditTable;

    public ThreadCalendar(Context context, String choice, CreditTable creditTable) {
        this.context = context;
        this.choice = choice;
        this.creditTable = creditTable;
        calendar = new Calendar(context);
    }

    public ThreadCalendar(Context context, String choice, DebtorTable debtorTable, CreditTable creditTable) {
        this.context = context;
        this.choice = choice;
        this.debtorTable = debtorTable;
        this.creditTable = creditTable;
        calendar = new Calendar(context);
    }

    @Override
    public void run() {
        switch (choice){
            case CREATE_CALENDAR:{
             calendar.createRecordInCalendar(creditTable, debtorTable);
                break;
            }
            case UPDATE_CALENDAR:{
                calendar.updateRecordInCalendar(creditTable, debtorTable);
                break;
            }
            case DELETE_CALENDAR:{
                calendar.deleteRecordInCalendar(creditTable.getCalendarId());
                break;
            }
            default:
                break;
        }
    }
}
