package com.example.android.day12task1.fragments.operations_on_debtor;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;


public class CreateDebtorFragment extends AbstractDebtor implements DatePickerDialog.OnDateSetListener {

    @Bind(R.id.etDateFrom)
    EditText etDateFrom;
    @Bind(R.id.etDateTo)
    EditText etDateTo;
    @Bind(R.id.etMoney)
    EditText etMoney;
    @Bind(R.id.ivStuff)
    ImageView ivStuff;
    @Bind(R.id.sChoice)
    Spinner sChoice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_debtor, container, false);
        setRetainInstance(true);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbar.setTitle(getString(R.string.create_debtor));
        setHasOptionsMenu(true);
        animateSample(layoutInformation, floatingActionButton, collapsingToolbar);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.material_background);
        header.setImageBitmap(bitmap);
        String[] list = getResources().getStringArray(R.array.choice);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, list);
        sChoice.setAdapter(adapter);
        sChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch (position) {
                    case 0:
                        etMoney.setInputType(InputType.TYPE_CLASS_NUMBER);
                        etMoney.setHint(R.string.amount_of_money);
                        ivStuff.setVisibility(View.GONE);
                        isChoisCredit = true;
                        break;
                    case 1:
                        etMoney.setInputType(InputType.TYPE_CLASS_TEXT);
                        etMoney.setHint(R.string.input_name_things);
                        ivStuff.setVisibility(View.VISIBLE);
                        isChoisCredit = false;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (savedInstanceState != null) {
            isDialogShowed = savedInstanceState.getBoolean(UtileName.KEY_IS_DIALOG_SHOW);
            if (isDialogShowed)
                imagePickerDialog(getString(R.string.photo_picker), getString(R.string.choise_a_photo),
                        WorkWithPhoto.GALLERY_PICTURE, WorkWithPhoto.CAMERA_PICTURE);
        }

        return view;
    }

    @OnClick(R.id.etDateFrom)
    void getDateFrom(View view) {
        if (view.getId() == R.id.etDateFrom) {
            getDate();
            etDateTo.setVisibility(View.VISIBLE);
            isChoise = true;
        }
    }
    @OnClick(R.id.etDateTo)
    void getDateTo(View view) {
        if (view.getId() == R.id.etDateTo) {
            getDate();
            isChoise = false;
        }
    }
    @OnClick(R.id.bAddDebtor)
    void addDebtor(View view) {
        if (view.getId() == R.id.bAddDebtor) {
            createDebtor();
        }
    }
    @OnClick(R.id.bGetPhoto)
    void getPhoto(View view) {
        if (view.getId() == R.id.bGetPhoto) {
            imagePickerDialog(getString(R.string.photo_picker), getString(R.string.choise_a_photo),
                    WorkWithPhoto.GALLERY_PICTURE, WorkWithPhoto.CAMERA_PICTURE);
        }
    }
    @OnClick(R.id.bGetContact)
    void getContact(View view) {
        if (view.getId() == R.id.bGetContact) {
            pickContact();
        }
    }
    @OnClick(R.id.ivStuff)
    void addImageStuff(View view) {
        if (view.getId() == R.id.ivStuff) {
            imagePickerDialog(getString(R.string.photo_picker), getString(R.string.you_can_choose_a_photo_pledge)
                    , WorkWithPhoto.GALLERY_PICTURE_FROM_STAFF, WorkWithPhoto.CAMERA_PICTURE_FROM_STAFF);
        }
    }
    private void getDate() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                CreateDebtorFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(true);
        dpd.vibrate(true);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_debtor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            case R.id.action_create:
                if (validate()) {
                    createDebtor();
                    WritePhoneContact(etFirstName.getText().toString()+" "+etLastName.getText()
                            .toString(),etPhone.getText().toString(), etEmail.getText().toString());
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }
    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        simpleDateFormat = new SimpleDateFormat(UtileName.DATE_FORMAT);
        date = dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        try {
            if(isChoise) {
                dateFrom = simpleDateFormat.parse(date);
                etDateFrom.setText(date);
            }else{
                dateTo = simpleDateFormat.parse(date);
                if(dateTo.before(dateFrom) || dateTo.equals(dateFrom))
                    etDateTo.setText(getString(R.string.not_correct_date));
                else
                    etDateTo.setText(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String firstName = etFirstName.getText().toString();
        String LastName = etLastName.getText().toString();
        String money = etMoney.getText().toString();
        String dateFrom = etDateFrom.getText().toString();
        String dateTo = etDateTo.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.enter_a_valid_email));
            valid = false;
        } else {
            etEmail.setError(null);
        }
        if (phone.length() < 6 || phone.length() > 13) {
            etPhone.setError(getString(R.string.between_6_and_12_numbers));
            valid = false;
        } else {
            etPhone.setError(null);
        }
        if (firstName.equals("")) {
            etFirstName.setError(getString(R.string.plase_enter_first_name));
            valid = false;
        } else {
            etFirstName.setError(null);
        }
        if (LastName.equals("")) {
            etLastName.setError(getString(R.string.plase_enter_last_name));
            valid = false;
        } else {
            etLastName.setError(null);
        }
        if (money.equals("")) {
            etMoney.setError(getString(R.string.please_enter_amount_of_money));
            valid = false;
        } else {
            etMoney.setError(null);
        }
        if (dateFrom.equals("")) {
            etDateFrom.setError(getString(R.string.plase_enter_date));
            valid = false;
        } else {
            etDateFrom.setError(null);
        }
        if (dateTo.equals("") || (dateTo.equals(getString(R.string.not_correct_date)))) {
            etDateTo.setError(getString(R.string.plase_enter_date));
            valid = false;
        } else {
            etDateTo.setError(null);
        }
        if (srcImageAvatar == null) {
            Toast.makeText(getContext(), R.string.input_photo, Toast.LENGTH_LONG).show();
            valid = false;
        }
        return valid;
    }

    public void createDebtor() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        DebtorTable debtorTable = null;
        CreditTable creditTable = null;
        try {
            debtorTable = new DebtorTable();
            debtorTable.setFirstName(etFirstName.getText().toString());
            debtorTable.setLastName(etLastName.getText().toString());
            debtorTable.setEmail(etEmail.getText().toString());
            debtorTable.setPhone(etPhone.getText().toString());
            debtorTable.setSrcPhoto(srcImageAvatar);
            debtorTable.setParseId("0");
            debtorTable.save();
            creditTable  = new CreditTable();
            creditTable.setDebtorTable(debtorTable);
            creditTable.setDateStartСredit(simpleDateFormat.parse(etDateFrom.getText().toString()));
            creditTable.setDateEndСredit((simpleDateFormat.parse(etDateTo.getText().toString())));
            if(isChoisCredit) {
                creditTable.setAmountOfMoney(Integer.parseInt(etMoney.getText().toString()));
            }else {
                creditTable.setStuff(etMoney.getText().toString());
                if(isPhotoStaff)
                    creditTable.setPhotoStaff(srcImageStaff);
            }
            creditTable.save();
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            ThreadCalendar threadCalendar = new ThreadCalendar(getContext(), ThreadCalendar.CREATE_CALENDAR,
                    debtorTable, creditTable);
            executorService.execute(threadCalendar);
            successfullyDialog(getString(R.string.you_create_debtor));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case WorkWithPhoto.GALLERY_PICTURE:
                    setImageInHead(imageSelecting(data));
                    break;
                case WorkWithPhoto.CAMERA_PICTURE:
                    setImageInHead(imageSelecting(data));
                    break;
                case WorkWithPhoto.PICK_CONTACT_REQUEST:
                    getContact(data);
                    break;
                case WorkWithPhoto.CAMERA_PICTURE_FROM_STAFF:
                    setImageInStaff(imageSelecting(data));
                    break;
                case WorkWithPhoto.GALLERY_PICTURE_FROM_STAFF:
                    setImageInStaff(imageSelecting(data));
                    break;
                default:
                    break;
            }
        }
    }

    private void setImageInStaff(File file) {
        Bitmap bitmap = WorkWithPhoto.decodeFile(file);
        String validPhoto = file.toString();
        srcImageStaff = validPhoto;
        isPhotoStaff = true;
        bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
        ivStuff.setImageBitmap(bitmap);
        ivStuff.setRotation(WorkWithPhoto.getExifOrientation(validPhoto));
    }
}


