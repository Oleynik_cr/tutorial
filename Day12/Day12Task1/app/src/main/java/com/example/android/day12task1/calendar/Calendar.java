package com.example.android.day12task1.calendar;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by android on 26.10.15.
 */
public class Calendar  {
    private  static final String TIMEZONE = "America/Los_Angeles";
    long calID = 3;
    long startMillis = 0;
    long endMillis = 0;
    private Context context;

    public Calendar(Context context) {
        this.context = context;
    }

    public void createRecordInCalendar(CreditTable creditTable, DebtorTable debtorTable){
        java.util.Calendar beginTime = java.util.Calendar.getInstance();
        beginTime.setTime(creditTable.getDateEndСredit());
        startMillis = beginTime.getTimeInMillis();
        java.util.Calendar endTime = java.util.Calendar.getInstance();
        endTime.setTime(creditTable.getDateEndСredit());
        endMillis = endTime.getTimeInMillis();

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, context.getString(R.string.overdue_a_credit));
        if(creditTable.getStuff() == null) {
            values.put(CalendarContract.Events.DESCRIPTION, debtorTable.getFirstName() + " " +
                    debtorTable.getLastName() + "\n" + context.getString(R.string.amount_of_credit) + creditTable.getAmountOfMoney());
        }else{
            values.put(CalendarContract.Events.DESCRIPTION, debtorTable.getFirstName() + " " +
                    debtorTable.getLastName() + "\n" + context.getString(R.string.credit_name) + creditTable.getStuff());
        }
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TIMEZONE);
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        long eventID = Long.parseLong(uri.getLastPathSegment());
        creditTable.setCalendarId(eventID);
        creditTable.save();
    }

    public void deleteRecordInCalendar(Long eventID){
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        Uri deleteUri = null;
        deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
        context.getContentResolver().delete(deleteUri, null, null);
    }

    public void updateRecordInCalendar(CreditTable creditTable, DebtorTable debtorTable){
        Uri updateUri = null;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, context.getString(R.string.overdue_a_credit));
        if(creditTable.getStuff() == null) {
            values.put(CalendarContract.Events.DESCRIPTION, debtorTable.getFirstName() + " " +
                    debtorTable.getLastName() + "\n" + context.getString(R.string.amount_of_credit) + creditTable.getAmountOfMoney());
        }else{
            values.put(CalendarContract.Events.DESCRIPTION, debtorTable.getFirstName() + " " +
                    debtorTable.getLastName() + "\n" + context.getString(R.string.credit_name) + creditTable.getStuff());
        }
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        updateUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, creditTable.getCalendarId());
         context.getContentResolver().update(updateUri, values, null, null);
    }
}
