package com.example.android.day12task1.fragments.operations_on_debtor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.qwery.Query;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.fragments.list_fragment.CreditListFragment;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by android on 12.10.15.
 */
public class DataInformationDebtorFragment extends AbstractDebtor {

    private DialogFragment mMenuDialogFragment;
    DebtorTable debtorTable;
    @Bind(R.id.tvCredits)
    TextView tvCredits;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        id = bundle.getLong(UtileName.KEY_DEBTOR_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detal_information_debtor, container, false);
        setRetainInstance(true);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbar.setTitle(getString(R.string.update_debtor));
        setHasOptionsMenu(true);
        setIformationInFields();
        animateSample(layoutInformation, floatingActionButton, collapsingToolbar);
        if (savedInstanceState != null) {
            isDialogShowed = savedInstanceState.getBoolean(UtileName.KEY_IS_DIALOG_SHOW);
            if (isDialogShowed)
                imagePickerDialog(getString(R.string.photo_picker), getString(R.string.choise_a_photo),
                        WorkWithPhoto.GALLERY_PICTURE, WorkWithPhoto.CAMERA_PICTURE);
        }
        //  initMenuFragment();
        return view;
    }

    private void setIformationInFields() {
        debtorTable = Query.getDebtor(id);
        etFirstName.setText(debtorTable.getFirstName());
        etLastName.setText(debtorTable.getLastName());
        etPhone.setText(debtorTable.getPhone());
        etEmail.setText(debtorTable.getEmail());
        setImageInHead(new File(debtorTable.getSrcPhoto()));
        tvCredits.setText(getString(R.string.sum_of_credits) + Query.getCountCredit(id));
    }

    @Override
    public boolean validate() {
        boolean valid = true;
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String firstName = etFirstName.getText().toString();
        String LastName = etLastName.getText().toString();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.enter_a_valid_email));
            valid = false;
        } else {
            etEmail.setError(null);
        }
        if (phone.length() < 6 || phone.length() > 13) {
            etPhone.setError(getString(R.string.between_6_and_12_numbers));
            valid = false;
        } else {
            etPhone.setError(null);
        }
        if (firstName.equals("")) {
            etFirstName.setError(getString(R.string.plase_enter_first_name));
            valid = false;
        } else {
            etFirstName.setError(null);
        }
        if (LastName.equals("")) {
            etLastName.setError(getString(R.string.plase_enter_last_name));
            valid = false;
        } else {
            etLastName.setError(null);
        } if (srcImageAvatar == null) {
            Toast.makeText(getContext(), R.string.input_photo, Toast.LENGTH_LONG).show();
            valid = false;
        }
        return valid;
    }

    @OnClick(R.id.bGetPhoto)
    void getPhoto(View view) {
        if (view.getId() == R.id.bGetPhoto) {
            imagePickerDialog(getString(R.string.photo_picker), getString(R.string.choise_a_photo),
                    WorkWithPhoto.GALLERY_PICTURE, WorkWithPhoto.CAMERA_PICTURE);
        }
    }
    @OnClick(R.id.bGetContact)
    void getContacts(View view) {
        if (view.getId() == R.id.bGetContact) {
            pickContact();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case WorkWithPhoto.GALLERY_PICTURE:
                    setImageInHead(imageSelecting(data));
                    break;
                case WorkWithPhoto.CAMERA_PICTURE:
                    setImageInHead(imageSelecting(data));
                    break;
                case WorkWithPhoto.PICK_CONTACT_REQUEST:
                    getContact(data);
                    break;
                default:
                    break;
            }
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            case R.id.action_update:
                updateDebtor();
                return true;
            case R.id.action_show_credit:
                CreditListFragment creditListFragment = new CreditListFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(UtileName.KEY_DEBTOR_ID, id);
                creditListFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.frame, creditListFragment)
                        .addToBackStack(null).commit();
                return true;
//            case R.id.context_menu:
//                mMenuDialogFragment.show(getFragmentManager(), ContextMenuDialogFragment.TAG);
//                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateDebtor() {
        debtorTable.setFirstName(etFirstName.getText().toString());
        debtorTable.setLastName(etLastName.getText().toString());
        debtorTable.setEmail(etEmail.getText().toString());
        debtorTable.setPhone(etPhone.getText().toString());
        debtorTable.setSrcPhoto(srcImageAvatar);
        if(!debtorTable.getParseId().equals("0")){
            debtorTable.setIsUpdate(true);
        }
        debtorTable.save();
        updateCalendar(debtorTable);
        successfullyDialog(getString(R.string.you_update_debtor));
    }

    private void updateCalendar(DebtorTable debtorTable) {
        ArrayList<CreditTable> creditTableList = (ArrayList<CreditTable>)
                Query.getCreditList(debtorTable.getId());
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ThreadCalendar threadCalendar;
        for (CreditTable creditTable: creditTableList){
            threadCalendar = new ThreadCalendar(getContext(),
                    ThreadCalendar.UPDATE_CALENDAR, debtorTable, creditTable);
            executorService.execute(threadCalendar);
        }
    }

//    private List<MenuObject> getMenuObjects() {
//        List<MenuObject> menuObjects = new ArrayList<>();
//
//        MenuObject close = new MenuObject();
//        close.setResource(R.drawable.cross);
//
//        MenuObject update = new MenuObject("Update");
//        update.setResource(R.drawable.update);
//
//        MenuObject show = new MenuObject("Show credit detail");
//        show.setResource(R.drawable.show);
//
//        menuObjects.add(close);
//        menuObjects.add(update);
//        menuObjects.add(show);
//        return menuObjects;
//    }
//    private void initMenuFragment() {
//        MenuParams menuParams = new MenuParams();
//        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
//        menuParams.setMenuObjects(getMenuObjects());
//        menuParams.setClosableOutside(false);
//        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
//    }
}
