package com.example.android.day12task1.fragments.operations_on_credit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.eftimoff.androidplayer.Player;
import com.eftimoff.androidplayer.actions.property.PropertyAction;
import com.example.android.day12task1.R;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import cn.pedant.SweetAlert.SweetAlertDialog;

public abstract class AbstractCredits extends Fragment implements DatePickerDialog.OnDateSetListener{

    @Bind(R.id.etDateFrom)
    EditText etDateFrom;
    @Bind(R.id.etDateTo)
    EditText etDateTo;
    @Bind(R.id.etMoney)
    EditText etMoney;
    @Bind(R.id.sChoice)
    Spinner sChoice;
    @Bind(R.id.ivStuff)
    ImageView ivStuff;
    @Bind(R.id.llStaff)
    LinearLayout llstaff;
    @Bind(R.id.ivCalendar)
    ImageView ivCalendar;
    @Bind(R.id.ivMoney)
    ImageView ivMoney;
    @Bind(R.id.ivCamera)
    ImageView ivCamera;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    protected boolean isDialogShowed;
    protected AlertDialog alertDialog;
    protected boolean isChoise = true;
    protected String srcImageStaff;
    protected SimpleDateFormat simpleDateFormat;
    protected boolean isChoisCredit;
    protected boolean isPhotoStaff;
    protected Date dateFrom;
    protected Date dateTo;
    protected String date;
    protected long id;

    public void setImageInStaff(File file) {
        Bitmap bitmap = WorkWithPhoto.decodeFile(file);
        String validPhoto = file.toString();
        srcImageStaff = validPhoto;
        isPhotoStaff = true;
        bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
        ivStuff.setImageBitmap(bitmap);
        ivStuff.setRotation(WorkWithPhoto.getExifOrientation(validPhoto));
    }

    public void getDate() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                AbstractCredits.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(true);
        dpd.vibrate(true);
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null)
            dpd.setOnDateSetListener((DatePickerDialog.OnDateSetListener) getActivity());
    }
    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        simpleDateFormat = new SimpleDateFormat(UtileName.DATE_FORMAT);
        date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        try {
            if (isChoise) {
                dateFrom = simpleDateFormat.parse(date);
                etDateFrom.setText(date);
            } else {
                dateTo = simpleDateFormat.parse(date);
                if (dateTo.before(dateFrom) || dateTo.equals(dateFrom))
                    etDateTo.setText(getString(R.string.not_correct_date));
                else
                    etDateTo.setText(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String money = etMoney.getText().toString();
        String dateFrom = etDateFrom.getText().toString();
        String dateTo = etDateTo.getText().toString();

        if (money.equals("")) {
            etMoney.setError(getString(R.string.please_enter_amount_of_money));
            valid = false;
        } else {
            etMoney.setError(null);
        }
        if (dateFrom.equals("")) {
            etDateFrom.setError(getString(R.string.plase_enter_date));
            valid = false;
        } else {
            etDateFrom.setError(null);
        }
        if (dateTo.equals("") && (dateTo.equals("Not correct date"))) {
            etDateTo.setError(getString(R.string.plase_enter_date));
            valid = false;
        } else {
            etDateFrom.setError(null);
        }
        return valid;
    }

    public void imagePickerDialog(String title, String message, final int requestCodeForGallery, final int requestCodeForCamera) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(getString(R.string.gallery), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setType(UtileName.SET_TYPE);
                intent.putExtra(UtileName.RETURN_DATE, true);
                startActivityForResult(intent, requestCodeForGallery);
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.camera), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, requestCodeForCamera);
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public File imageSelecting(Intent data) {
        try {
            WorkWithPhoto.uri = data.getData();
            if (WorkWithPhoto.uri != null) {
                Cursor cursor = getActivity().getContentResolver().query(WorkWithPhoto.uri, new String[]
                        {android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                cursor.moveToFirst();
                final String imageFilePath = cursor.getString(0);
                WorkWithPhoto.DefaultDIR = new File(imageFilePath);
                WorkWithPhoto.CreateImagesDir();
                WorkWithPhoto.copyFile(WorkWithPhoto.DefaultDIR, WorkWithPhoto.imgDir);
                cursor.close();
            } else {
                Toast toast = Toast.makeText(getActivity(), getString(R.string.selected_any_image), Toast.LENGTH_LONG);
                toast.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WorkWithPhoto.PasteTargetLocation;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == WorkWithPhoto.CAMERA_PICTURE_FROM_STAFF) {
                setImageInStaff(imageSelecting(data));
            } else if (requestCode == WorkWithPhoto.GALLERY_PICTURE_FROM_STAFF) {
                setImageInStaff(imageSelecting(data));
            }
        }
    }
    public void animate(View activityDateFrom,View activityDateTo,View activityMoney
            ,View activityIconDate, View activityIconMoney, View activityIconCamera
            ,View activityIconStaff) {
        PropertyAction activityFrom = PropertyAction.newPropertyAction(activityDateFrom)
                .scaleX(0).scaleY(0).duration(550).alpha(0f).build();
        PropertyAction activityTo = PropertyAction.newPropertyAction(activityDateTo)
                .scaleX(0).scaleY(0).duration(550).alpha(0f).build();
        PropertyAction activityMone = PropertyAction.newPropertyAction(activityMoney)
                .scaleX(0).scaleY(0).duration(550).alpha(0f).build();
        PropertyAction activityiConStaff = PropertyAction.newPropertyAction(activityIconStaff)
                .scaleX(0).scaleY(0).duration(550).alpha(0f).build();
        PropertyAction iconDate = PropertyAction.newPropertyAction(activityIconDate)
                .scaleX(0).scaleY(0).duration(750).alpha(0f).build();
        PropertyAction iconMoney = PropertyAction.newPropertyAction(activityIconMoney)
                .scaleX(0).scaleY(0).duration(750).alpha(0f).build();
        PropertyAction iconCamera = PropertyAction.newPropertyAction(activityIconCamera)
                .scaleX(0).scaleY(0).duration(750).alpha(0f).build();
        Player.init().
                animate(iconDate).
                animate(iconMoney).
                animate(iconCamera).
                animate(activityiConStaff).
                then().
                animate(activityFrom).
                animate(activityTo).
                animate(activityMone).
                play();
    }
    public void successfullyDialog(String contentText){
        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.good_job))
                .setContentText(contentText)
                .show();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_credit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(UtileName.KEY_IS_DIALOG_SHOW, alertDialog != null && alertDialog.isShowing());
        super.onSaveInstanceState(outState);
    }

    public void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }
}
