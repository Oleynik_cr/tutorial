package com.example.android.day12task1.active_dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

@Table(name = "debtors", id = "_id")
public class DebtorTable extends Model {

    @Column(name = "firstName")
    private String firstName;
    @Column(name ="lastName")
    private String lastName;
    @Column(name = "srcPhoto")
    private String srcPhoto;
    @Column(name = "email")
    private String email;
    @Column(name = "userEmail")
    private String userEmail;
    @Column(name = "phone")
    private String phone;
    @Column(name = "parseId")
    private String parseId;
    @Column(name = "isUpdate")
    private Boolean isUpdate;

    public DebtorTable (){super();}

    public DebtorTable(String firstName, String lastName, String srcPhoto, String email, String userEmail, String phone,String parseId, Boolean isUpdate) {
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.srcPhoto = srcPhoto;
        this.email = email;
        this.userEmail = userEmail;
        this.parseId = parseId;
        this.isUpdate = isUpdate;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSrcPhoto() {
        return srcPhoto;
    }

    public void setSrcPhoto(String srcPhoto) {
        this.srcPhoto = srcPhoto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getParseId() {
        return parseId;
    }

    public void setParseId(String parseId) {
        this.parseId = parseId;
    }

    public Boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Boolean isUpdate) {
        this.isUpdate = isUpdate;
    }
}
