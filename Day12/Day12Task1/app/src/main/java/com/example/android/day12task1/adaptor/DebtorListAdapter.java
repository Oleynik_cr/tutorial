package com.example.android.day12task1.adaptor;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.qwery.Query;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.Bind;


public class DebtorListAdapter extends AbstractListAdaptor<DebtorTable> {
    private List<DebtorTable> data;

    public DebtorListAdapter(Context context, List<DebtorTable> mData) {
        super(context,0, mData);
        data = mData;
    }

    static class ViewHolder {
        @Bind(R.id.tvFirstName)
        TextView tvFirstName;
        @Bind(R.id.tvLastName)
        TextView tvLastName;
        @Bind(R.id.imageView)
        ImageView imageView;
        @Bind(R.id.tvExpired)
        TextView tvExpired;
        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_debtor, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        DebtorTable debtorTable = getItem(position);
        if(debtorTable != null) {
            if(getCheckedCredit(debtorTable))
                viewHolder.tvExpired.setText(R.string.overdue_payment);
            else
                viewHolder.tvExpired.setText("");
            viewHolder.tvFirstName.setText(debtorTable.getFirstName());
            viewHolder.tvLastName.setText(debtorTable.getLastName());
            Picasso.with(getContext())
                    .load(new File(debtorTable.getSrcPhoto()))
                    .resize(125, 125)
                    .error(R.drawable.logo)
                    .centerCrop()
                    .into(viewHolder.imageView);
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public void  setDate(List<DebtorTable> items){
        data.clear();
        searchList.clear();
        data.addAll(items);
        searchList.addAll(items);
    }

    @Override
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(searchList);
        } else {
            for (DebtorTable debtorTable : searchList) {
                if (debtorTable.getFirstName().toLowerCase(Locale.getDefault())
                        .contains(charText)|| debtorTable.getLastName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    data.add(debtorTable);
                }
            }
        }
        notifyDataSetChanged();
    }
    private  boolean getCheckedCredit(DebtorTable debtorTable){
        boolean status = false;
        ArrayList<CreditTable> creditList = (ArrayList<CreditTable>)
                Query.getCreditList(debtorTable.getId());
        for (CreditTable creditTable: creditList){
            if(cheсkExpiredCredit(creditTable.getDateEndСredit()))
                status = true;
        }
        return status;
    }
}






