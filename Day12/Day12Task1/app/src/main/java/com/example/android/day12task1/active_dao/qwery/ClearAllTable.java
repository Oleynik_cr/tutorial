package com.example.android.day12task1.active_dao.qwery;


import android.content.Context;

import com.activeandroid.query.Delete;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.InformationAboutDeleteRows;
import com.example.android.day12task1.calendar.Calendar;
import com.example.android.day12task1.utile.WorkWithPhoto;

import java.util.AbstractList;

public class ClearAllTable implements Runnable {
    Context context;

    public ClearAllTable(Context context) {
        this.context = context;
    }

    @Override
    public void run() {

        clearCalendar();
        WorkWithPhoto.deleteDirectory();
        new Delete().from(DebtorTable.class).execute();
        new Delete().from(CreditTable.class).execute();
        new Delete().from(InformationAboutDeleteRows.class).execute();
    }

    private void clearCalendar() {
        Calendar calendar = new Calendar(context);
        AbstractList<CreditTable> creditTablesList = (AbstractList<CreditTable>) Query.getAllCredit();
        for (CreditTable creditTable: creditTablesList)
            calendar.deleteRecordInCalendar(creditTable.getCalendarId());
    }
}
