package com.example.android.day12task1.registration;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.day12task1.MainActivity;
import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.qwery.ClearAllTable;
import com.example.android.day12task1.fragments.OnHeadlineSelectedListener;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks ,GoogleApiClient.OnConnectionFailedListener {

    @Bind(R.id.etEmaill)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.bLogin)
    Button blogin;
    @Bind(R.id.bgoogle)
    Button bgoogle;
    @Bind(R.id.bTwitter)
    Button bTwitte;
    @Bind(R.id.bFacebook)
    Button bFacebook;
    @Bind(R.id.tvSingnupLink)
    TextView tvSingnupLink;
    private TwitterAuthClient authClient;
    private GoogleApiClient mGoogleApiClient;
    private TwitterAuthClient mTwitterAuthClient;
    private static int G_PLUS = 23;
    public static final String SECOND_IN = "secondIn";
    public static final String PROFILE_FACEBOOK = "public_profile";
    public static final String USER_FACEBOOK = "user_friends";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mTwitterAuthClient = new TwitterAuthClient();
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bgoogle)
    void getGoogle(View view) {
        if (view.getId() == R.id.bgoogle)
            if(WorkWithPhoto.isConnected(this)){
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(Plus.API)
                        .addScope(new Scope(Scopes.PROFILE))
                        .addScope(new Scope(Scopes.EMAIL))
                        .build();
                mGoogleApiClient.connect();
        }
    }


    @OnClick(R.id.bFacebook)
    void getFacebook(View view) {
        if (view.getId() == R.id.bFacebook) {
            if(WorkWithPhoto.isConnected(this))
                autorizationFaceBook();
        }
    }


    @OnClick(R.id.bTwitter)
    void getTwitter(View view) {
        if (view.getId() == R.id.bTwitter) {
            if(WorkWithPhoto.isConnected(this))
                autorizationTwitter();
                mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        autorizationTwitter();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                    }
                });
        }
    }

    @OnClick(R.id.tvSingnupLink)
    void singnupLink(View view) {
        if (view.getId() == R.id.tvSingnupLink) {
            startActivity(new Intent(getApplication(), SingUpActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    @OnClick(R.id.bLogin)
    void login(View view) {
        if (view.getId() == R.id.bLogin)
            if(WorkWithPhoto.isConnected(this)){
                singIn(etEmail.getText().toString(), etPassword.getText().toString());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        if(requestCode == G_PLUS) {
            mGoogleApiClient.connect();
            return;
        }
        mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        if (authClient != null)
            authClient.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String personName = currentPerson.getDisplayName();
            String personGooglePlusProfile = currentPerson.getId();
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
            autorizationGoogle(email, personGooglePlusProfile);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    private void autorizationFaceBook() {
        Collection<String> permissions = Arrays.asList(PROFILE_FACEBOOK, USER_FACEBOOK);
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                } else if (user.isNew()) {
                    firstIn();
                } else {
                    secondIn();
                }
            }
        });
    }
    private void autorizationTwitter() {
        ParseTwitterUtils.logIn(this, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                } else if (user.isNew()) {
                    firstIn();
                } else {
                    secondIn();
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
        try {
            if(connectionResult.hasResolution())
                connectionResult.startResolutionForResult(this, G_PLUS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }
    private void firstIn(){
        startActivity(new Intent(getApplication(), MainActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }
    private void secondIn(){
        WorkWithPhoto.CreateImagesDir();
        Intent intent = new Intent(getApplication(),MainActivity.class);
        intent.putExtra(SECOND_IN, true);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }

    private void singIn(String email, String password){
        ParseUser.logInInBackground(email, password,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            secondIn();
                            Toast.makeText(getApplication(),
                                    R.string.successfully_logged_in,
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(
                                    getApplication(),
                                    R.string.no_such_user_exist,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void autorizationGoogle(final String email, final String password){
        ParseUser.logInInBackground(email,password,
                new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            secondIn();
                            Toast.makeText(getApplication(),
                                    R.string.successfully_logged_in,
                                    Toast.LENGTH_LONG).show();
                        } else {
                            ParseUser parseUser = new ParseUser();
                            parseUser.setUsername(email);
                            parseUser.setPassword(password);
                            parseUser.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        firstIn();
                                    } else {
                                        Toast.makeText(getApplication(),
                                                R.string.sign_up_error, Toast.LENGTH_LONG)
                                                .show();
                                    }
                                }
                            });
                        }
                    }
                });
    }
}
