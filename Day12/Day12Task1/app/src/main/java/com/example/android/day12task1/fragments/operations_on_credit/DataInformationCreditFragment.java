package com.example.android.day12task1.fragments.operations_on_credit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.qwery.Query;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.ButterKnife;
import butterknife.Bind;
import butterknife.OnClick;


public class DataInformationCreditFragment extends AbstractCredits {

    CreditTable creditTable;
    @Bind(R.id.sChoice)
    Spinner sChoice;
    @Bind(R.id.bAddCredit)
    Button bUpdate;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    private long debtorId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        id = bundle.getLong(UtileName.KEY_CREDIT_ID);
        debtorId = bundle.getLong(UtileName.KEY_DEBTOR_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_credit, container, false);
        setRetainInstance(true);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.update_credit);
        setHasOptionsMenu(true);
        bUpdate.setText(R.string.update);
        ivStuff.setRotation(90);
        tvTitle.setText(R.string.update_credit);
        String[] list = getResources().getStringArray(R.array.choice);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, list);
        sChoice.setAdapter(adapter);
        setIformationInFields();
        sChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch (position) {
                    case 0:
                        etMoney.setInputType(InputType.TYPE_CLASS_NUMBER);
                        etMoney.setHint(R.string.amount_of_money);
                        llstaff.setVisibility(View.INVISIBLE);
                        isChoisCredit = true;
                        break;
                    case 1:
                        etMoney.setInputType(InputType.TYPE_CLASS_TEXT);
                        etMoney.setHint(R.string.input_name_things);
                        llstaff.setVisibility(View.VISIBLE);
                        isChoisCredit = false;
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        animate(etDateFrom, etDateTo, etMoney, ivCalendar, ivMoney, ivCamera, ivStuff);
        if(savedInstanceState != null){
            isDialogShowed = savedInstanceState.getBoolean(UtileName.KEY_IS_DIALOG_SHOW);
            if (isDialogShowed)
                imagePickerDialog(getString(R.string.photo_picker), getString(R.string.you_can_choose_a_photo_for_the_pledge)
                        , WorkWithPhoto.GALLERY_PICTURE_FROM_STAFF, WorkWithPhoto.CAMERA_PICTURE_FROM_STAFF);
        }
        return view;
    }

    private void setIformationInFields() {
        simpleDateFormat = new SimpleDateFormat(UtileName.DATE_FORMAT);
        creditTable = Query.getCredit(id);
        etDateFrom.setText(simpleDateFormat.format(creditTable.getDateStartСredit()));
        try {
            dateFrom = simpleDateFormat.parse(simpleDateFormat.format(creditTable.getDateStartСredit()));
            dateTo = simpleDateFormat.parse(simpleDateFormat.format(creditTable.getDateEndСredit()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        etDateTo.setText(simpleDateFormat.format(creditTable.getDateEndСredit()));
        if (creditTable.getStuff() != null) {
            sChoice.setSelection(1);
            etMoney.setText(creditTable.getStuff());
            if (creditTable.getPhotoStaff() != null) {
                setImageInStaff(new File(creditTable.getPhotoStaff()));
            }
        } else {
            etMoney.setText(creditTable.getAmountOfMoney() + "");
            sChoice.setSelection(0);
        }
    }

    @OnClick(R.id.etDateFrom)
    void getDateFrom(View view) {
        if (view.getId() == R.id.etDateFrom) {
            getDate();
            etDateTo.setVisibility(View.VISIBLE);
            isChoise = true;
        }
    }

    @OnClick(R.id.etDateTo)
    void getDateTo(View view) {
        if (view.getId() == R.id.etDateTo) {
            getDate();
            isChoise = false;
        }
    }

    @OnClick(R.id.bChannge)
    void chengPhoto(View view) {
        if (view.getId() == R.id.bChannge) {
            imagePickerDialog(getString(R.string.photo_picker), getString(R.string.you_can_choose_a_photo_for_the_pledge)
                    , WorkWithPhoto.GALLERY_PICTURE_FROM_STAFF, WorkWithPhoto.CAMERA_PICTURE_FROM_STAFF);
        }
    }

    @OnClick(R.id.bAddCredit)
    void update(View view) {
        if (view.getId() == R.id.bAddCredit) {
            updateCredit();
        }
    }

    public void updateCredit() {
        if (!validate()) {
            return;
        }
        try {
            DebtorTable debtorTable = Query.getDebtor(debtorId);
            creditTable.setDateStartСredit(simpleDateFormat.parse(etDateFrom.getText().toString()));
            creditTable.setDateEndСredit((simpleDateFormat.parse(etDateTo.getText().toString())));
            if (isChoisCredit) {
                creditTable.setAmountOfMoney(Integer.parseInt(etMoney.getText().toString()));
                creditTable.setStuff(null);
                creditTable.setPhotoStaff(null);
            } else {
                creditTable.setStuff(etMoney.getText().toString());
                creditTable.setAmountOfMoney(0);
                if (isPhotoStaff)
                    creditTable.setPhotoStaff(srcImageStaff);
            }
            if(!debtorTable.getParseId().equals("0")) {
                debtorTable.setIsUpdate(true);
                debtorTable.save();
            }
            creditTable.save();
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            ThreadCalendar threadCalendar = new ThreadCalendar(getContext(),
                    ThreadCalendar.UPDATE_CALENDAR, debtorTable, creditTable);
            executorService.execute(threadCalendar);
            successfullyDialog(getString(R.string.you_update_credit));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
