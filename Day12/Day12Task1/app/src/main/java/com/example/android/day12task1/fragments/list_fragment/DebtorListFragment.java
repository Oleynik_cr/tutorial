package com.example.android.day12task1.fragments.list_fragment;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.InformationAboutDeleteRows;
import com.example.android.day12task1.active_dao.qwery.Query;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.fragments.operations_on_debtor.CreateDebtorFragment;
import com.example.android.day12task1.fragments.operations_on_debtor.DataInformationDebtorFragment;
import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.qwery.AsyncDeleteRow;
import com.example.android.day12task1.active_dao.qwery.AsyncSelectAll;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.adaptor.DebtorListAdapter;
import com.example.android.day12task1.utile.UtileName;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;


import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class DebtorListFragment extends AbstractListFragment  {

    private DebtorTable debtorTable;
    private SwipeMenuListView mListView;
    private AbstractListFragment abstractListFragment;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(null);
        tvToolbarTitle.setText(R.string.debtor_list);
        setHasOptionsMenu(true);
        abstractListFragment = this;
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);
        adapter = new DebtorListAdapter(getContext(),new ArrayList<DebtorTable>());
        animationAdapter = new SwingRightInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(mListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                debtorTable = (DebtorTable) adapter.getItem(position);
                DataInformationDebtorFragment informationDebtorFragment = new DataInformationDebtorFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(UtileName.KEY_DEBTOR_ID, debtorTable.getId());
                informationDebtorFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame,
                        informationDebtorFragment).addToBackStack(null).commit();
            }
        });
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                deleteItem.setWidth(dp2px(90));
                deleteItem.setIcon(R.drawable.delete);
                menu.addMenuItem(deleteItem);
            }
        };
        mListView.setMenuCreator(creator);
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                debtorTable = (DebtorTable) adapter.getItem(position);
                confirmationDelete();
                return false;
            }
        });
        return view;
    }

    @OnClick(R.id.fab)
    void addDebtor(View view) {
        if (view.getId() == R.id.fab) {
            CreateDebtorFragment createDebtorFragment = new CreateDebtorFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame,
                    createDebtorFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        upDateList();
    }

    public void upDateList() {
        new AsyncSelectAll<DebtorTable>(adapter, mListView, animationAdapter).execute(new Select().from(DebtorTable.class));
    }
    public void confirmationDelete() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.are_you_sure))
                .setContentText(getString(R.string.wont_be_able_to_recover_this_row))
                .setConfirmText(getString(R.string.delete_it))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.setTitleText(getString(R.string.deleted))
                                .setContentText(getString(R.string.your_imaginary_row_has_been_deleted))
                                .setConfirmText(getString(R.string.ok))
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        setInformatiomAboutDelete();
                        deleteCalendar(debtorTable);
                        new AsyncDeleteRow<DebtorTable>(debtorTable, abstractListFragment).execute();
                    }
                })
                .show();
    }

    private void setInformatiomAboutDelete() {
        if(!debtorTable.getParseId().equals("0")) {
            InformationAboutDeleteRows informationAboutDeleteRows = new InformationAboutDeleteRows();
            informationAboutDeleteRows.setIdDeletedRow(debtorTable.getParseId());
            informationAboutDeleteRows.save();
        }
    }

    private void deleteCalendar(DebtorTable debtorTable) {
        ArrayList<CreditTable> creditTableList = (ArrayList<CreditTable>)
                Query.getCreditList(debtorTable.getId());
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ThreadCalendar threadCalendar;
        for (CreditTable creditTable: creditTableList){
            threadCalendar = new ThreadCalendar(getContext(),
                    ThreadCalendar.DELETE_CALENDAR,creditTable);
            executorService.execute(threadCalendar);
        }
    }
}
