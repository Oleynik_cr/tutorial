package com.example.android.day12task1.active_dao.qwery;


import com.activeandroid.Model;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.InformationAboutDeleteRows;

import java.util.ArrayList;
import java.util.List;

public class Query {

    public static int getCountCredit(Long id) {
        From from = new Select()
                .from(CreditTable.class)
                .where("debtor_id =?", id);
        return from.count();
    }

    public static DebtorTable getDebtor(Long id) {
        return   new Select()
                .from(DebtorTable.class)
                .where("_id =?", id)
                .executeSingle();
    }
    public static CreditTable getCredit(Long id) {
        return new Select()
                .from(CreditTable.class)
                .where("_id =?", id)
                .executeSingle();
    }
    public static List<DebtorTable> getDebtorList() {
        return   new Select()
                .from(DebtorTable.class)
                .where("parseId =?", "0")
                .execute();
    }
    public static List<CreditTable> getCreditList(Long id) {
        return   new Select()
                .from(CreditTable.class)
                .where("debtor_id =?", id)
                .execute();
    }
    public static DebtorTable getDebtorParseId(String parseId) {
        return   new Select()
                .from(DebtorTable.class)
                .where("parseId =?", parseId)
                .executeSingle();
    }
    public static List<InformationAboutDeleteRows> getDeleteDebtorId() {
        return   new Select()
                .from(InformationAboutDeleteRows.class)
                .execute();
    }
    public static List<DebtorTable> getUpdateDebtor() {
        return   new Select()
                .from(DebtorTable.class)
                .where("isUpdate =?", true)
                .execute();
    }
    public static List<CreditTable> getAllCredit() {
        return   new Select()
                .from(CreditTable.class)
                .execute();
    }

}
