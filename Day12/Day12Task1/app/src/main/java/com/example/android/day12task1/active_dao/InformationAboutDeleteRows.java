package com.example.android.day12task1.active_dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


@Table(name = "deleted_rows", id = "_id")
public class InformationAboutDeleteRows extends Model {

    @Column(name = "id_delete_row")
    private String idDeletedRow;

    public InformationAboutDeleteRows() {
        super();
    }

    public InformationAboutDeleteRows(String idDeletedRow) {
        this.idDeletedRow = idDeletedRow;
    }

    public String getIdDeletedRow() {
        return idDeletedRow;
    }

    public void setIdDeletedRow(String idDeletedRow) {
        this.idDeletedRow = idDeletedRow;
    }
}
