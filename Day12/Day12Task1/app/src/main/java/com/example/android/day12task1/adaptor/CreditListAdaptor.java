package com.example.android.day12task1.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.Bind;


public  class CreditListAdaptor extends AbstractListAdaptor<CreditTable> {
    private List<CreditTable> data;
    SimpleDateFormat simpleDateFormat;

    public CreditListAdaptor(Context context, List<CreditTable> mData) {
        super(context, 0, mData);
        data = mData;
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    static class ViewHolder {
        @Bind(R.id.tvDateFrom)
        TextView tvDateFrom;
        @Bind(R.id.tvDateTo)
        TextView tvDateTo;
        @Bind(R.id.tvCredits)
        TextView tvCredits;
        @Bind(R.id.tvTypeStaff)
        TextView tvTypeStaff;
        @Bind(R.id.imageView)
        ImageView imageView;
        @Bind(R.id.linearLayout)
        LinearLayout linearLayout;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_credit, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CreditTable creditTable = getItem(position);

        if (creditTable != null) {
            if(cheсkExpiredCredit(creditTable.getDateEndСredit()))
                viewHolder.linearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.red));
            viewHolder.tvDateFrom.setText(simpleDateFormat.format(creditTable.getDateStartСredit()));
            viewHolder.tvDateTo.setText(simpleDateFormat.format(creditTable.getDateEndСredit()));
            if (creditTable.getStuff() != null) {
                viewHolder.tvTypeStaff.setText(R.string.credit_staf);
                viewHolder.tvCredits.setText(creditTable.getStuff());
                if (creditTable.getPhotoStaff() != null) {
                    Picasso.with(getContext())
                            .load(new File(creditTable.getPhotoStaff()))
                            .resize(125, 125)
                            .centerCrop()
                            .into(viewHolder.imageView);
                }
            } else {
                viewHolder.tvTypeStaff.setText(R.string.credit_money);
                viewHolder.tvCredits.setText(creditTable.getAmountOfMoney() + "");
                Picasso.with(getContext())
                        .load(R.drawable.coins36)
                        .resize(125, 125)
                        .centerCrop()
                        .into(viewHolder.imageView);
            }
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public void setDate(List<CreditTable> items) {
        data.clear();
        searchList.clear();
        data.addAll(items);
        searchList.addAll(items);
    }

    @Override
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(searchList);
        } else {
            for (CreditTable creditTable : searchList) {
                if (Integer.toString(creditTable.getAmountOfMoney()).toLowerCase(Locale.getDefault())
                        .contains(charText)||simpleDateFormat.format(creditTable.getDateStartСredit()).
                        toLowerCase(Locale.getDefault())
                        .contains(charText)||simpleDateFormat.format(creditTable.getDateStartСredit()).
                        toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    data.add(creditTable);
                }
            }
        }
        notifyDataSetChanged();
    }
}
