package com.example.android.day12task1.utile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.android.day12task1.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;

public class WorkWithPhoto {

	public static File CopySourceLocation;
	public static File PasteTargetLocation;
	public static File imgDir;
	public static File DefaultDIR;
	public static Uri uri;
	public static final int CAMERA_PICTURE = 1;
	public static final int GALLERY_PICTURE = 2;
	public static final int PICK_CONTACT_REQUEST = 3;
	public static final int CAMERA_PICTURE_FROM_STAFF = 4;
	public static final int GALLERY_PICTURE_FROM_STAFF = 5;
	private static final String NAME_FOLDER = "/Image_collector/";

	public static String GetFileName() {
		final Calendar c = Calendar.getInstance();
		int myYear = c.get(Calendar.YEAR);
		int myMonth = c.get(Calendar.MONTH);
		int myDay = c.get(Calendar.DAY_OF_MONTH);
		String RandomImageText = "" + myDay + myMonth + myYear + "_" + Math.random();
		return RandomImageText;
	}

	public static File copyFile(File currentLocation, File destinationLocation) {
		CopySourceLocation = new File("" + currentLocation);
		PasteTargetLocation = new File("" + destinationLocation + "/" + WorkWithPhoto.GetFileName() + ".jpg");
		try {
			if (CopySourceLocation.exists()) {
				InputStream in = new FileInputStream(CopySourceLocation);
				OutputStream out = new FileOutputStream(PasteTargetLocation);
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PasteTargetLocation;
	}

	public static Bitmap decodeFile(File file) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(file), null, options);
			final int REQUIRED_SIZE = 70;
			int widthTmp = options.outWidth;
			int heightTmp = options.outHeight;
			int scale = 1;
			while (true) {
				if (widthTmp / 2 < REQUIRED_SIZE || heightTmp / 2 < REQUIRED_SIZE)
					break;
				widthTmp /= 2;
				heightTmp /= 2;
				scale++;
			}
			BitmapFactory.Options options1 = new BitmapFactory.Options();
			options1.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(file), null, options1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static File CreateImagesDir() {
		try {
			imgDir = new File(Environment.getExternalStorageDirectory(), NAME_FOLDER);
			if (!imgDir.exists()) {
				imgDir.mkdirs();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imgDir;
	}

	public static int getExifOrientation(String filepath) {
		int degree = 0;
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(filepath);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		if (exif != null) {
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
			if (orientation != -1) {
				switch (orientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						degree = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						degree = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						degree = 270;
						break;
				}
			}
		}
		return degree;
	}

	public static byte[] getBite(String src) {
		File file = new File(src);
		int size = (int) file.length();
		byte[] bytes = new byte[size];
		try {
			BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
			buf.read(bytes, 0, bytes.length);
			buf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}

	public static boolean isConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if(activeNetwork != null && activeNetwork.isConnectedOrConnecting())
			return true;
		else {
			Toast toast = Toast.makeText(context, R.string.internet_is_no_excuse, Toast.LENGTH_LONG);
			toast.show();
			return false;
		}
	}

	public static void writeImage(byte[] data, String src) {
		FileOutputStream fop = null;
		File file;
		try {
			file = new File(src);
			fop = new FileOutputStream(file);
			if (!file.exists()) {
				file.createNewFile();
			}
			fop.write(data);
			fop.flush();
			fop.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static byte[] getBiteWithBitMap(Bitmap bitmap, String src) {
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(src);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int exifOrientation = exif.getAttributeInt(
				ExifInterface.TAG_ORIENTATION,
				ExifInterface.ORIENTATION_NORMAL);
		int rotate = 0;
		switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;

			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;

			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
		}
		if (rotate != 0) {
			int w = bitmap.getWidth();
			int h = bitmap.getHeight();
			Matrix mtx = new Matrix();
			mtx.preRotate(rotate);

			bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
			bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}

	public static void deleteDirectory() {
		File dir = new File(Environment.getExternalStorageDirectory()+NAME_FOLDER);
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				new File(dir, children[i]).delete();
			}
		}
	}
}
