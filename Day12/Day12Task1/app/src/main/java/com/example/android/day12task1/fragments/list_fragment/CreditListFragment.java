package com.example.android.day12task1.fragments.list_fragment;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.activeandroid.query.Select;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.android.day12task1.R;
import com.example.android.day12task1.active_dao.qwery.AsyncDeleteRow;
import com.example.android.day12task1.active_dao.qwery.AsyncSelectAll;
import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.adaptor.CreditListAdaptor;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.fragments.operations_on_credit.CreateCreditFragment;
import com.example.android.day12task1.fragments.operations_on_credit.DataInformationCreditFragment;
import com.example.android.day12task1.utile.UtileName;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class CreditListFragment extends AbstractListFragment {

    private SwipeMenuListView mListView;
    private AbstractListFragment abstractListFragment;
    private long id;
    private CreditTable  creditTable;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        id = bundle.getLong(UtileName.KEY_DEBTOR_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lis_creditt, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(null);
        tvToolbarTitle.setText(getString(R.string.credit_list));
        setHasOptionsMenu(true);
        abstractListFragment = this;
        adapter = new CreditListAdaptor(getContext(),new ArrayList<CreditTable>());
        mListView = (SwipeMenuListView) view.findViewById(R.id.listView);
        animationAdapter = new SwingRightInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(mListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long idList) {
                CreditTable creditTable = (CreditTable) adapter.getItem(position);
                DataInformationCreditFragment dataInformationCreditFragment = new DataInformationCreditFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(UtileName.KEY_CREDIT_ID, creditTable.getId());
                bundle.putLong(UtileName.KEY_DEBTOR_ID, id);
                dataInformationCreditFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.frame, dataInformationCreditFragment)
                        .addToBackStack(null).commit();
            }
        });
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity().getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                deleteItem.setWidth(dp2px(90));
                deleteItem.setIcon(R.drawable.delete);
                menu.addMenuItem(deleteItem);
            }
        };
        mListView.setMenuCreator(creator);
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                creditTable = (CreditTable) adapter.getItem(position);
                confirmationDelete();
                return false;
            }
        });
        return view;
    }

    @OnClick(R.id.fab)
    void addDebtor(View view) {
        if (view.getId() == R.id.fab) {
            CreateCreditFragment createCreditFragment = new CreateCreditFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(UtileName.KEY_DEBTOR_ID, id);
            createCreditFragment.setArguments(bundle);
            getFragmentManager().beginTransaction().replace(R.id.frame, createCreditFragment)
                    .addToBackStack(null).commit();
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void upDateList() {
        new AsyncSelectAll<CreditTable>(adapter,mListView,animationAdapter).execute(new Select().from(CreditTable.class)
                .where("debtor_id =?", id));
    }

    public void confirmationDelete() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.are_you_sure))
                .setContentText(getString(R.string.wont_be_able_to_recover_this_row))
                .setConfirmText(getString(R.string.delete_it))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.setTitleText(getString(R.string.deleted))
                                .setContentText(getString(R.string.your_imaginary_row_has_been_deleted))
                                .setConfirmText(getString(R.string.ok))
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        ExecutorService executorService = Executors.newFixedThreadPool(1);
                        ThreadCalendar threadCalendar = new ThreadCalendar(getContext(),
                                ThreadCalendar.DELETE_CALENDAR, creditTable);
                        executorService.execute(threadCalendar);
                        new AsyncDeleteRow<CreditTable>(creditTable,abstractListFragment).execute();
                    }
                })
                .show();
    }
}
