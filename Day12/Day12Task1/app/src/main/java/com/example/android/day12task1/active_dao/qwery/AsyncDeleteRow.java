package com.example.android.day12task1.active_dao.qwery;

import android.os.AsyncTask;

import com.activeandroid.Model;
import com.example.android.day12task1.fragments.list_fragment.AbstractListFragment;


public class AsyncDeleteRow<T extends Model> extends AsyncTask<Void, Void, Void> {
    AbstractListFragment abstractListFragment;
    T data;
    public AsyncDeleteRow(T data, AbstractListFragment abstractListFragment) {
        this.data = data;
        this.abstractListFragment = abstractListFragment;
    }

    @Override
    protected Void doInBackground(Void... params) {
        data.delete();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        abstractListFragment.upDateList();
    }
}
