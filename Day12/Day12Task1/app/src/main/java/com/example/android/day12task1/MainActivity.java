package com.example.android.day12task1;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.example.android.day12task1.active_dao.qwery.ClearAllTable;
import com.example.android.day12task1.dialogs.DialogSynchronization;
import com.example.android.day12task1.fragments.OnHeadlineSelectedListener;
import com.example.android.day12task1.fragments.list_fragment.DebtorListFragment;
import com.example.android.day12task1.registration.LoginActivity;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.drakeet.materialdialog.MaterialDialog;


public class MainActivity extends AppCompatActivity implements OnMenuItemClickListener, OnHeadlineSelectedListener {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private AlertDialog alertDialog;
    private boolean isDialogShowed;
    ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActiveAndroid.initialize(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.getBoolean(LoginActivity.SECOND_IN) == true)
                startDialog(false);
            checkLogin();
        } else {
            isDialogShowed = savedInstanceState.getBoolean(UtileName.KEY_IS_DIALOG_SHOW);
            if (isDialogShowed)
                startDialog(true);
        }
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.synchronization:
                        if (WorkWithPhoto.isConnected(getApplication()))
                            startDialog(true);
                        return true;
                    case R.id.twitterAssociation:
                        twitterAssociation();
                        return true;
                    case R.id.facebookAssociation:
                        facebookAssociation();
                        return true;
                    case R.id.exit:
                        confirExitInAccount();
                        return true;
                    default:
                        return true;
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        if ( fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onMenuItemClick(View view, int i) {
    }

    private void checkLogin(){
        if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
            goToLoginIn();
        } else {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                DebtorListFragment debtorListFragment = new DebtorListFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, debtorListFragment).commit();
            } else {
                goToLoginIn();
            }
        }
    }
    public void startDialog(final boolean ischoice) {
        AlertDialog.Builder  alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle(getString(R.string.attention));
        alertDialogBuilder
                .setMessage(getString(R.string.synchronize_with_parse))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DialogSynchronization dialogSynchronization = DialogSynchronization.newInstance(ischoice);
                        dialogSynchronization.show(getFragmentManager(), null);
                        dialogSynchronization.setCancelable(false);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void goToLoginIn(){
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }
    @Override
    public void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(UtileName.KEY_IS_DIALOG_SHOW, alertDialog != null && alertDialog.isShowing());
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onDestroy() {
        if(alertDialog != null)
            alertDialog.dismiss();
        super.onDestroy();
    }

    @Override
    public void updateListDebtor() {
        DebtorListFragment debtorListFragment = new DebtorListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, debtorListFragment).commit();
    }

    public void confirExitInAccount() {
        final MaterialDialog mMaterialDialog = new MaterialDialog(this);
        mMaterialDialog.setTitle(getString(R.string.are_you_sure));
        mMaterialDialog.setMessage(getString(R.string.exit_account));
        mMaterialDialog.setPositiveButton(getString(R.string.ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExecutorService executorService = Executors.newFixedThreadPool(1);
                ClearAllTable clearAllTable = new ClearAllTable(getBaseContext());
                executorService.execute(clearAllTable);
                mMaterialDialog.dismiss();
                ParseUser.logOut();
                goToLoginIn();
            }
        });
        mMaterialDialog.setNegativeButton(getString(R.string.cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog.dismiss();
            }
        });
        mMaterialDialog.show();
    }
    private void twitterAssociation(){
        final ParseUser parseUser = ParseUser.getCurrentUser();
        if (!ParseTwitterUtils.isLinked(parseUser))
            ParseTwitterUtils.link(parseUser, this, new SaveCallback() {
                @Override
                public void done(ParseException ex) {
                    if (ParseTwitterUtils.isLinked(parseUser)) {
                        Toast toast = Toast.makeText(getApplication(), R.string.logged_in_with_twitter, Toast.LENGTH_LONG);
                        toast.show();
                    }else{
                        Toast toast = Toast.makeText(getApplication(), R.string.something_wrong, Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            });
    }
    private void facebookAssociation() {
        final ParseUser parseUser = ParseUser.getCurrentUser();
        Collection<String> permissions = Arrays.asList(LoginActivity.PROFILE_FACEBOOK, LoginActivity.USER_FACEBOOK);
        if (!ParseFacebookUtils.isLinked(parseUser)) {
            ParseFacebookUtils.linkWithReadPermissionsInBackground(parseUser, this, permissions, new SaveCallback() {
                @Override
                public void done(ParseException ex) {
                    if (ParseFacebookUtils.isLinked(parseUser)) {
                        Toast toast = Toast.makeText(getApplication(), R.string.logged_in_with_facebook, Toast.LENGTH_LONG);
                        toast.show();
                    }else{
                        Toast toast = Toast.makeText(getApplication(), R.string.something_wrong, Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            });
        }
    }
}


