package com.example.android.day12task1.adaptor;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by android on 09.10.15.
 */
public abstract class AbstractListAdaptor<T> extends ArrayAdapter<T> {

    public ArrayList<T>searchList;
    public AbstractListAdaptor(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        this.searchList = new ArrayList<T>();
    }

    public abstract void  setDate(List<T> items);

    @Override
    public abstract View getView(int position, View convertView, ViewGroup viewGroup);
    public abstract void filter(String charText);

    public boolean cheсkExpiredCredit(Date endDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        if(endDate.equals(simpleDateFormat.format(currentDate)) || endDate.before(currentDate))
            return true;
        else
            return false;
    }
}


