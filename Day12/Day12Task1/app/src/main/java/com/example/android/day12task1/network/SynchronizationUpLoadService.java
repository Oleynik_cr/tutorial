package com.example.android.day12task1.network;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.example.android.day12task1.active_dao.CreditTable;
import com.example.android.day12task1.active_dao.DebtorTable;
import com.example.android.day12task1.active_dao.InformationAboutDeleteRows;
import com.example.android.day12task1.active_dao.qwery.Query;
import com.example.android.day12task1.calendar.ThreadCalendar;
import com.example.android.day12task1.dialogs.DialogSynchronization;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SynchronizationUpLoadService extends Service {

    public final static String CREDIT_TABLE = "Credit";
    public final static String CREDIT_DATA_START = "date_start_credit";
    public final static String CREDIT_DATA_END = "date_end_credit";
    public final static String CREDIT_MONEY = "money";
    public final static String CREDIT_STUFF = "stuff";
    public final static String CREDIT_SRC_STUFF = "src_staff";
    public final static String CREDIT_PHOTO_STAFF = "photo_staff";
    public final static String CREDIT_ID_DEBTOR = "id_debtor";
    public final static String DEBTOR_TABLE = "Debtor";
    public final static String DEBTOR_FIRST_NAME = "first_name";
    public final static String DEBTOR_LAST_NAME = "last_name";
    public final static String DEBTOR_PHONE = "phone";
    public final static String DEBTOR_EMAIL = "email";
    public final static String DEBTOR_SRC_PHOTO = "src_photo";
    public final static String DEBTOR_PHOTO = "photo";
    public final static String TYPE_PHOTO = "photo.png";

    private DialogSynchronization dialogSynchronization;
    android.os.Handler handler = new android.os.Handler();
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public SynchronizationUpLoadService getService() {
            return SynchronizationUpLoadService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    public void startSynchronization(Boolean ischoiсe){
        if(ischoiсe) {
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            Run mr = new Run();
            executorService.execute(mr);
        }else {
            saveDebtorInPhone();
        }
    }

    class Run implements Runnable {
        public void run() {
            uploadInParse();

        }
    }
    public void setLocalBinding(DialogSynchronization dialogSynchronization) {
        this.dialogSynchronization = dialogSynchronization;}

    private void uploadInParse(){
        saveDebtorInParse();
    }

    private void saveCreditInParse(ArrayList<DebtorTable> debtorList){
        ParseFile file;
        for (final DebtorTable debtorTable : debtorList) {
            ArrayList<CreditTable> creditTableArrayList = (ArrayList<CreditTable>)
                    Query.getCreditList(debtorTable.getId());
            for (CreditTable creditTable : creditTableArrayList) {
                String idObject = null;
                ParseObject credit = new ParseObject(CREDIT_TABLE);
                credit.put(CREDIT_DATA_START, creditTable.getDateStartСredit());
                credit.put(CREDIT_DATA_END, creditTable.getDateEndСredit());
                if (creditTable.getStuff() == null) {
                    credit.put(CREDIT_MONEY, creditTable.getAmountOfMoney());
                } else {
                    credit.put(CREDIT_STUFF, creditTable.getStuff());
                    if (creditTable.getPhotoStaff() != null)
                        credit.put(CREDIT_SRC_STUFF, creditTable.getPhotoStaff());
                    try {
                        file = new ParseFile(TYPE_PHOTO, WorkWithPhoto.getBite(creditTable.getPhotoStaff()));
                        credit.put(CREDIT_PHOTO_STAFF, file);
                    } catch (IllegalStateException e1) {
                        e1.printStackTrace();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialogSynchronization.failed();
                            }
                        });
                    }
                }
                credit.put(CREDIT_ID_DEBTOR, ParseObject.createWithoutData(DEBTOR_TABLE, debtorTable.getParseId()));
                try {
                    credit.save();
                    idObject = credit.getObjectId();
                    creditTable.setParseId(idObject);
                    creditTable.save();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        deleteDebtor();
    }
    private void saveDebtorInParse() {
        int iterat = 0;
        ParseFile file;
        try {
            final ArrayList<DebtorTable> debtorList =(ArrayList<DebtorTable>) Query.getDebtorList();
            for(final DebtorTable debtorTable : debtorList){
                final int i = iterat;
                final ParseObject debtor = new ParseObject(DEBTOR_TABLE);
                iterat++;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialogSynchronization.setUpdate(i * (100 / debtorList.size()));
                    }
                });
                debtor.put(DEBTOR_FIRST_NAME, debtorTable.getFirstName());
                debtor.put(DEBTOR_LAST_NAME, debtorTable.getLastName());
                debtor.put(DEBTOR_PHONE, debtorTable.getPhone());
                debtor.put(DEBTOR_EMAIL,debtorTable.getEmail());
                debtor.put(DEBTOR_SRC_PHOTO, debtorTable.getSrcPhoto());
                file = new ParseFile(TYPE_PHOTO, WorkWithPhoto.getBite(debtorTable.getSrcPhoto()));
                debtor.put(DEBTOR_PHOTO, file);
                ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                ParseACL.setDefaultACL(new ParseACL(), true);
                debtor.setACL(postACL);
                debtor.save();
                debtorTable.setParseId(debtor.getObjectId());
                debtorTable.save();
            }
            saveCreditInParse(debtorList);
        }catch(IllegalStateException e){
            e.printStackTrace();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    dialogSynchronization.failed();
                }
            });
        } catch (ParseException e1) {
            e1.printStackTrace();
        }

    }
    private void saveDebtorInPhone() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(DEBTOR_TABLE);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objectList, ParseException e) {
                if (e == null) {
                    int i = 0;
                    if(objectList.size() == 0){
                        dialogSynchronization.closeDialog();
                    }
                    for (ParseObject object : objectList) {
                        dialogSynchronization.setUpdate(i * (100 / objectList.size()));
                        final DebtorTable debtorTable = new DebtorTable();
                        debtorTable.setParseId(object.getObjectId());
                        debtorTable.setFirstName(object.getString(DEBTOR_FIRST_NAME));
                        debtorTable.setLastName(object.getString(DEBTOR_LAST_NAME));
                        debtorTable.setPhone(object.getString(DEBTOR_PHONE));
                        debtorTable.setEmail(object.getString(DEBTOR_EMAIL));
                        debtorTable.setSrcPhoto(object.getString(DEBTOR_SRC_PHOTO));
                        debtorTable.save();
                        i++;
                        ParseFile applicantResume = (ParseFile) object.get(DEBTOR_PHOTO);
                        applicantResume.getDataInBackground(new GetDataCallback() {
                            public void done(byte[] data, ParseException e) {
                                if (e == null) {
                                    WorkWithPhoto.writeImage(data, debtorTable.getSrcPhoto());
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                } else {
                    e.printStackTrace();
                }
            }
        });
        saveCreditInPhone();
    }
    private void saveCreditInPhone(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(CREDIT_TABLE);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(final List<ParseObject> creditList, ParseException e) {
                if (e == null) {
                    for (ParseObject object : creditList) {
                        final CreditTable creditTable = new CreditTable();
                        creditTable.setParseId(object.getObjectId());
                        creditTable.setStuff(object.getString(CREDIT_STUFF));
                        creditTable.setAmountOfMoney(object.getInt(CREDIT_MONEY));
                        creditTable.setPhotoStaff(object.getString(CREDIT_SRC_STUFF));
                        creditTable.setDateStartСredit(object.getDate(CREDIT_DATA_START));
                        creditTable.setDateEndСredit(object.getDate(CREDIT_DATA_END));
                        object.getParseObject(CREDIT_ID_DEBTOR)
                                .fetchIfNeededInBackground(new GetCallback<ParseObject>() {
                                    public void done(ParseObject object, ParseException e) {
                                        DebtorTable debtorTable = Query.getDebtorParseId(object.getObjectId());
                                        if (debtorTable != null) {
                                            creditTable.setDebtorTable(debtorTable);
                                            createRowInCalendar(debtorTable, creditTable);
                                            creditTable.save();
                                            dialogSynchronization.closeDialog();
                                        }
                                    }

                                });

                        if (object.get(CREDIT_PHOTO_STAFF) != null) {
                            ParseFile applicantResume = (ParseFile) object.get(CREDIT_PHOTO_STAFF);
                            applicantResume.getDataInBackground(new GetDataCallback() {
                                public void done(byte[] data, ParseException e) {
                                    if (e == null) {
                                        WorkWithPhoto.writeImage(data, creditTable.getPhotoStaff());
                                    } else {

                                    }
                                }
                            });
                        }
                    }

                } else {
                    dialogSynchronization.closeDialog();
                    e.printStackTrace();
                }
            }
        });
    }

    private void createRowInCalendar(DebtorTable debtorTable, CreditTable creditTable) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        ThreadCalendar threadCalendar = new ThreadCalendar(getBaseContext(), ThreadCalendar.CREATE_CALENDAR,
                debtorTable, creditTable);
        executorService.execute(threadCalendar);
    }

    private  void deleteDebtor() {
        List<InformationAboutDeleteRows> deleteDebtorId = Query.getDeleteDebtorId();
        for (final InformationAboutDeleteRows informationAboutDeleteRows : deleteDebtorId) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(DEBTOR_TABLE);
            query.getInBackground(informationAboutDeleteRows.getIdDeletedRow(), new GetCallback<ParseObject>() {
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        try {
                            deleteCredit(object);
                            object.delete();
                            informationAboutDeleteRows.delete();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        e.printStackTrace();
                    }
                }
            });
        }
        updateDebtor();
    }
    private void deleteCredit(ParseObject objectId){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(CREDIT_TABLE);
        query.whereEqualTo(CREDIT_ID_DEBTOR, objectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> creditList, ParseException e) {
                if (e == null) {
                    for (ParseObject object : creditList) {
                        try {
                            object.delete();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateDebtor() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialogSynchronization.setUpdate(100);
            }
        });
        List<DebtorTable> updateDebtor = Query.getUpdateDebtor();
        for (final DebtorTable debtorTable : updateDebtor) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery(DEBTOR_TABLE);
            query.getInBackground(debtorTable.getParseId(), new GetCallback<ParseObject>() {
                public void done(ParseObject debtor, ParseException e) {
                    if (e == null) {
                        debtor.put(DEBTOR_FIRST_NAME, debtorTable.getFirstName());
                        debtor.put(DEBTOR_LAST_NAME, debtorTable.getLastName());
                        debtor.put(DEBTOR_PHONE, debtorTable.getPhone());
                        debtor.put(DEBTOR_EMAIL, debtorTable.getEmail());
                        debtor.put(DEBTOR_SRC_PHOTO, debtorTable.getSrcPhoto());
                        try {
                            ParseFile file = new ParseFile(TYPE_PHOTO, WorkWithPhoto.getBite(debtorTable.getSrcPhoto()));
                            debtor.put(DEBTOR_PHOTO, file);
                            debtor.save();
                            debtorTable.setIsUpdate(false);
                            debtorTable.save();
                        } catch (ParseException e1) {
                            e1.printStackTrace();

                        } catch (IllegalStateException e2) {
                            e2.printStackTrace();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dialogSynchronization.failed();
                                }
                            });
                        }
                    }
                }
            });
            updateCredit(debtorTable);
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialogSynchronization.closeDialog();
            }
        });
    }

    private void updateCredit(DebtorTable debtorTable) {
        ParseFile file;
        ParseQuery<ParseObject> query = ParseQuery.getQuery(DEBTOR_TABLE);
        query.getInBackground(debtorTable.getParseId(), new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery(CREDIT_TABLE);
                    query.whereEqualTo(CREDIT_ID_DEBTOR, object);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> creditList, ParseException e) {
                            if (e == null) {
                                for (ParseObject object : creditList) {
                                    try {
                                        object.delete();
                                    } catch (ParseException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } else {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    e.printStackTrace();
                }
            }
        });
        ArrayList<CreditTable> creditTableArrayList = (ArrayList<CreditTable>)
                Query.getCreditList(debtorTable.getId());
        for (CreditTable creditTable : creditTableArrayList) {
            String idObject = null;
            ParseObject credit = new ParseObject(CREDIT_TABLE);
            credit.put(CREDIT_DATA_START, creditTable.getDateStartСredit());
            credit.put(CREDIT_DATA_END, creditTable.getDateEndСredit());
            if (creditTable.getStuff() == null) {
                credit.put(CREDIT_MONEY, creditTable.getAmountOfMoney());
            } else {
                credit.put(CREDIT_STUFF, creditTable.getStuff());
                if (creditTable.getPhotoStaff() != null)
                    credit.put(CREDIT_SRC_STUFF, creditTable.getPhotoStaff());
                file = new ParseFile(TYPE_PHOTO, WorkWithPhoto.getBite(creditTable.getPhotoStaff()));
                credit.put(CREDIT_PHOTO_STAFF, file);
            }
            credit.put(CREDIT_ID_DEBTOR, ParseObject.createWithoutData(DEBTOR_TABLE, debtorTable.getParseId()));
            try {
                credit.save();
                idObject = credit.getObjectId();
                creditTable.setParseId(idObject);
                creditTable.save();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }
}


