package com.example.android.day12task1.active_dao;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

@Table(name = "credit", id = "_id")
public class CreditTable extends Model {

    @Column(name = "debtor_id", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete =  Column.ForeignKeyAction.CASCADE)
    private DebtorTable debtorTable;
    @Column(name = "amountOfMoney")
    private int amountOfMoney;
    @Column(name = "date_start_credit")
    private Date dateStartСredit;
    @Column(name = "date_end_credit")
    private Date dateEndСredit;
    @Column(name = "stuff")
    private String stuff;
    @Column(name = "photo_stuff")
    private String photoStaff;
    @Column(name = "parseId")
    private String parseId;
    @Column(name = "calendarId")
    private long calendarId;

    public CreditTable() {
        super();
    }

    public CreditTable(DebtorTable debtorTable, int amountOfMoney, Date dateStartСredit, Date dateEndСredit, String stuff, String photoStaff, String parseId, long calendarId) {
        this.debtorTable = debtorTable;
        this.amountOfMoney = amountOfMoney;
        this.dateStartСredit = dateStartСredit;
        this.dateEndСredit = dateEndСredit;
        this.stuff = stuff;
        this.photoStaff = photoStaff;
        this.parseId = parseId;
        this.calendarId = calendarId;
    }

    public String getParseId() {
        return parseId;
    }

    public void setParseId(String parseId) {
        this.parseId = parseId;
    }

    public DebtorTable getDebtorTable() {
        return debtorTable;
    }

    public void setDebtorTable(DebtorTable debtorTable) {
        this.debtorTable = debtorTable;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public Date getDateStartСredit() {
        return dateStartСredit;
    }

    public void setDateStartСredit(Date dateStartСredit) {
        this.dateStartСredit = dateStartСredit;
    }

    public Date getDateEndСredit() {
        return dateEndСredit;
    }

    public void setDateEndСredit(Date dateEndСredit) {
        this.dateEndСredit = dateEndСredit;
    }

    public String getStuff() {
        return stuff;
    }

    public void setStuff(String stuff) {
        this.stuff = stuff;
    }

    public String getPhotoStaff() {
        return photoStaff;
    }

    public void setPhotoStaff(String photoStaff) {
        this.photoStaff = photoStaff;
    }

    public long getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }


}
