package com.example.android.day12task1.fragments.operations_on_debtor;


import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.eftimoff.androidplayer.Player;
import com.eftimoff.androidplayer.actions.property.PropertyAction;
import com.example.android.day12task1.R;
import com.example.android.day12task1.utile.UtileName;
import com.example.android.day12task1.utile.WorkWithPhoto;

import android.content.ContentProviderResult;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import cn.pedant.SweetAlert.SweetAlertDialog;

public abstract class AbstractDebtor extends Fragment{

    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.etPhone)
    EditText etPhone;
    @Bind(R.id.etEmail)
    EditText etEmail;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.backdrop)
    ImageView header;
    @Bind(R.id.layoutInformation)
    LinearLayout layoutInformation;
    @Bind(R.id.bGetPhoto)
    FloatingActionButton floatingActionButton;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    protected String srcImageAvatar;
    protected boolean isPhotoStaff;
    protected long id;
    protected boolean isChoise = true;
    protected String date;
    protected Date dateFrom;
    protected Date dateTo;
    protected String srcImageStaff;
    protected SimpleDateFormat simpleDateFormat;
    protected boolean isChoisCredit;
    protected boolean isDialogShowed;
    protected AlertDialog alertDialog;

    public void imagePickerDialog(String title, String message, final int requestCodeForGallery, final int requestCodeForCamera ) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(R.string.gallery, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setType(UtileName.SET_TYPE);
                intent.putExtra(UtileName.RETURN_DATE, true);
                startActivityForResult(intent, requestCodeForGallery);
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.camera, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, requestCodeForCamera);
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public File imageSelecting(Intent data) {
        try {
            WorkWithPhoto.uri = data.getData();
            if (WorkWithPhoto.uri != null) {
                Cursor cursor = getContext().getContentResolver().query(WorkWithPhoto.uri, new String[]
                        {android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                cursor.moveToFirst();
                final String imageFilePath = cursor.getString(0);
                WorkWithPhoto.DefaultDIR = new File(imageFilePath);
                WorkWithPhoto.CreateImagesDir();
                WorkWithPhoto.copyFile(WorkWithPhoto.DefaultDIR, WorkWithPhoto.imgDir);
                cursor.close();
            } else {
                Toast toast = Toast.makeText(getContext(), R.string.selected_any_image, Toast.LENGTH_LONG);
                toast.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WorkWithPhoto.PasteTargetLocation;
    }

    void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse(UtileName.TYPE_CONTACTS));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(pickContactIntent, WorkWithPhoto.PICK_CONTACT_REQUEST);
    }

    void  getContact(Intent data) {
        Uri contactUri = data.getData();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract
                .CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,ContactsContract.CommonDataKinds.Email.TYPE,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID};
        Cursor cursor = getContext().getContentResolver()
                .query(contactUri, projection, null, null, null);
        cursor.moveToFirst();
        etPhone.setText(cursor.getString(cursor.getColumnIndexOrThrow(
                ContactsContract.CommonDataKinds.Phone.NUMBER)));
        etFirstName.setText(cursor.getString(cursor.getColumnIndexOrThrow(
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
    }

    public void setImageInHead(File file) {
        Bitmap bitmap = WorkWithPhoto.decodeFile(file);
        String validPhoto = file.toString();
        bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
        srcImageAvatar = validPhoto;
        header.setImageBitmap(bitmap);
        header.setRotation(WorkWithPhoto.getExifOrientation(validPhoto));
    }
    public void onLoginFailed() {
        Toast.makeText(getContext(), getString(R.string.failed), Toast.LENGTH_LONG).show();
    }
    public void animateSample(View activityMainMobileNumberLayout, View activityMainPinkFab, View activityMainheaderLayout) {
        final PropertyAction fabAction = PropertyAction.newPropertyAction(activityMainPinkFab).
                scaleX(0).scaleY(0).duration(750).interpolator(new AccelerateDecelerateInterpolator()
        ).build();
        final PropertyAction headerAction = PropertyAction.newPropertyAction(activityMainheaderLayout)
                .interpolator(new DecelerateInterpolator()).translationY(-200).duration(850).alpha(0.4f).build();
        final PropertyAction bottomAction = PropertyAction.newPropertyAction(activityMainMobileNumberLayout
        ).translationY(500).duration(850).alpha(0f).build();
        Player.init().
                animate(headerAction).
                then().
                animate(bottomAction).
                play();
    }
    public void successfullyDialog(String contentText){
        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.good_job))
                .setContentText(contentText)
                .show();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(UtileName.KEY_IS_DIALOG_SHOW, alertDialog != null && alertDialog.isShowing());
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }

    public abstract boolean validate();

    public void WritePhoneContact(String displayName, String number,String email) {
        String strDisplayName 	=  displayName;
        String strNumber 	=  number;
        ArrayList<ContentProviderOperation> cntProOper = new ArrayList<>();
        int contactIndex = cntProOper.size();

        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                .withValue(StructuredName.DISPLAY_NAME, strDisplayName)
                .build());
        cntProOper.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
                        contactIndex)
                .withValue(Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.DATA, email)
                .withValue(Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, "2").build());
        cntProOper.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, WorkWithPhoto.getBiteWithBitMap(
                        WorkWithPhoto.decodeFile(new File(srcImageAvatar)),srcImageAvatar))
                .build());

        cntProOper.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,contactIndex)
                .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                .withValue(Phone.NUMBER, strNumber)
                .withValue(Phone.TYPE, Phone.TYPE_MOBILE).build());
        try {
            ContentProviderResult[] contentProresult = null;
            contentProresult = getContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, cntProOper);
        }
        catch (RemoteException exp) {
         exp.printStackTrace();
        }
        catch (OperationApplicationException exp) {
            exp.printStackTrace();
        }
    }
}
