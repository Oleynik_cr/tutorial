package com.example.android.day12task1.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.day12task1.MainActivity;
import com.example.android.day12task1.R;
import com.example.android.day12task1.utile.WorkWithPhoto;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SingUpActivity extends AppCompatActivity {

    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.etEmaill)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.bSignup)
    Button bSignup;
    @Bind(R.id.etLinklogin)
    TextView etLinklogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actyvity_signup);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.bSignup)
    void singnUp(View view) {
        if (view.getId() == R.id.bSignup) {
            if (WorkWithPhoto.isConnected(this)) {
                if (validate()) {
                    ParseUser user = new ParseUser();
                    user.setUsername(etEmail.getText().toString());
                    user.setPassword(etPassword.getText().toString());
                    user.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                if (WorkWithPhoto.isConnected(getApplication())) {
                                    ParseUser.logInInBackground(etEmail.getText().toString(), etPassword.getText().toString(),
                                            new LogInCallback() {
                                                public void done(ParseUser user, ParseException e) {
                                                    if (user != null) {
                                                        startActivity(new Intent(getApplication(), MainActivity.class));
                                                        finish();
                                                    } else {
                                                        Toast.makeText(
                                                                getApplication(),
                                                                R.string.no_such_user_exist,
                                                                Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                }
                            } else {
                                Toast.makeText(getApplication(),
                                        R.string.sign_up_error, Toast.LENGTH_LONG)
                                        .show();
                            }
                        }
                    });
                }
            }
        }
    }
    @OnClick(R.id.etLinklogin)
    void singnupLink(View view) {
        if (view.getId() == R.id.etLinklogin) {
            startActivity(new Intent(getApplication(),LoginActivity.class));
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
            finish();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            etName.setError(getString(R.string.plase_enter_last_name));
            valid = false;
        } else {
            etName.setError(null);
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.enter_a_valid_email));
            valid = false;
        } else {
            etEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            etPassword.setError(getString(R.string.between_6_and_12_numbers));
            valid = false;
        } else {
            etPassword.setError(null);
        }

        return valid;
    }
}