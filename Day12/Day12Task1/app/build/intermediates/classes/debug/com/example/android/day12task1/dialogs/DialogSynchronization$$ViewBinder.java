// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.dialogs;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DialogSynchronization$$ViewBinder<T extends com.example.android.day12task1.dialogs.DialogSynchronization> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689659, "field 'donutProgress'");
    target.donutProgress = finder.castView(view, 2131689659, "field 'donutProgress'");
  }

  @Override public void unbind(T target) {
    target.donutProgress = null;
  }
}
