// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_credit;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AbstractCredits$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_credit.AbstractCredits> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689663, "field 'etDateFrom'");
    target.etDateFrom = finder.castView(view, 2131689663, "field 'etDateFrom'");
    view = finder.findRequiredView(source, 2131689664, "field 'etDateTo'");
    target.etDateTo = finder.castView(view, 2131689664, "field 'etDateTo'");
    view = finder.findRequiredView(source, 2131689666, "field 'etMoney'");
    target.etMoney = finder.castView(view, 2131689666, "field 'etMoney'");
    view = finder.findRequiredView(source, 2131689667, "field 'sChoice'");
    target.sChoice = finder.castView(view, 2131689667, "field 'sChoice'");
    view = finder.findRequiredView(source, 2131689670, "field 'ivStuff'");
    target.ivStuff = finder.castView(view, 2131689670, "field 'ivStuff'");
    view = finder.findRequiredView(source, 2131689668, "field 'llstaff'");
    target.llstaff = finder.castView(view, 2131689668, "field 'llstaff'");
    view = finder.findRequiredView(source, 2131689662, "field 'ivCalendar'");
    target.ivCalendar = finder.castView(view, 2131689662, "field 'ivCalendar'");
    view = finder.findRequiredView(source, 2131689665, "field 'ivMoney'");
    target.ivMoney = finder.castView(view, 2131689665, "field 'ivMoney'");
    view = finder.findRequiredView(source, 2131689669, "field 'ivCamera'");
    target.ivCamera = finder.castView(view, 2131689669, "field 'ivCamera'");
    view = finder.findRequiredView(source, 2131689660, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131689660, "field 'toolbar'");
  }

  @Override public void unbind(T target) {
    target.etDateFrom = null;
    target.etDateTo = null;
    target.etMoney = null;
    target.sChoice = null;
    target.ivStuff = null;
    target.llstaff = null;
    target.ivCalendar = null;
    target.ivMoney = null;
    target.ivCamera = null;
    target.toolbar = null;
  }
}
