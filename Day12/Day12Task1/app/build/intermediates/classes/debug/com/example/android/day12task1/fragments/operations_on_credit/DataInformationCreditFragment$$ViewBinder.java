// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_credit;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class DataInformationCreditFragment$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_credit.DataInformationCreditFragment> extends com.example.android.day12task1.fragments.operations_on_credit.AbstractCredits$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131689667, "field 'sChoice'");
    target.sChoice = finder.castView(view, 2131689667, "field 'sChoice'");
    view = finder.findRequiredView(source, 2131689672, "field 'bUpdate' and method 'update'");
    target.bUpdate = finder.castView(view, 2131689672, "field 'bUpdate'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.update(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689661, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131689661, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131689663, "method 'getDateFrom'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateFrom(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689664, "method 'getDateTo'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateTo(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689671, "method 'chengPhoto'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.chengPhoto(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.sChoice = null;
    target.bUpdate = null;
    target.tvTitle = null;
  }
}
