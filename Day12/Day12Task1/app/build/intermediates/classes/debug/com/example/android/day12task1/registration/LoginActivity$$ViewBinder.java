// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LoginActivity$$ViewBinder<T extends com.example.android.day12task1.registration.LoginActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689597, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131689597, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131689598, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131689598, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131689599, "field 'blogin' and method 'login'");
    target.blogin = finder.castView(view, 2131689599, "field 'blogin'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.login(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689602, "field 'bgoogle' and method 'getGoogle'");
    target.bgoogle = finder.castView(view, 2131689602, "field 'bgoogle'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getGoogle(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689603, "field 'bTwitte' and method 'getTwitter'");
    target.bTwitte = finder.castView(view, 2131689603, "field 'bTwitte'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getTwitter(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689601, "field 'bFacebook' and method 'getFacebook'");
    target.bFacebook = finder.castView(view, 2131689601, "field 'bFacebook'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getFacebook(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689604, "field 'tvSingnupLink' and method 'singnupLink'");
    target.tvSingnupLink = finder.castView(view, 2131689604, "field 'tvSingnupLink'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.singnupLink(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.etEmail = null;
    target.etPassword = null;
    target.blogin = null;
    target.bgoogle = null;
    target.bTwitte = null;
    target.bFacebook = null;
    target.tvSingnupLink = null;
  }
}
