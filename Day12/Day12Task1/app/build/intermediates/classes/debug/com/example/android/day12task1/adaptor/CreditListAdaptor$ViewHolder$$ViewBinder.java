// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.adaptor;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CreditListAdaptor$ViewHolder$$ViewBinder<T extends com.example.android.day12task1.adaptor.CreditListAdaptor.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689703, "field 'tvDateFrom'");
    target.tvDateFrom = finder.castView(view, 2131689703, "field 'tvDateFrom'");
    view = finder.findRequiredView(source, 2131689704, "field 'tvDateTo'");
    target.tvDateTo = finder.castView(view, 2131689704, "field 'tvDateTo'");
    view = finder.findRequiredView(source, 2131689688, "field 'tvCredits'");
    target.tvCredits = finder.castView(view, 2131689688, "field 'tvCredits'");
    view = finder.findRequiredView(source, 2131689702, "field 'tvTypeStaff'");
    target.tvTypeStaff = finder.castView(view, 2131689702, "field 'tvTypeStaff'");
    view = finder.findRequiredView(source, 2131689701, "field 'imageView'");
    target.imageView = finder.castView(view, 2131689701, "field 'imageView'");
    view = finder.findRequiredView(source, 2131689700, "field 'linearLayout'");
    target.linearLayout = finder.castView(view, 2131689700, "field 'linearLayout'");
  }

  @Override public void unbind(T target) {
    target.tvDateFrom = null;
    target.tvDateTo = null;
    target.tvCredits = null;
    target.tvTypeStaff = null;
    target.imageView = null;
    target.linearLayout = null;
  }
}
