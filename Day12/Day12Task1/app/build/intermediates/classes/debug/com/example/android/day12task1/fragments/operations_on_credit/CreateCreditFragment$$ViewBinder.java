// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_credit;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class CreateCreditFragment$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_credit.CreateCreditFragment> extends com.example.android.day12task1.fragments.operations_on_credit.AbstractCredits$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131689663, "method 'getDateFrom'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateFrom(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689664, "method 'getDateTo'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateTo(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689671, "method 'chengPhoto'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.chengPhoto(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689672, "method 'addCredit'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.addCredit(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

  }
}
