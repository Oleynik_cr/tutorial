// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.registration;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SingUpActivity$$ViewBinder<T extends com.example.android.day12task1.registration.SingUpActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689608, "field 'etName'");
    target.etName = finder.castView(view, 2131689608, "field 'etName'");
    view = finder.findRequiredView(source, 2131689597, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131689597, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131689598, "field 'etPassword'");
    target.etPassword = finder.castView(view, 2131689598, "field 'etPassword'");
    view = finder.findRequiredView(source, 2131689609, "field 'bSignup' and method 'singnUp'");
    target.bSignup = finder.castView(view, 2131689609, "field 'bSignup'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.singnUp(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689610, "field 'etLinklogin' and method 'singnupLink'");
    target.etLinklogin = finder.castView(view, 2131689610, "field 'etLinklogin'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.singnupLink(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    target.etName = null;
    target.etEmail = null;
    target.etPassword = null;
    target.bSignup = null;
    target.etLinklogin = null;
  }
}
