// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_debtor;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class DataInformationDebtorFragment$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_debtor.DataInformationDebtorFragment> extends com.example.android.day12task1.fragments.operations_on_debtor.AbstractDebtor$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131689688, "field 'tvCredits'");
    target.tvCredits = finder.castView(view, 2131689688, "field 'tvCredits'");
    view = finder.findRequiredView(source, 2131689687, "method 'getPhoto'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getPhoto(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689680, "method 'getContacts'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getContacts(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.tvCredits = null;
  }
}
