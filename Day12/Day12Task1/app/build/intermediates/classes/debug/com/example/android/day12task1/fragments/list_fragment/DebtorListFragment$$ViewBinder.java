// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.list_fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class DebtorListFragment$$ViewBinder<T extends com.example.android.day12task1.fragments.list_fragment.DebtorListFragment> extends com.example.android.day12task1.fragments.list_fragment.AbstractListFragment$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131689660, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131689660, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131689690, "method 'addDebtor'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.addDebtor(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.toolbar = null;
  }
}
