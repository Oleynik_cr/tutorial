// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_debtor;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class CreateDebtorFragment$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_debtor.CreateDebtorFragment> extends com.example.android.day12task1.fragments.operations_on_debtor.AbstractDebtor$$ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    super.bind(finder, target, source);

    View view;
    view = finder.findRequiredView(source, 2131689663, "field 'etDateFrom' and method 'getDateFrom'");
    target.etDateFrom = finder.castView(view, 2131689663, "field 'etDateFrom'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateFrom(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689664, "field 'etDateTo' and method 'getDateTo'");
    target.etDateTo = finder.castView(view, 2131689664, "field 'etDateTo'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getDateTo(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689666, "field 'etMoney'");
    target.etMoney = finder.castView(view, 2131689666, "field 'etMoney'");
    view = finder.findRequiredView(source, 2131689670, "field 'ivStuff' and method 'addImageStuff'");
    target.ivStuff = finder.castView(view, 2131689670, "field 'ivStuff'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.addImageStuff(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689667, "field 'sChoice'");
    target.sChoice = finder.castView(view, 2131689667, "field 'sChoice'");
    view = finder.findRequiredView(source, 2131689686, "method 'addDebtor'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.addDebtor(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689687, "method 'getPhoto'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getPhoto(p0);
        }
      });
    view = finder.findRequiredView(source, 2131689680, "method 'getContact'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.getContact(p0);
        }
      });
  }

  @Override public void unbind(T target) {
    super.unbind(target);

    target.etDateFrom = null;
    target.etDateTo = null;
    target.etMoney = null;
    target.ivStuff = null;
    target.sChoice = null;
  }
}
