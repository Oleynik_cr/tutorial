// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.adaptor;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DebtorListAdapter$ViewHolder$$ViewBinder<T extends com.example.android.day12task1.adaptor.DebtorListAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689705, "field 'tvFirstName'");
    target.tvFirstName = finder.castView(view, 2131689705, "field 'tvFirstName'");
    view = finder.findRequiredView(source, 2131689706, "field 'tvLastName'");
    target.tvLastName = finder.castView(view, 2131689706, "field 'tvLastName'");
    view = finder.findRequiredView(source, 2131689701, "field 'imageView'");
    target.imageView = finder.castView(view, 2131689701, "field 'imageView'");
    view = finder.findRequiredView(source, 2131689707, "field 'tvExpired'");
    target.tvExpired = finder.castView(view, 2131689707, "field 'tvExpired'");
  }

  @Override public void unbind(T target) {
    target.tvFirstName = null;
    target.tvLastName = null;
    target.imageView = null;
    target.tvExpired = null;
  }
}
