// Generated code from Butter Knife. Do not modify!
package com.example.android.day12task1.fragments.operations_on_debtor;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class AbstractDebtor$$ViewBinder<T extends com.example.android.day12task1.fragments.operations_on_debtor.AbstractDebtor> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131689679, "field 'etFirstName'");
    target.etFirstName = finder.castView(view, 2131689679, "field 'etFirstName'");
    view = finder.findRequiredView(source, 2131689681, "field 'etLastName'");
    target.etLastName = finder.castView(view, 2131689681, "field 'etLastName'");
    view = finder.findRequiredView(source, 2131689683, "field 'etPhone'");
    target.etPhone = finder.castView(view, 2131689683, "field 'etPhone'");
    view = finder.findRequiredView(source, 2131689685, "field 'etEmail'");
    target.etEmail = finder.castView(view, 2131689685, "field 'etEmail'");
    view = finder.findRequiredView(source, 2131689660, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131689660, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131689676, "field 'header'");
    target.header = finder.castView(view, 2131689676, "field 'header'");
    view = finder.findRequiredView(source, 2131689677, "field 'layoutInformation'");
    target.layoutInformation = finder.castView(view, 2131689677, "field 'layoutInformation'");
    view = finder.findRequiredView(source, 2131689687, "field 'floatingActionButton'");
    target.floatingActionButton = finder.castView(view, 2131689687, "field 'floatingActionButton'");
    view = finder.findRequiredView(source, 2131689675, "field 'collapsingToolbar'");
    target.collapsingToolbar = finder.castView(view, 2131689675, "field 'collapsingToolbar'");
  }

  @Override public void unbind(T target) {
    target.etFirstName = null;
    target.etLastName = null;
    target.etPhone = null;
    target.etEmail = null;
    target.toolbar = null;
    target.header = null;
    target.layoutInformation = null;
    target.floatingActionButton = null;
    target.collapsingToolbar = null;
  }
}
