package com.example.android.day10_task1;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.android.day10_task1.Service.ChatHeadService;
import com.example.android.day10_task1.Service.ClockTimeService;

import java.util.HashMap;
import java.util.Map;

import static android.appwidget.AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT;
import static android.view.LayoutInflater.*;


public class ClockWidget extends AppWidgetProvider {

    private static final Map<Integer, DrawView> widgets = new HashMap<>();
    private static int nwidth = 200;
    private static int nheight = 200;
    private final static String RESIZE ="com.sec.android.widgetapp.APPWIDGET_RESIZE";
    private final static String ROW_WIDGET ="\"configwidget://widget/id/#\"";
    private final static String WIDGET_ID ="widgetId";
    private final static String WIDGET_SPAN_X ="widgetspanx";
    private final static String WIDGET_SPAN_Y ="widgetspany";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int id : appWidgetIds) {
            Bundle options = appWidgetManager.getAppWidgetOptions(id);
            onAppWidgetOptionsChanged(context, appWidgetManager, id, options);

        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {

        SharedPreferences.Editor editor = context.getSharedPreferences(
                ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE).edit();
        for (int widgetID : appWidgetIds) {
            editor.remove(ConfigActivity.WIDGET_COLOR_GLASS + widgetID);
            editor.remove(ConfigActivity.WIDGET_COLOR_SEND + widgetID);
            editor.remove(ConfigActivity.WIDGET_TIME + widgetID);
            DrawView  drawView = widgets.get(widgetID);
                      drawView.stopClock();
        }
        editor.commit();
        super.onDeleted(context, appWidgetIds);
    }
    @Override
    public void onEnabled(Context context) {}
    public static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                                    int widgetID,Bundle newOptions) {
        if(newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT) > 100 && newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH) > 120){
            nwidth   =(int) (newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)*1.4);
            nheight  =(int) (newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)* 2);
        }else{
            nwidth = 200;
            nheight = 200;
        }
        SharedPreferences sp = context.getSharedPreferences(
                ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
        String time = sp.getString(ConfigActivity.WIDGET_TIME + widgetID, null);
        if (time == null) return;
        int colorGlass = sp.getInt(ConfigActivity.WIDGET_COLOR_GLASS + widgetID, 0);
        int colorSend = sp.getInt(ConfigActivity.WIDGET_COLOR_SEND + widgetID, 0);
        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.clock_widget);
        DrawView drawView;
        if (!widgets.containsKey(widgetID)) {
            drawView = new DrawView(context, widgetID);
            if (colorGlass != 0)
                drawView.setGlassColor(colorGlass);
            if (colorSend != 0)
                drawView.setSandColor(colorSend);
            drawView.setDrawingCacheEnabled(true);
            drawView.buildDrawingCache(true);
            widgets.put(widgetID, drawView);
        } else
            drawView = widgets.get(widgetID);
        drawView.measure(View.MeasureSpec.makeMeasureSpec(nwidth, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(nheight, View.MeasureSpec.EXACTLY));
        drawView.layout(0, 0, drawView.getMeasuredWidthAndState(), drawView.getMeasuredHeight());
        Bitmap bitClock = drawView.getDrawingCache();
        widgetView.setImageViewBitmap(R.id.imageView, bitClock);

        Intent clickIntent = new Intent(context, ClockWidget.class);
        clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, widgetID, clickIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.imageView, pendingIntent);
        appWidgetManager.updateAppWidget(widgetID, widgetView);
    }

    public static void setAlarm(Context context, int appWidgetId, int updateRate) {
        Intent active = new Intent(context, ClockTimeService.class);
        active.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        active.putExtra(ClockTimeService.UPDATEROTATE,(long) updateRate);
        context.startService(active);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (RESIZE.equals(intent.getAction())) {
            handleTouchWiz(context, intent);
        }
        if ( intent.getAction()== null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
                SharedPreferences sp = context.getSharedPreferences(
                        ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
                String time = sp.getString(ConfigActivity.WIDGET_TIME + widgetId, null);
                DrawView drawView = widgets.get(widgetId);
                drawView.startClock(Integer.parseInt(time));
                if(Integer.parseInt(time)>1800)
                    setAlarm(context, widgetId, 10000);
                else
                    setAlarm(context, widgetId, 1000);
            }
        } else {
            super.onReceive(context, intent);
        }
    }

    @Override
    public void onDisabled(Context context) {
        context.stopService(new Intent(context, ClockTimeService.class));
        context.stopService(new Intent(context, ChatHeadService.class));
        super.onDisabled(context);
    }
    public static Bitmap RotateBitmap(Bitmap source) {
        Matrix matrix = new Matrix();
        matrix.postRotate(180);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public  void showChatHead(Context context, int appWidgetId) {
        context.startService(new Intent(context, ChatHeadService.class));
        context.stopService(new Intent(context, ClockTimeService.class));
        Bundle options = AppWidgetManager.getInstance(context).getAppWidgetOptions(appWidgetId);
        ClockWidget.updateWidget(context, AppWidgetManager.getInstance(context), appWidgetId, options);
    }
    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        updateWidget(context, appWidgetManager, appWidgetId, newOptions);
    }

    private void handleTouchWiz(Context context, Intent intent) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int appWidgetId = intent.getIntExtra(WIDGET_ID, 0);
        int widgetSpanX = intent.getIntExtra(WIDGET_SPAN_X, 0);
        int widgetSpanY = intent.getIntExtra(WIDGET_SPAN_Y, 0);
        if (appWidgetId > 0 && widgetSpanX > 0 && widgetSpanY > 0) {
            Bundle newOptions = new Bundle();
            newOptions.putInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT, widgetSpanY * 174);
            newOptions.putInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH, widgetSpanX * 200);
            onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        }
    }
     public void StopService(Context context) {
         context.stopService(new Intent(context, ClockTimeService.class));
     }
}

