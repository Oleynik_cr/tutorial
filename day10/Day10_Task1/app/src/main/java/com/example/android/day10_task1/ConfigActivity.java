package com.example.android.day10_task1;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.android.day10_task1.Service.ChatHeadService;

import yuku.ambilwarna.AmbilWarnaDialog;

public class ConfigActivity extends AppCompatActivity {

    private EditText editText;
    private int colorGlass;
    private int colorSend;
    private Button btColorGlass;
    private Button btColorSend;
    private int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    private Intent resultValue;
    private   DrawView drawView;

    public final static String WIDGET_PREF = "widget_pref";
    public final static String WIDGET_TIME = "widget_time_";
    public final static String WIDGET_COLOR_GLASS = "widget_color_glass_";
    public final static String WIDGET_COLOR_SEND = "widget_color_send_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        editText = (EditText) findViewById(R.id.editText);
        btColorGlass =(Button) findViewById(R.id.btColorGlass);
        btColorSend =(Button) findViewById(R.id.btColorSend);
        drawView = (DrawView) findViewById(R.id.view);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        setResult(RESULT_CANCELED, resultValue);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
    public  void onClick(View view) {
        switch (view.getId()){
            case R.id.btOk:
                if(!editText.getText().toString().equals("") && !editText.getText().toString().equals("0")) {
                    SharedPreferences sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(WIDGET_TIME + widgetID, editText.getText().toString());
                    editor.putInt(WIDGET_COLOR_GLASS + widgetID, getColorGlass());
                    editor.putInt(WIDGET_COLOR_SEND + widgetID, getColorSend());
                    editor.commit();
                    setResult(RESULT_OK, resultValue);
                    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
                    Bundle options = appWidgetManager.getAppWidgetOptions(widgetID);
                    ClockWidget.updateWidget(this, appWidgetManager, widgetID, options);
                    finish();
                }else {
                    Toast toast = Toast.makeText(this, R.string.input_number,Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            case R.id.btColorSend:
                getColor(true);
                break;
            case R.id.btColorGlass:
                getColor(false);
                break;
            default:
                break;
        }
    }

    private void getColor(final boolean choice){
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, 0xff0000ff, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog){
            }
            @Override
            public void onOk(AmbilWarnaDialog dialog, int colors) {
                if(choice) {
                    setColorSend(colors);
                    btColorSend.setBackgroundColor(colors);
                    drawView.setSandColor(colors);
                }else {
                    setColorGlass(colors);
                    btColorGlass.setBackgroundColor(colors);
                    drawView.setGlassColor(colors);
                }
            }
        });
        dialog.show();
    }

    public int getColorSend() {
        return colorSend;
    }

    public void setColorSend(int colorSend) {
        this.colorSend = colorSend;
    }

    public int getColorGlass() {
        return colorGlass;
    }

    public void setColorGlass(int colorGlass) {
        this.colorGlass = colorGlass;
    }
}

