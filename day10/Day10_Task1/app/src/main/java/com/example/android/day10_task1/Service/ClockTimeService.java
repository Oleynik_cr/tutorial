package com.example.android.day10_task1.Service;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.android.day10_task1.ClockWidget;
import com.example.android.day10_task1.R;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ClockTimeService extends Service {

    public static final String UPDATEROTATE = "updateRate";
    long updateRate = 1000;
    TimeDisplayTimerTask timeDisplayTimerTask;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;

    public ClockTimeService() {}
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int appWidgetId = intent.getExtras().getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID);
        updateRate = intent.getLongExtra(
                UPDATEROTATE, updateRate);
        Bundle options = AppWidgetManager.getInstance(this).getAppWidgetOptions(appWidgetId);
        if (mTimer != null) {
        } else {
            mTimer = new Timer();
        }
        mTimer.schedule(new TimeDisplayTimerTask(appWidgetId, options, this), 0, updateRate);
        return START_STICKY;
    }
    class TimeDisplayTimerTask extends TimerTask {
        int appWidgetId;
        Bundle options;
        Context context;

        public TimeDisplayTimerTask(int appWidgetId, Bundle options, Context context) {
            this.appWidgetId = appWidgetId;
            this.options = options;
            this.context = context;
        }
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    ClockWidget.updateWidget(context, AppWidgetManager.getInstance(context), appWidgetId, options);
                }
            });
        }
    }

    @Override
    public boolean stopService(Intent name) {
        mTimer.cancel();
        timeDisplayTimerTask = null;
        return super.stopService(name);
    }
    @Override
    public void onDestroy()
    {
        mTimer.cancel();
        timeDisplayTimerTask = null;

    }
}
