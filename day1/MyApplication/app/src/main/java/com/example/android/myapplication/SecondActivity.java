package com.example.android.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class SecondActivity extends Activity {


    private Button btnGo;
    private EditText text;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

                btnGo =  (Button) findViewById(R.id.btnGo);
                text  =  (EditText) findViewById(R.id.editText);

        btnGo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intent = new Intent();

                Person person = new Person(text.getText().toString(), "Oleynik");
                intent.putExtra(Person.class.getCanonicalName(), person);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }
}
