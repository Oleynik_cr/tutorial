package com.example.android.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
;
public class MainActivity extends Activity {


    private TextView textFromString;
    private TextView textFromPerson;
    private Intent   intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        textFromPerson = (TextView) findViewById(R.id.textFromPerson);



    }
    public void onClick(View view) {
        intent = new Intent(MainActivity.this, SecondActivity.class);

        startActivityForResult(intent,1);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
       Person person = data.getParcelableExtra(Person.class.getCanonicalName());
        textFromPerson.setText(person.firstName+" "+person.lastName);

    }


    }



