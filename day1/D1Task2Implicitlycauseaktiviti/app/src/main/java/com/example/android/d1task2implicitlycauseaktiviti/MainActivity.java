package com.example.android.d1task2implicitlycauseaktiviti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button implicitCall;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        implicitCall = (Button) findViewById(R.id.btnImplicitCall);


        implicitCall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                intent = new Intent("CallSecondActivity");
                startActivity(intent);
            }
        });

    }
}
