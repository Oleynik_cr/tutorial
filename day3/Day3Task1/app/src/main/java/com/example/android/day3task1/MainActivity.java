package com.example.android.day3task1;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;



public class MainActivity extends AppCompatActivity {
    private EditText text;
    public static final String KEY = "key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText);

    }


    public void onClick(View view) {

        Runnable runnable = new Runnable() {
            public void run() {
                Message msg = handler.obtainMessage();
                Bundle bundle = new Bundle();

                bundle.putString(KEY, factorial(Integer.parseInt(text.getText().toString())));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            String date = bundle.getString(KEY);
           text.setText(date);
        }
    };

    public String factorial(int x){
        if (x<0)
            return getString(R.string.incorrect_value);
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return getString(R.string.resulted) + fact;
    }

}
