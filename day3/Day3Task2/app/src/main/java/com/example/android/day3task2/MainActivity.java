package com.example.android.day3task2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.ViewAsserts;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText text;
    private  MyTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.editText);

    }



    public void onClick(View view) {
        myTask = new MyTask();
        myTask.execute(text.getText().toString());

    }




    class MyTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            return  factorial(Integer.parseInt(params[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            text.setText(result);

        }
    }
    public String factorial(int x){
        if (x<0)
           return getString(R.string.incorrect_value);
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return getString(R.string.resulted)+fact;
    }



}
