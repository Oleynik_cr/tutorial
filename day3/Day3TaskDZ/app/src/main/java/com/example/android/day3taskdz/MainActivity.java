package com.example.android.day3taskdz;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private MyTask   myTask;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        myTask = (MyTask)getLastCustomNonConfigurationInstance();
        if(myTask==null)
        {
            myTask = new MyTask();
            myTask.execute();
        }

        myTask.setTextView(textView);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        myTask.setNullTextView();
        return myTask;
    }

    public void onClick(View view) {
        myTask = new MyTask();
        myTask.execute();

    }
    class MyTask extends AsyncTask<String, Void, String> {

        private TextView textView;

        public void setTextView(TextView textView)
        {
            this.textView = textView;
        }

        public void setNullTextView()
        {
            textView = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            for(int i = 0; i < 5;i++)
            {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return "Data";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(textView != null)
               textView.setText(result);
        }
    }
}
