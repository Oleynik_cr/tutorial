package com.example.android.day3task3;


import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by android on 8/12/15.
 */

public class FactorialLoader extends AsyncTaskLoader {
    public final static String KEY = "key";


     private String number;
    public FactorialLoader(Context context, Bundle args) {
        super(context);
        number = args.getString(KEY);
    }
    public String loadInBackground() {

        return factorial(Integer.parseInt(number));
    }
    public String factorial(int x){
        if (x<0)
            return getContext().getString(R.string.incorrect_value);
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return getContext().getString(R.string.resulted)+fact;
    }


}