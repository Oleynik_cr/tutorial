package com.example.android.day3task3;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks {
    private EditText text;
    static final int LOADER_ID = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText);


    }
    public Loader onCreateLoader(int id, Bundle args) {
        return new FactorialLoader(this, args);
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        text.setText((String) data);
    }


    public void onLoaderReset(Loader loader) {

    }
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putString(FactorialLoader.KEY, text.getText().toString());
        Loader  loader = getLoaderManager().restartLoader(LOADER_ID, bundle,
                this);
        loader.forceLoad();
    }
}


