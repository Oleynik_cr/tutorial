package com.example.android.day8task3;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FactoriacService extends Service {

    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_SET_VALUE = 3;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    ArrayList<Messenger> mClients = new ArrayList<>();
    ExecutorService executorService = Executors.newFixedThreadPool(1);

    public FactoriacService() {}

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    Run mr = new Run(msg.arg2);
                    executorService.execute(mr);

                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    class Run implements Runnable {
        int number;
        public Run(int number) {
            this.number = number;
        }
        public void run() {

            setToСlients(factorial(number));
        }
    }

    public String factorial(int x){
        if (x<0)
            return "incorrect value";
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return  fact+"";
    }

    private void setToСlients(String result)
    {
        for (int i=mClients.size()-1; i>=0; i--) {
            try {
                mClients.get(i).send(Message.obtain(null,
                        MSG_SET_VALUE,result));
            } catch (RemoteException e) {

                mClients.remove(i);
            }
        }
    }
}
