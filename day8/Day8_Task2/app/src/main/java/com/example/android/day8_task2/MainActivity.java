package com.example.android.day8_task2;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private PendingIntent pi;
    private Intent intent;
    private  boolean isLive;
    public  final static int TASK_CODE = 1;
    public final static String PARAM_PINTENT = "pendingIntent";
    public final static String PARAM_RESULT = "result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        isLive = true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                if(isLive) {
                    intent = new Intent(this, ConsiderService.class);
                    pi = createPendingResult(MainActivity.TASK_CODE, new Intent(), 0);
                    intent.putExtra(MainActivity.PARAM_PINTENT, pi);
                    startService(intent);
                    isLive = false;
                }
                break;
            case R.id.btnStop:
                if(intent != null) {
                    stopService(intent);
                    isLive = true;
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == MainActivity.TASK_CODE)
                textView.setText(""+data.getIntExtra(MainActivity.PARAM_RESULT,1));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(intent != null)
            stopService(intent);
    }
}
