package com.example.android.day8_task2;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ConsiderService extends Service {
    private boolean isLive = false;
    private static final int NOTIFY_ID = 101;
    private NotificationCompat.Builder mBuilder;

    public ConsiderService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        isLive = true;
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        PendingIntent pi = intent.getParcelableExtra(MainActivity.PARAM_PINTENT);
        mBuilder =
                new NotificationCompat.Builder(this);
        Run mr = new Run(pi);
        executorService.execute(mr);
        return START_NOT_STICKY;
    }

    class Run implements Runnable {
        int number = 0;
        PendingIntent pi;
        Intent intent;
        public Run(PendingIntent pi) {
            this.pi = pi;
        }
        public void run() {
            while (isLive) {
                number++;
                try {
                    Thread.sleep(1000);
                    sendNotif(number);
                    intent = new Intent().putExtra(MainActivity.PARAM_RESULT, number);
                    pi.send(ConsiderService.this, Activity.RESULT_OK, intent);
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isLive = false;
    }

    void sendNotif(int number) {
        mBuilder
                .setLocalOnly(true)
                .setSmallIcon(R.drawable.abc_btn_borderless_material)
                .setContentTitle("Result")
                .setContentText(number+"");
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(NOTIFY_ID, mBuilder.build());
    }
}
