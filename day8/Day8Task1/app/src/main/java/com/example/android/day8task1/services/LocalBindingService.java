package com.example.android.day8task1.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.example.android.day8task1.LocalBindingActivity;
import com.example.android.day8task1.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LocalBindingService extends Service {

    private LocalBindingActivity localBindingActivity;
    final android.os.Handler handler = new android.os.Handler();
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public LocalBindingService getService() {
            return LocalBindingService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void getCalculate(int number) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Run mr = new Run(number);
        executorService.execute(mr);
    }

    class Run implements Runnable {
        int number;

        public Run(int number) {
            this.number = number;
        }

        public void run() {
            final String result =  MainActivity.factorial(number);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    localBindingActivity.SetResult(result);
                }
            });

        }
    }
    public void setLocalBindingActivity(LocalBindingActivity localBindingActivity) {
        this.localBindingActivity = localBindingActivity;
    }
}

