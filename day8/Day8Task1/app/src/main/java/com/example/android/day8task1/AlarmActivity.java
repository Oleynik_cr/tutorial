package com.example.android.day8task1;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.day8task1.services.AlarmService;
import com.example.android.day8task1.services.IntentSer;


public class AlarmActivity extends AppCompatActivity {
    private EditText text;
    private AlarmManagerBroadcastReceiver alarm;
    public static final String ACTION1 = "com.example.RESPONSE1";
    public static final String ACTION2 = "com.example.RESPONSE2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servise);
        text = (EditText) findViewById(R.id.editText);
        alarm = new AlarmManagerBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(ACTION1);
        intentFilter.addAction(ACTION2);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(alarm, intentFilter);
    }

    public void OnClick(View view) {
        if(!text.getText().toString().equals("")) {
            int number = Integer.parseInt(text.getText().toString());
            Intent intent = new Intent(ACTION1);
            intent.putExtra(MainActivity.PARAMETR, number);
            sendBroadcast(intent);
        }else{
                Toast toast = Toast.makeText(this, "Input Number",Toast.LENGTH_LONG);
                toast.show();
            }
        }

    public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
        int number;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(AlarmActivity.ACTION1)) {
                number = intent.getExtras().getInt(MainActivity.PARAMETR);
                setOnetimeTimer(context);
            }else {
                String result = intent.getStringExtra(IntentServiceActivity.EXTRA_KEY_OUT);
                text.setText(result);
            }
        }

        public void setOnetimeTimer(Context context) {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, AlarmService.class);
            intent.putExtra(MainActivity.PARAMETR, number);
            PendingIntent pi = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 10, pi);
        }

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(alarm);
    }
}
