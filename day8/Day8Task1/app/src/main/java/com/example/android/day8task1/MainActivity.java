package com.example.android.day8task1;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    public  final static int TASK_CODE = 1;
    public final static String PARAMETR = "parametr";
    public final static String PARAM_PINTENT = "pendingIntent";
    public final static String PARAM_RESULT = "result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onclick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.pendingIntentService:
                intent = new Intent(MainActivity.this, PendingIntentServiceActivity.class);
                break;
            case R.id.intentService:
                intent = new Intent(MainActivity.this, IntentServiceActivity.class);
                break;
            case R.id.alarm:
                intent = new Intent(MainActivity.this, AlarmActivity.class);
                break;
            case R.id.localBind:
                intent = new Intent(MainActivity.this, LocalBindingActivity.class);
                break;
            case R.id.notLocalBinding:
                intent = new Intent(MainActivity.this, NotLocalBindibgAvtivity.class);
                break;
            default:
                break;
        }
        startActivity(intent);
    }

    public static String factorial(int x){
        if (x<0)
            return "incorrect value";
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return  fact+"";
    }

}
