package com.example.android.day8task1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.day8task1.services.IntentSer;

public class IntentServiceActivity extends AppCompatActivity {

    private MyBroadcastReceiver myBroadcastReceiver;
    public static final String ACTION = "com.example.RESPONSE";
    public static final String EXTRA_KEY_OUT = "EXTRA_OUT";
    private   EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servise);
        text = (EditText) findViewById(R.id.editText);
        myBroadcastReceiver = new MyBroadcastReceiver();

        IntentFilter intentFilter = new IntentFilter(ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(myBroadcastReceiver, intentFilter);
    }

    public void OnClick(View view) {
        if(!text.getText().toString().equals("")) {
            int number = Integer.parseInt(text.getText().toString());
            Intent intent = new Intent(this, IntentSer.class).putExtra(MainActivity.PARAMETR, number);
            startService(intent);
        }else {
            Toast toast = Toast.makeText(this, "Input Number", Toast.LENGTH_LONG);
            toast.show();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myBroadcastReceiver);
    }

    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String result = intent.getStringExtra(EXTRA_KEY_OUT);
            text.setText(result);
        }
    }

}
