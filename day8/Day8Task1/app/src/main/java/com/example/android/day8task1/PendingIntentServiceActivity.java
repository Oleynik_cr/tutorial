package com.example.android.day8task1;


import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.day8task1.services.PendingIntentService;
public class PendingIntentServiceActivity extends AppCompatActivity {
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servise);
        text = (EditText) findViewById(R.id.editText);

    }
    public void OnClick(View view) {
        PendingIntent pi;
        Intent intent;
        if (!text.getText().toString().equals("")) {
            int number = Integer.parseInt(text.getText().toString());
            intent = new Intent(this, PendingIntentService.class).putExtra(MainActivity.PARAMETR, number);
            pi = createPendingResult(MainActivity.TASK_CODE, intent, 0);
            intent.putExtra(MainActivity.PARAM_PINTENT, pi);
            startService(intent);
        } else {
            Toast toast = Toast.makeText(this, "Input Number", Toast.LENGTH_LONG);
            toast.show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if(requestCode == MainActivity.TASK_CODE)
                text.setText(data.getStringExtra(MainActivity.PARAM_RESULT));
        }
    }
}
