package com.example.android.day8task1.services;

import android.app.IntentService;
import android.content.Intent;

import com.example.android.day8task1.IntentServiceActivity;
import com.example.android.day8task1.MainActivity;

/**
 * Created by android on 15/09/15.
 */
public class IntentSer extends IntentService {

    public IntentSer() {
        super("myname");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int numbre = intent.getIntExtra(MainActivity.PARAMETR, 0);
        Intent intentResponse = new Intent();
        intentResponse.setAction(IntentServiceActivity.ACTION);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        intentResponse.putExtra(IntentServiceActivity.EXTRA_KEY_OUT, MainActivity.factorial(numbre));
        sendBroadcast(intentResponse);
    }
}
