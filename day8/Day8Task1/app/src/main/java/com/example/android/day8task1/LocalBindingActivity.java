package com.example.android.day8task1;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.day8task1.services.LocalBindingService;

public class LocalBindingActivity extends AppCompatActivity {
    private EditText text;
    LocalBindingService localBindigService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servise);
        text = (EditText) findViewById(R.id.editText);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, LocalBindingService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    public void OnClick(View view) {
        int number;
        if (!text.getText().toString().equals("")) {
            number = Integer.parseInt(text.getText().toString());
            localBindigService.getCalculate(number);
        } else {
            Toast toast = Toast.makeText(this, "Input Number", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            LocalBindingService.LocalBinder binder = (LocalBindingService.LocalBinder) service;
            localBindigService = binder.getService();
            localBindigService.setLocalBindingActivity(LocalBindingActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    public void SetResult(String result){
        text.setText(result);
    }
}