package com.example.android.day8task1.services;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.android.day8task1.MainActivity;
import com.example.android.day8task1.PendingIntentServiceActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class PendingIntentService extends Service {

    public PendingIntentService() {
    }
    public void onCreate() {
        super.onCreate();

    }
    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        int number = intent.getIntExtra(MainActivity.PARAMETR, 1);
        PendingIntent pi = intent.getParcelableExtra(MainActivity.PARAM_PINTENT);
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Run mr = new Run(number,pi);
        executorService.execute(mr);
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class Run implements Runnable {
        int number;
        PendingIntent pi;
        public Run(int number, PendingIntent pi) {
            this.number = number;
            this.pi = pi;
        }
        public void run() {
              String result = MainActivity.factorial(number);
            Intent intent = new Intent().putExtra(MainActivity.PARAM_RESULT,result);
            try {
                pi.send(PendingIntentService.this, Activity.RESULT_OK, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
    }

}
