package com.example.android.day8task1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.android.day8task1.services.NotifandService;

/**
 * Created by android on 15/09/15.
 */
public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent pushIntent = new Intent(context, NotifandService.class);
            context.startService(pushIntent);
        }
    }

}