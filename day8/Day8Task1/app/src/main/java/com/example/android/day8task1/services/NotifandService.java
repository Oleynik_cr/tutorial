package com.example.android.day8task1.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.android.day8task1.MainActivity;
import com.example.android.day8task1.R;

import java.util.concurrent.ThreadLocalRandom;

public class NotifandService extends Service {
    private static final int NOTIFY_ID = 101;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendNotif();
        return Service.START_STICKY;
    }
    void sendNotif() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.abc_btn_borderless_material)
                        .setContentTitle("Result")
                        .setContentText(factorial());
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(NOTIFY_ID, mBuilder.build());
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public String factorial(){
        int x = (int)(Math.random() * ((10 - 1) + 1));
        if (x<0)
            return "incorrect value";
        int fact = 1;
        for (int i=2; i<=x; i++)
            fact= fact * i;
        return  fact+"";
    }
}
