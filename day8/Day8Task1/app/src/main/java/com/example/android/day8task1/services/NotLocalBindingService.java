package com.example.android.day8task1.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import com.example.android.day8task1.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class NotLocalBindingService extends Service {
    public final static String BROADCAST_ACTION = "servicebackbroadcast";
    public NotLocalBindingService() {}

    public int onStartCommand(Intent intent, int flags, int startId) {
        int number =  intent.getIntExtra(MainActivity.PARAMETR, 0);
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Run mr = new Run(number);
        executorService.execute(mr);
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class Run implements Runnable {
        int number;
        public Run(int number) {
            this.number = number;
        }

        public void run() {
            Intent intent = new Intent(BROADCAST_ACTION);
            intent.putExtra(MainActivity.PARAM_RESULT, MainActivity.factorial(number));
            sendBroadcast(intent);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
    }
}
