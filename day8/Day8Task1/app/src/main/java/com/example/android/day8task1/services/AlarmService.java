package com.example.android.day8task1.services;

import android.app.IntentService;
import android.content.Intent;


import com.example.android.day8task1.AlarmActivity;
import com.example.android.day8task1.IntentServiceActivity;
import com.example.android.day8task1.MainActivity;


public class AlarmService extends IntentService {

    public AlarmService() {
        super("myname");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int numbre = intent.getIntExtra(MainActivity.PARAMETR, 0);
        Intent intentResponse = new Intent();
        intentResponse.setAction(AlarmActivity.ACTION2);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        intentResponse.putExtra(IntentServiceActivity.EXTRA_KEY_OUT, MainActivity.factorial(numbre));
        sendBroadcast(intentResponse);
    }
}
