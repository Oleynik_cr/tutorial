package com.example.android.day8task1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.day8task1.services.NotLocalBindingService;

public class NotLocalBindibgAvtivity extends AppCompatActivity {
    private EditText text;
    BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servise);
        text = (EditText) findViewById(R.id.editText);
        broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String result = intent.getStringExtra(MainActivity.PARAM_RESULT);
                text.setText(result);
            }
        };

        IntentFilter intFilt = new IntentFilter(NotLocalBindingService.BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, intFilt);
    }

    public void OnClick(View view) {
        int number;
        if(!text.getText().toString().equals("")) {
            number = Integer.parseInt(text.getText().toString());
            startService(new Intent(this, NotLocalBindingService.class).putExtra(MainActivity.PARAMETR, number));
        }else {
            Toast toast = Toast.makeText(this, "Input Number",Toast.LENGTH_LONG);
            toast.show();
        }
    }
     @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}
