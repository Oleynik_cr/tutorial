package com.example.android.day5task1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by android on 8/14/15.
 */
public class GetDataFromExpandableList {

    String[] groups = new String[] {"Man", "Women"};
    String[] Man = new String[] {"Иван", "Петр", "Антон", "Борис","Костя"};
    String[] Woman = new String[] {"Марья", "Даша", "Анна"};


    ArrayList<Map<String, String>> groupData;


    ArrayList<Map<String, String>> childDataItem;


    ArrayList<ArrayList<Map<String, String>>> childData;

    Map<String, String> m;

    public   ArrayList<Map<String, String>> getGroups() {


        groupData = new ArrayList<Map<String, String>>();
        for (String group : groups) {

            m = new HashMap<String, String>();
            m.put("groupName", group);
            groupData.add(m);
        }
        return  groupData;
    }


        public   ArrayList<ArrayList<Map<String, String>>> getNames()  {

            childData = new ArrayList<ArrayList<Map<String, String>>>();


            childDataItem = new ArrayList<Map<String, String>>(); // заполняем список аттрибутов для каждого элемента
            for (String NameMan : Man) {
                m = new HashMap<String, String>();
                m.put("Name", NameMan);
                childDataItem.add(m);
            }

            childData.add(childDataItem);


            childDataItem = new ArrayList<Map<String, String>>();
            for (String NameWoman : Woman) {
                m = new HashMap<String, String>();
                m.put("Name", NameWoman);
                childDataItem.add(m);
            }
             childData.add(childDataItem);
            return childData;
        }

    }



