package com.example.android.day5task1;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;

import com.example.android.day5task1.fragment.SimpleListFragment;

import java.util.ArrayList;
import java.util.Map;

public class Activity2 extends Activity {
    private ListView listView;
    private GridView gridView;
    private Spinner  spinner;
    private ExpandableListView expandableListView;
    private ArrayAdapter<String> adapter;
  public static final   String[] NAMES = { "Иван", "Марья", "Петр", "Антон", "Даша", "Борис",
            "Костя", "Игорь", "Анна", "Денис", "Андрей" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        listView = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);
        spinner  = (Spinner)  findViewById(R.id.spinner);
        spinner.setVisibility(View.GONE);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        showCorrectView();
    }


    private void showCorrectView()
    {
        switch (getIntent().getStringExtra(MainActivity.KEY_INTENT))
        {
            case "ListView":
                   showListView();
                break;
            case "GridView":
                    showGridView();
                break;
            case "Spinner":
                    showSpinner();
                break;
            case "ListActivity":
                    showListActivity();
                break;
            case "ListFragment":
                    showListFragment();
                break;
            case "ExpandableListView":
                    showExpandableListView();
                break;
            default:
                break;

        }
    }




    private void showListView() {
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, NAMES);
                 listView.setAdapter(adapter);
    }

    private void showGridView() {
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, NAMES);
        gridView.setAdapter(adapter);
    }
    private void showSpinner() {

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, NAMES);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(adapter);
    }
    private void showListActivity() {
        Intent intent = new Intent(Activity2.this, ListActivity.class);
        startActivity(intent);
    }
    private void showListFragment() {

        FragmentManager fm = getFragmentManager();

        if (fm.findFragmentById(android.R.id.content) == null) {
            SimpleListFragment list = new SimpleListFragment();
            fm.beginTransaction().add(android.R.id.content, list).commit();
        }
    }
    private void showExpandableListView() {
        GetDataFromExpandableList  getDataFromExpandableList = new GetDataFromExpandableList();
        ArrayList<Map<String, String>> groupData = getDataFromExpandableList.getGroups();
        ArrayList<ArrayList<Map<String, String>>> childData = getDataFromExpandableList.getNames();
        String childFrom[] = new String[] {"Name"};
        int childTo[] = new int[] {android.R.id.text1};

        String groupFrom[] = new String[] {"groupName"};

        int groupTo[] = new int[] {android.R.id.text1};
        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                groupFrom,
                groupTo,
                childData,
                android.R.layout.simple_list_item_1,
                childFrom,
                childTo);

        expandableListView.setAdapter(adapter);

    }


}
