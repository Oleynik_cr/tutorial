package com.example.android.day5task2.saportClassForExpListView;

/**
 * Created by android on 8/14/15.
 */
public class Child {

    private String Name;
    private int Image;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }
}
