package com.example.android.day5task2;

/**
 * Created by android on 8/14/15.
 */
public class ViewItem
{
    private String name;
    private int image;


    public ViewItem() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
