package com.example.android.day5task2.saportClassForExpListView;

import com.example.android.day5task2.R;

import java.util.ArrayList;


public class GetData {


  public static final   String GROUP_NAMES[] = { "GROUP A", "GROUP B", "GROUP C", "GROUP D"
            };

  public static final   String COUNTRY_NAMES[] = { "Abkhazia", "Afghanistan", "Aland", "Albania",
                               "Algeria", "American_Samoa","Andorra","Angola",
                               "Anguilla", "Antarctica","Antigua_and_Barbuda","Argentina",
                               "Armenia", "Aruba","Australia","Argentina",
    };

  public static final   int IMAGES[] = { R.drawable.abkhazia, R.drawable.afghanistan,
            R.drawable.aland, R.drawable.albania, R.drawable.algeria,
            R.drawable.american_samoa, R.drawable.andorra, R.drawable.angola,
            R.drawable.anguilla, R.drawable.antarctica, R.drawable.antigua_and_barbuda,
            R.drawable.argentina, R.drawable.armenia, R.drawable.aruba,
            R.drawable.australia, R.drawable.austria
            };

    public ArrayList<Group> SetStandardGroups() {



        ArrayList<Group> list = new ArrayList<Group>();

        ArrayList<Child> ch_list;

        int size = 4;
        int j = 0;

        for (String group_name : GROUP_NAMES) {
            Group gru = new Group();
            gru.setName(group_name);

            ch_list = new ArrayList<Child>();
            for (; j < size; j++) {
                Child ch = new Child();
                ch.setName(COUNTRY_NAMES[j]);
                ch.setImage(IMAGES[j]);
                ch_list.add(ch);
            }
            gru.setItems(ch_list);
            list.add(gru);

            size = size + 4;
        }

        return list;
    }
}
