package com.example.android.day5task2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.android.day5task2.ViewItem;
import com.example.android.day5task2.R;

import java.util.List;


public class ListViewAdapter extends AbstractAdapter {
    public ListViewAdapter(Context context, List<ViewItem> mData) {
        super(context, mData);
    }



    static class ViewHolder {
        TextView  txtItem;
        ImageView imageView;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;


        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_view_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.txtItem = (TextView) convertView.findViewById(R.id.txtItem);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ViewItem viewItem = getItem(position);

        if(viewItem != null) {
            viewHolder.txtItem.setText(viewItem.getName());
            viewHolder.imageView.setImageResource(viewItem.getImage());
        }



        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return null;
    }


}

