package com.example.android.day5task2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_INTENT = "key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view)
    {
        Intent intent = new Intent(MainActivity.this, Activity2.class);
        switch (view.getId())
        {
            case R.id.button:
                intent.putExtra(KEY_INTENT,"ListView");
                break;
            case R.id.button1:
                intent.putExtra(KEY_INTENT,"GridView");
                break;
            case R.id.button2:
                intent.putExtra(KEY_INTENT,"Spinner");
                break;
            case R.id.button5:
                intent.putExtra(KEY_INTENT,"ExpandableListView");
                break;
            default:
                break;

        }
        startActivity(intent);
    }
}
