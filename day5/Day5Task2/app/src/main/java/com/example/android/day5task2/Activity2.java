package com.example.android.day5task2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;

import android.widget.Spinner;

import com.example.android.day5task2.adapter.ExpandListAdapter;
import com.example.android.day5task2.adapter.ListViewAdapter;
import com.example.android.day5task2.adapter.GridViewAdapter;
import com.example.android.day5task2.adapter.SpinnerAdapter;
import com.example.android.day5task2.saportClassForExpListView.GetData;

import java.util.ArrayList;
import java.util.List;

public class Activity2 extends Activity {
    private ListView listView;
    private GridView gridView;
    private Spinner  spinner;
    private ExpandableListView expandableListView;
    private List<GridViewItem> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        listView = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);
        spinner  = (Spinner)  findViewById(R.id.spinner);
        spinner.setVisibility(View.GONE);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        showCorrectView();
    }


    private void showCorrectView()
    {
        switch (getIntent().getStringExtra(MainActivity.KEY_INTENT))
        {
            case "ListView":
                   showListView();
                break;
            case "GridView":
                    showGridView();
                break;
            case "Spinner":
                    showSpinner();
                break;
            case "ExpandableListView":
                    showExpandableListView();
                break;
            default:
                break;

        }
    }




    private void showListView() {
        ListViewAdapter adapter = new ListViewAdapter(this,  getDataLists());
        listView.setAdapter(adapter);

    }
    private void showGridView() {

        GridViewAdapter mAdapter = new GridViewAdapter(this, getDataLists());
        gridView.setAdapter(mAdapter);
    }
    private void showSpinner() {
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, getDataLists());
        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(spinnerAdapter);

    }
    private void showExpandableListView() {
        GetData getData = new GetData();
        ExpandListAdapter expAdapter = new ExpandListAdapter(this, getData.SetStandardGroups());
        expandableListView.setAdapter(expAdapter);
    }

    private List<ViewItem> getDataLists() {
        List<ViewItem> mItems = new ArrayList<>();

        for (int i = 0; i < GetData.COUNTRY_NAMES.length; i++) {
         ViewItem viewItem = new ViewItem();
            viewItem.setImage(GetData.IMAGES[i]);
            viewItem.setName(GetData.COUNTRY_NAMES[i]);
          mItems.add(viewItem);
        }
        return mItems;
    }

}
