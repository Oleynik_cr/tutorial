package com.example.android.day5task2.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.android.day5task2.ViewItem;

import java.util.List;

/**
 * Created by android on 17.08.15.
 */
public abstract class AbstractAdapter extends ArrayAdapter<ViewItem> {


    public AbstractAdapter(Context context, List<ViewItem> mData) {
        super(context,0, mData);

    }

    @Override
    abstract public View getView(int position, View convertView, ViewGroup viewGroup);


    @Override
    abstract  public View getDropDownView(int position, View convertView,
                                ViewGroup parent);

}
