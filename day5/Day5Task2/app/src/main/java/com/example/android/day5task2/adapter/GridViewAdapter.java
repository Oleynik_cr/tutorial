package com.example.android.day5task2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.day5task2.ViewItem;
import com.example.android.day5task2.R;

import java.util.List;

public class GridViewAdapter extends AbstractAdapter {


    public GridViewAdapter(Context context, List<ViewItem> mData) {
        super(context, mData);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.gridview_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            convertView.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        ViewItem viewItem = getItem(position);
          if(viewItem != null) {
              viewHolder.ivIcon.setImageResource(viewItem.getImage());
              viewHolder.tvTitle.setText(viewItem.getName());
          }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return null;
    }


    private static class ViewHolder {
        ImageView ivIcon;
        TextView tvTitle;
    }
}