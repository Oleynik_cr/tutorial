package com.example.android.day6tasksall.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class DaoArea extends DAO<Person> {


    public DaoArea(Context ctx) {
        super(ctx);

    }

    @Override
    public Cursor getAll() {
        open();
        return database.query(DatabaseHelper.TABLE_AREA, null, null, null, null, null, null);

    }

    @Override
    public void create(Person person) {
        open();
        ContentValues   cv = new ContentValues();
        cv.put(DatabaseHelper.KEY_NAME, person.getName());
        database.insert(DatabaseHelper.TABLE_AREA, null, cv);
        close();

    }

    @Override
    public void delete(long id) {
        open();
        database.delete(DatabaseHelper.TABLE_AREA, DatabaseHelper.KEY_ID + "=" + id, null);
        close();
    }

    @Override
    public void update(Person person) {
        open();
        ContentValues    cv = new ContentValues();
        cv.put(DatabaseHelper.KEY_NAME, person.getName());
        boolean b = database.update(DatabaseHelper.TABLE_AREA, cv, DatabaseHelper.KEY_ID + "=" + person.getId(), null) > 0;
        close();

    }





}
