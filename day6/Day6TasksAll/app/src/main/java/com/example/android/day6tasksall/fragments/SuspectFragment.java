package com.example.android.day6tasksall.fragments;

import android.database.Cursor;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;



import com.example.android.day6tasksall.R;
import com.example.android.day6tasksall.dataBase.AsyncSelect;
import com.example.android.day6tasksall.dataBase.BaseCursorLoader;
import com.example.android.day6tasksall.dataBase.DAO;
import com.example.android.day6tasksall.dataBase.DaoSuspend;
import com.example.android.day6tasksall.dataBase.DatabaseHelper;
import com.example.android.day6tasksall.dataBase.Person;
import com.example.android.day6tasksall.dialogs.DialogSuspect;
//
public class SuspectFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,ReloadData {


    private   SimpleCursorAdapter scAdapter;
    private   ListView            lvData;
    private   DAO                 dao;
    private   Button              button;
    private   Loader<Cursor>      cursorLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.name_people_fragment, container, false);
        setRetainInstance(true);

         dao = new DaoSuspend(getActivity());

        button = (Button) view.findViewById(R.id.dtnAdd);


        String[] from = new String[] {DatabaseHelper.KEY_NAME, DatabaseHelper.KEY_ID };
        int[] to = new int[] { R.id.textView2, R.id.textView1 };


        scAdapter = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);
        lvData = (ListView) view.findViewById(R.id.lvData);
        lvData.setAdapter(scAdapter);
        registerForContextMenu(lvData);


        getLoaderManager().initLoader(0, null, this);


        lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

            }

        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogSuspect dialogSuspect = new DialogSuspect(null, DatabaseHelper.CONTEXTMENU_OPTION1, cursorLoader);
                dialogSuspect.show(getFragmentManager(), null);
            }
        });

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
              cursorLoader = new BaseCursorLoader(getActivity(), dao );
        return cursorLoader;


    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        scAdapter.swapCursor(cursor);
       // cursorLoader.onContentChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        scAdapter.swapCursor(null);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        dao.close();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION1, 0, "Delete");
        menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION2, 0, "Update");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (getUserVisibleHint()) {
            AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();


            switch (item.getItemId()) {
                case DatabaseHelper.CONTEXTMENU_OPTION1:

                    deleteItem(menuInfo.position);
                    break;
                case DatabaseHelper.CONTEXTMENU_OPTION2:
                    updateItem(menuInfo.position);
                    break;
                default:
                    break;
            }

            return true;
        } else
            return false;
    }


    private void deleteItem(int position) {
      Cursor   cursor = (Cursor) scAdapter.getItem(position);
        String id = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
        new AsyncSelect<Person>(dao, null,DatabaseHelper.KEY_DELETE,Long.parseLong(id),cursorLoader)
                .execute();

    }

    private void updateItem(int position) {
        Person person  = new Person();
     Cursor   cursor = (Cursor)scAdapter.getItem(position);
        String id = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
        String name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_NAME));
        person.setId(Long.parseLong(id));
        person.setName(name);
        DialogSuspect dialogSuspect = new DialogSuspect(person, DatabaseHelper.CONTEXTMENU_OPTION2,cursorLoader);
        dialogSuspect.show(getFragmentManager(), null);

    }

    @Override
    public void reload() {
        cursorLoader.onContentChanged();
    }
}