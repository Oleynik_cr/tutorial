package com.example.android.day6tasksall.dialogs;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.android.day6tasksall.R;
import com.example.android.day6tasksall.dataBase.AsyncSelect;
import com.example.android.day6tasksall.dataBase.DAO;
import com.example.android.day6tasksall.dataBase.DaoOfficer;
import com.example.android.day6tasksall.dataBase.DatabaseHelper;
import com.example.android.day6tasksall.dataBase.Person;


public class DialogOfficer extends DialogFragment implements OnClickListener {

    private Activity activity;
    private EditText editText;

    private  long id;
    private  DAO daoOfficer;
    private  Person person;
    private  int key;
    private  Loader loader;

    public DialogOfficer(Person person,int key, Loader loader) {
        this.person = person;
        this.key    = key;
        this.loader = loader;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activity = getActivity();




    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        getDialog().setTitle("Input Name");
        View v = inflater.inflate(R.layout.dialog_people_name,container, false);

        editText = (EditText) v.findViewById(R.id.editText);
        v.findViewById(R.id.btnAdd).setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);
        daoOfficer = new DaoOfficer(activity);
        if(key == 2)
            editText.setText(person.getName());

        return v;
    }

    public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.btnAdd :
                    correctChis();
                    dismiss();
                    break;
                case R.id.btnCancel :
                     dismiss();
                    break;
                default:
                    break;

            }

    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }


    private void correctChis()
    {


        switch (key)
        {
            case 1 :

               Person personAdd = new Person();
                personAdd.setName(editText.getText().toString());
                new AsyncSelect<Person>(daoOfficer, personAdd, DatabaseHelper.KEY_CREATE, null, loader)
                        .execute();

                break;
            case 2 :

                person.setName(editText.getText().toString());
                new AsyncSelect<Person>(daoOfficer, person, DatabaseHelper.KEY_UPDATE,null, loader)
                        .execute();

                break;
            default:
                break;

        }
    }
}