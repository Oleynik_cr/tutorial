package com.example.android.day6tasksall.dialogs;


import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.android.day6tasksall.R;
import com.example.android.day6tasksall.dataBase.Arrest;
import com.example.android.day6tasksall.dataBase.AsyncSelect;
import com.example.android.day6tasksall.dataBase.BaseCursorLoader;
import com.example.android.day6tasksall.dataBase.DAO;
import com.example.android.day6tasksall.dataBase.DaoArea;
import com.example.android.day6tasksall.dataBase.DaoArrest;
import com.example.android.day6tasksall.dataBase.DaoOfficer;
import com.example.android.day6tasksall.dataBase.DaoSuspend;
import com.example.android.day6tasksall.dataBase.DatabaseHelper;
import com.example.android.day6tasksall.dataBase.Person;

public class DialogArrest extends DialogFragment implements OnClickListener {


    private   DAO          daoOfficers;
    private   DAO          daoSuspects;
    private   DAO          daoArea;
    private Loader<Cursor> cursorLoaderOfficers;
    private Loader<Cursor> cursorLoaderSuspects;
    private Loader<Cursor> cursorLoaderArea;
    private Spinner        spinnerOfficers;
    private Spinner        spinnerSuspects;
    private Spinner        spinnerArea;
    private SimpleCursorAdapter officersAdapter;
    private SimpleCursorAdapter suspectsAdapter;
    private SimpleCursorAdapter areaAdapter;
    private String              idOfficer;
    private String              idSuspend;
    private String              idArea;
    private Cursor              cursor;
    private int                 key;
    private Loader              loader;
    private String              id;
    private Button              btnAdd;
    private Arrest              arrest;


    public DialogArrest( int key, Loader loader, String id, Arrest arrest) {

        this.key    = key;
        this.loader = loader;
        this.id     = id;
        this.arrest = arrest;
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
                           setRetainInstance(true);
        getDialog().setTitle("Input Name");
        View v = inflater.inflate(R.layout.dialogh_add_arests, container, false);
         spinnerOfficers = (Spinner) v.findViewById(R.id.spinnerOfficer);
         spinnerSuspects = (Spinner) v.findViewById(R.id.spinnerSuspend);
         spinnerArea =     (Spinner) v.findViewById(R.id.spinnerArea);

        daoOfficers = new DaoOfficer(getActivity());
        daoSuspects = new DaoSuspend(getActivity());
        daoArea     = new DaoArea(getActivity());
        btnAdd = (Button) v.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);

        String[] from = new String[]{DatabaseHelper.KEY_NAME, DatabaseHelper.KEY_ID};
        int[] to = new int[]{R.id.textView2, R.id.textView1};


        officersAdapter = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);
        suspectsAdapter = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);
        areaAdapter     = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);

        spinnerOfficers.setAdapter(officersAdapter);
        getLoaderManager().initLoader(0, null, cursorLoaderCallOfficer);
        spinnerSuspects.setAdapter(suspectsAdapter);
        getLoaderManager().initLoader(1, null, cursorLoaderCllSuspects);
        spinnerArea.setAdapter(areaAdapter);
        getLoaderManager().initLoader(2, null, cursorLoaderCllArea);



        spinnerOfficers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cursor = (Cursor) officersAdapter.getItem(position);
                idOfficer = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });



        spinnerSuspects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cursor = (Cursor) suspectsAdapter.getItem(position);
                idSuspend = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                cursor = (Cursor) areaAdapter.getItem(position);
                idArea = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


        return v;
    }

    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAdd :
                callCorrectChois ();
                dismiss();
                break;
            case R.id.btnCancel :
                dismiss();
                break;
            default:
                break;

        }

    }

    private  void callCorrectChois ()
    {
        Arrest arrest  = new Arrest();
        arrest.setOfficer_id(Long.parseLong(idOfficer));
        arrest.setSuspect_id(Long.parseLong(idSuspend));
        arrest.setArea_id(Long.parseLong(idArea));
        arrest.setId(Long.parseLong(id));
        DAO  daoArrest = new DaoArrest(getActivity());

        switch (key)
        {
            case 1:
                new AsyncSelect<Arrest>(daoArrest, arrest, DatabaseHelper.KEY_CREATE, null, loader)
                        .execute();
                break;
            case 2:

                new AsyncSelect<Arrest>(daoArrest, arrest, DatabaseHelper.KEY_UPDATE,null, loader)
                        .execute();
                break;
            default:
                break;
        }


    }




    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }

    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallOfficer = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            cursorLoaderOfficers = new BaseCursorLoader(getActivity(), daoOfficers);
            return cursorLoaderOfficers;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if(cursor.getCount() != 0) {
                if(arrest != null) {
                    while (cursor.moveToNext()) {
                        if (cursor.getString(1).equals(arrest.getOfficerName())) {
                            spinnerOfficers.setSelection(cursor.getPosition());
                            break;
                        }

                    }
                }
                officersAdapter.swapCursor(cursor);
            }
            else {
                Toast.makeText(getActivity(), "Table Officers is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }

        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            officersAdapter.swapCursor(null);

        }
    };


    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCllSuspects = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            cursorLoaderSuspects = new BaseCursorLoader(getActivity(), daoSuspects);
            return cursorLoaderSuspects;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

            if(cursor.getCount() != 0) {
                if(arrest != null) {
                    while (cursor.moveToNext()) {
                        if (cursor.getString(1).equals(arrest.getSuspendName())) {
                            spinnerSuspects.setSelection(cursor.getPosition());
                            break;
                        }

                    }
                }
                suspectsAdapter.swapCursor(cursor);
            }
            else {
                Toast.makeText(getActivity(), "Table Suspects is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }

        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            suspectsAdapter.swapCursor(null);

        }
    };

    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCllArea = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            cursorLoaderArea = new BaseCursorLoader(getActivity(), daoArea);
            return cursorLoaderArea;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

            if(cursor.getCount() != 0) {
                 if(arrest != null) {
                     while (cursor.moveToNext()) {
                         if (cursor.getString(1).equals(arrest.getAreaName())) {
                             spinnerArea.setSelection(cursor.getPosition());
                             break;
                         }

                     }
                 }
                areaAdapter.swapCursor(cursor);
            }
            else {
                Toast.makeText(getActivity(), "Table Areas is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }
        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            areaAdapter.swapCursor(null);

        }
    };



}