package com.example.android.day6tasksall.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class DaoArrest extends DAO<Arrest> {

    public DaoArrest(Context ctx) {
        super(ctx);
    }

    @Override
    public Cursor getAll() {
        open();
    String countQuery = "SELECT " +DatabaseHelper.TABLE_ARRESTS+"."+DatabaseHelper.KEY_ID +", "
            + DatabaseHelper.TABLE_OFFICERS + "." + DatabaseHelper.KEY_NAME + " as "
            + DatabaseHelper.KEY_OFFICER_NAME + ", " + DatabaseHelper.TABLE_SUSPECTS + "."
            + DatabaseHelper.KEY_NAME + " as " + DatabaseHelper.KEY_SUSPECTS_NAME + ", "
            + DatabaseHelper.TABLE_AREA + "." + DatabaseHelper.KEY_NAME + " as "
            + DatabaseHelper.KEY_AREA_NAME +  " FROM " + DatabaseHelper.TABLE_ARRESTS
            + " INNER JOIN " + DatabaseHelper.TABLE_OFFICERS + " ON officers._id = arrests.officer_id "
            + " INNER JOIN " + DatabaseHelper.TABLE_SUSPECTS + " ON suspects._id = arrests.suspect_id "
            + " INNER JOIN " + DatabaseHelper.TABLE_AREA     + " ON area._id = arrests.area_id";
                Cursor cursor = database.rawQuery(countQuery, null);

    return cursor;
}
    @Override
    public void create(Arrest arrest) {
        open();
        ContentValues  cv = new ContentValues();
        cv.put(DatabaseHelper.KEY_OFFICER_ID, arrest.getOfficer_id());
        cv.put(DatabaseHelper.KEY_SUSPECT_ID, arrest.getSuspect_id());
        cv.put(DatabaseHelper.KEY_AREA_ID, arrest.getArea_id());
        database.insert(DatabaseHelper.TABLE_ARRESTS, null, cv);
        close();


    }

    @Override
    public void delete(long id) {
        open();
        database.delete(DatabaseHelper.TABLE_ARRESTS, DatabaseHelper.KEY_ID + "=" + id, null);
        close();
    }



    @Override
    public void update(Arrest arrest) {
         open();
        ContentValues    cv = new ContentValues();
        cv.put(DatabaseHelper.KEY_SUSPECT_ID, arrest.getSuspect_id());
        cv.put(DatabaseHelper.KEY_OFFICER_ID, arrest.getOfficer_id());
        cv.put(DatabaseHelper.KEY_AREA_ID, arrest.getArea_id());

       boolean b = database.update(DatabaseHelper.TABLE_ARRESTS, cv, DatabaseHelper.KEY_ID + "=" + arrest.getId(), null) > 0;
        close();
    }
}
