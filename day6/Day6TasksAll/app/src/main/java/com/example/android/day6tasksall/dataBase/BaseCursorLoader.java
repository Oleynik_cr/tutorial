package com.example.android.day6tasksall.dataBase;

import android.content.Context;
import android.database.Cursor;


public class BaseCursorLoader extends android.support.v4.content.CursorLoader
{


    DAO dao;

    public BaseCursorLoader(Context context, DAO dao) {
        super(context);
        this.dao = dao;
    }

    @Override
    public Cursor loadInBackground() {
        Cursor cursor = dao.getAll();

        return cursor;
    }
}
