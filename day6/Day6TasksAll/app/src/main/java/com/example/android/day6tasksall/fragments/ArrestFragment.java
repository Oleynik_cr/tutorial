package com.example.android.day6tasksall.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.android.day6tasksall.R;
import com.example.android.day6tasksall.dataBase.Arrest;
import com.example.android.day6tasksall.dataBase.AsyncSelect;
import com.example.android.day6tasksall.dataBase.BaseCursorLoader;
import com.example.android.day6tasksall.dataBase.DAO;
import com.example.android.day6tasksall.dataBase.DaoArrest;
import com.example.android.day6tasksall.dataBase.DatabaseHelper;
import com.example.android.day6tasksall.dataBase.Person;
import com.example.android.day6tasksall.dialogs.DialogArrest;
import com.example.android.day6tasksall.dialogs.DialogOfficer;


public class ArrestFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ReloadData{

    private  SimpleCursorAdapter  scAdapter;
    private  ListView              lvData;
    private  Loader<Cursor>        cursorLoader;
    private  DAO                   dao;
    private  Button                button;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.arrest_fragment, container, false);
        setRetainInstance(true);
        button = (Button) view.findViewById(R.id.dtnAdd);
        dao = new DaoArrest(getActivity());


        String[] from = new String[] {DatabaseHelper.KEY_ID, DatabaseHelper.KEY_OFFICER_NAME, DatabaseHelper.KEY_SUSPECTS_NAME, DatabaseHelper.KEY_AREA_NAME};
        int[] to = new int[] {R.id.textView ,R.id.textView1, R.id.textView2,R.id.textView3 };



        scAdapter = new SimpleCursorAdapter(getActivity(), R.layout.arrest_item, null, from, to, 0);
        lvData = (ListView) view.findViewById(R.id.lvData);
        lvData.setAdapter(scAdapter);
        registerForContextMenu(lvData);


        getLoaderManager().initLoader(0, null, this);


        lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

            }

        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogArrest dialogArrest = new DialogArrest(DatabaseHelper.CONTEXTMENU_OPTION1, cursorLoader, "0",null);
                dialogArrest.show(getFragmentManager(), null);


            }
        });

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        cursorLoader = new BaseCursorLoader(getActivity(), dao );
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        scAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION1, 0, "Delete");
        menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION2, 0, "Update");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()) {
            AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();


            switch (item.getItemId()) {
                case DatabaseHelper.CONTEXTMENU_OPTION1:

                    deleteItem(menuInfo.position);
                    break;
                case DatabaseHelper.CONTEXTMENU_OPTION2:
                    updateItem(menuInfo.position);
                    break;
                default:
                    break;
            }

            return true;
        } else
            return false;
    }

    private void deleteItem(int position) {
      Cursor  cursor = (Cursor) scAdapter.getItem(position);
        String id = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
        new AsyncSelect<Arrest>(dao, null,DatabaseHelper.KEY_DELETE,Long.parseLong(id),cursorLoader)
                .execute();
    }

    private void updateItem(int position) {
            Arrest arrest = new Arrest();
      Cursor   cursor = (Cursor) scAdapter.getItem(position);
        arrest.setId(cursor.getLong(0));
        arrest.setOfficerName(cursor.getString(1));
        arrest.setSuspendName(cursor.getString(2));
        arrest.setAreaName(cursor.getString(3));
        String id = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ID));
        DialogArrest dialogArrest = new DialogArrest(DatabaseHelper.CONTEXTMENU_OPTION2, cursorLoader, id, arrest);
        dialogArrest.show(getFragmentManager(), null);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        dao.close();
    }


    @Override
    public void reload() {
        cursorLoader.onContentChanged();
    }
}