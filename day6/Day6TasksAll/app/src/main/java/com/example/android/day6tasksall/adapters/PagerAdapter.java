package com.example.android.day6tasksall.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.android.day6tasksall.fragments.AreaFragment;
import com.example.android.day6tasksall.fragments.ArrestFragment;
import com.example.android.day6tasksall.fragments.OfficerFragment;
import com.example.android.day6tasksall.fragments.SuspectFragment;



public class PagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
    final int PAGE_COUNT = 4;
    private  SuspectFragment suspectFragment;
    private  OfficerFragment officerFragment;
    private  ArrestFragment  arrestFragment ;
    private  AreaFragment    areaFragment   ;

    private String tabTitles[] = new String[] { "Suspends","Officers" , "Area", "Arrests" };

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        suspectFragment = new SuspectFragment();
        officerFragment = new OfficerFragment();
        areaFragment    = new AreaFragment();
        arrestFragment  = new ArrestFragment();

    }



    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {



     switch (position)
     {
         case 0:   return  suspectFragment;
         case 1:   return  officerFragment;
         case 2:   return  areaFragment;
         case 3:   return  arrestFragment;
                  default:
             return null;
     }


    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position)
        {
            case 0:     //suspectFragment;
                break;
            case 1:     officerFragment.reload();
                break;
            case 2:     //areaFragment;
                break;
            case 3:     arrestFragment.reload();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
