package com.example.android.day6tasksall.dataBase;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.content.Loader;




public class AsyncSelect<T> extends AsyncTask <Void, Void, Void> {

    private DAO dao;
    private T data;
    private String choice;
    private Long id;
    private Loader loader;


    public  AsyncSelect(DAO dao, T data, String choice, Long id,Loader loader) {

        this.dao      = dao;
        this.data     = data;
        this.choice   = choice;
        this.id       = id;
        this.loader   = loader;

    }

    @Override
    protected Void doInBackground(Void... params) {
        switch (choice) {
            case DatabaseHelper.KEY_CREATE:
                dao.create(data);

                break;
            case DatabaseHelper.KEY_UPDATE:
                dao.update(data);
                break;
            case DatabaseHelper.KEY_DELETE:
                dao.delete(id);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(loader != null)
            loader.onContentChanged();
    }
}

