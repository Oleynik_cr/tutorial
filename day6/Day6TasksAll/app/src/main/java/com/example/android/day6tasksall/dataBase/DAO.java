package com.example.android.day6tasksall.dataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public abstract class DAO<T>
{
    protected SQLiteOpenHelper databaseHelper;
    protected SQLiteDatabase database;

    public DAO(Context ctx) {
        databaseHelper = new DatabaseHelper(ctx);
    }

    public abstract Cursor getAll();

    public abstract void create(T object);

    public abstract void   delete(long id);
    public abstract void   update(T Object);

    public void open() {
        database= databaseHelper.getWritableDatabase();
    }

    public void close() {
        if (databaseHelper != null)
            databaseHelper.close();
    }





}
