package com.example.android.day63task1;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;


public class DaoGenerat {


    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(1, "green_dao");
        createDB(schema);
        new DaoGenerator().generateAll(schema, "../Day6.3Task1/app/src/main/java/com/example/android/day63task1");
    }



    private  static void createDB(Schema schema) {


        Entity suspends = schema.addEntity("Suspend");
        suspends.setTableName("suspends");
        suspends.addIdProperty();
        suspends.addStringProperty("name").notNull();
        suspends.addContentProvider();




        Entity officers = schema.addEntity("Officer");
        officers.setTableName("officers");
        officers.addIdProperty();
        officers.addStringProperty("name").notNull();
        officers.addContentProvider();


        Entity areas = schema.addEntity("Area");
        areas.setTableName("areas");
        areas.addIdProperty();
        areas.addDoubleProperty("name").notNull();
        areas.addContentProvider();


        Entity arrests = schema.addEntity("Arrest");
        arrests.setTableName("arrests");
        arrests.addIdProperty();
        Property arrestSusId  = arrests.addLongProperty("suspend_id").getProperty();
        Property arrestOffId  = arrests.addLongProperty("officer_id").getProperty();
        Property arrestAreaId = arrests.addLongProperty("area_id").getProperty();
        arrests.addToMany(suspends, arrestSusId);
        arrests.addToMany(officers, arrestOffId);
        arrests.addToMany(areas, arrestAreaId);
        arrests.addContentProvider();
    }

}