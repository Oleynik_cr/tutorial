package com.example.android.day63task1;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.example.android.day63task1.adapters.PagerAdapter;
import com.example.android.day63task1.fragments.AreaFragment;
import com.example.android.day63task1.fragments.ArrestFragment;
import com.example.android.day63task1.fragments.OfficerFragment;
import com.example.android.day63task1.fragments.SuspectFragment;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private PagerAdapter pagerAdapter;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(pagerAdapter);
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_add) {


            switch (viewPager.getCurrentItem())
            {
                case 0:
                    SuspectFragment suspectFragment = (SuspectFragment) pagerAdapter.getItem(0);
                    suspectFragment.addRecord();
                    break;
                case 1:
                    OfficerFragment officerFragment = (OfficerFragment) pagerAdapter.getItem(1);
                    officerFragment.addRecord();
                    break;
                case 2:
                    AreaFragment areaFragment = (AreaFragment) pagerAdapter.getItem(2);
                    areaFragment.addRecord();
                    break;
                case 3:
                    ArrestFragment arrestFragment = (ArrestFragment) pagerAdapter.getItem(3);
                    arrestFragment.addRecord();
                    break;
            }


        }
        return super.onOptionsItemSelected(item);
    }


}
