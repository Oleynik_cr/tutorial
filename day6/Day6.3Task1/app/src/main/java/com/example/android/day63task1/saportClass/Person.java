package com.example.android.day63task1.saportClass;

/**
 * Created by android on 18.08.15.
 */
public class Person {
   private long id;
   private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
