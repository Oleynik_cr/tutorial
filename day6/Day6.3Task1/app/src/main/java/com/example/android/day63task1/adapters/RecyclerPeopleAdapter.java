package com.example.android.day63task1.adapters;

/**
 * Created by android on 20.08.15.
 */

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.android.day63task1.R;
import com.example.android.day63task1.key.KeyWords;
import com.example.android.day63task1.saportClass.Person;

import java.util.List;

public class RecyclerPeopleAdapter extends RecyclerView.Adapter<RecyclerPeopleAdapter.ViewHolder>  {

    private List<Person> persons;

    int position;

    public RecyclerPeopleAdapter(List<Person> personList) {
        this.persons = personList;
    }


    public Person getItem(int position) {
        return  persons.get(position) ;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder contactViewHolder, int i) {
        Person person = persons.get(i);
        contactViewHolder.id.setText(""+person.getId());
        contactViewHolder.name.setText(person.getName());


        contactViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(contactViewHolder.getAdapterPosition());
                return false;
            }
        });

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.itemView.setOnClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.recycle_people_item, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return holder.itemView.callOnClick();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        protected TextView id;
        protected TextView name;


        public ViewHolder(View v) {
            super(v);
            id =  (TextView) v.findViewById(R.id.textViewId);
            name = (TextView)  v.findViewById(R.id.textViewOffName);
            v.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE, KeyWords.CONTEXTMENU_OPTION1, 0, "Delete");
            menu.add(Menu.NONE, KeyWords.CONTEXTMENU_OPTION2, 0, "Update");
        }

    }
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void clear() {
        persons.clear();
    }

    public void addAll(List<Person> collection) {
        persons.addAll(
                collection
        );
    }

}