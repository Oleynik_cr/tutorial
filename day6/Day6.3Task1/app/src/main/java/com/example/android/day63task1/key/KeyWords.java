package com.example.android.day63task1.key;

/**
 * Created by android on 25.08.15.
 */
public class KeyWords {

    public   static final String TABLE_SUSPECTS = "suspends";
    public   static final String TABLE_OFFICERS = "officers";
    public   static final String TABLE_AREA     = "areas";
    public   static final String TABLE_ARRESTS  = "arrests";

    public   static final String KEY_ID    = "_id";
    public   static final String KEY_NAME  = "NAME";

    public   static final String KEY_SUSPECT_ID = "suspend_id";
    public   static final String KEY_OFFICER_ID = "officer_id";
    public   static final String KEY_AREA_ID    = "area_id";

    public   static final String KEY_OFFICER_NAME     = "officers_name";
    public   static final String KEY_SUSPECTS_NAME    = "suspects_name";
    public   static final String KEY_AREA_NAME        = "area_name";



    public   static final String KEY_CREATE = "create";
    public   static final String KEY_UPDATE = "update";
    public   static final String KEY_DELETE = "delete";


    public   static final int CONTEXTMENU_OPTION1 = 1;
    public   static final int CONTEXTMENU_OPTION2 = 2;

    public   static final String ID = "id";
    public   static final String KEY = "key";

 public static final    String countQuery = "SELECT " +KeyWords.TABLE_ARRESTS+"."+KeyWords.KEY_ID +", "
            + KeyWords.TABLE_OFFICERS + "." + KeyWords.KEY_NAME + " as "
            + KeyWords.KEY_OFFICER_NAME + ", " + KeyWords.TABLE_SUSPECTS + "."
            + KeyWords.KEY_NAME + " as " + KeyWords.KEY_SUSPECTS_NAME + ", "
            + KeyWords.TABLE_AREA + "." + KeyWords.KEY_NAME + " as "
            + KeyWords.KEY_AREA_NAME +  " FROM " + KeyWords.TABLE_ARRESTS
            + " INNER JOIN " + KeyWords.TABLE_OFFICERS + " ON officers._id = arrests.officer_id "
            + " INNER JOIN " + KeyWords.TABLE_SUSPECTS + " ON suspends._id = arrests.suspend_id "
            + " INNER JOIN " + KeyWords.TABLE_AREA     + " ON areas._id = arrests.area_id";

}
