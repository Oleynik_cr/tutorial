package com.example.android.day63task1.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.android.day63task1.fragments.AreaFragment;
import com.example.android.day63task1.fragments.ArrestFragment;
import com.example.android.day63task1.fragments.OfficerFragment;
import com.example.android.day63task1.fragments.SuspectFragment;


/**
 * Created by android on 8/15/15.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 4;
    private SuspectFragment suspectFragment;
    public OfficerFragment officerFragment;
    private ArrestFragment arrestFragment ;
    private AreaFragment areaFragment   ;





    private String tabTitles[] = new String[] { "Suspends","Officers" , "Area", "Arrests" };

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        suspectFragment = new SuspectFragment();
        officerFragment = new OfficerFragment();
        areaFragment    = new AreaFragment();
        arrestFragment  = new ArrestFragment();
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {


     switch (position)
     {
         case 0:   return   suspectFragment;
         case 1:   return   officerFragment;
         case 2:   return   areaFragment;
         case 3:   return  arrestFragment;
                  default:
             return null;
     }


    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }



}
