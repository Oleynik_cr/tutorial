package com.example.android.day6task2.saportClass;

/**
 * Created by android on 18.08.15.
 */
public class Arrest {

    private  long id;
    private  long suspect_id;
    private  long officer_id;
    private  long area_id;
    private  String suspendName;
    private  String officerName;
    private  String areaName;

    public String getSuspendName() {
        return suspendName;
    }

    public void setSuspendName(String suspendName) {
        this.suspendName = suspendName;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSuspect_id() {
        return suspect_id;
    }

    public void setSuspect_id(long suspect_id) {
        this.suspect_id = suspect_id;
    }

    public long getOfficer_id() {
        return officer_id;
    }

    public void setOfficer_id(long officer_id) {
        this.officer_id = officer_id;
    }

    public long getArea_id() {
        return area_id;
    }

    public void setArea_id(long area_id) {
        this.area_id = area_id;
    }
}
