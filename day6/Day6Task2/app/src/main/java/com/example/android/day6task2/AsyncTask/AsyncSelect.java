package com.example.android.day6task2.AsyncTask;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.example.android.day6task2.dataBase.DatabaseHelper;


public class AsyncSelect extends AsyncTask <Void, Void, Void> {


    private  Uri uri;
    private  ContentValues cv;
    private String choice;
    private Long id;
    private Context context;

    public AsyncSelect( Context context,Uri uri,ContentValues cv, String choice, Long id) {
        this.context = context;
        this.cv      = cv;
        this.choice  = choice;
        this.id      = id;
        this.uri     = uri;
    }

    @Override
    protected Void doInBackground(Void... params) {
        switch (choice) {
            case DatabaseHelper.KEY_CREATE:
                context.getContentResolver().insert(uri, cv);
                break;
            case DatabaseHelper.KEY_UPDATE:
                context.getContentResolver().update(uri, cv, null, null);
                break;
            case DatabaseHelper.KEY_DELETE:
                context.getContentResolver().delete(uri, null, null);
              break;
            default:
                break;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

    }
}

