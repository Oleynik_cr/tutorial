package com.example.android.day6task2.dataBase;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by android on 20.08.15.
 */
public class BaseContentProvider  {

    public static final String AUTHORITY = "com.example.android.day6task2";
    public static final Uri OFFICERS_URI = Uri.parse("content://" + AUTHORITY + "/officers");
    public static final Uri SUSPECTS_URI = Uri.parse("content://" + AUTHORITY + "/suspects");
    public static final Uri AREA_URI = Uri.parse("content://" + AUTHORITY + "/area");
    public static final Uri ARRESTS_URI = Uri.parse("content://" + AUTHORITY + "/arrests");

    static  final String CONTENT_TYPE_OFFICERS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "officers";

    static final String CONTENT_TYPE_SUSPECTS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "suspects";
    static final String CONTENT_TYPE_AREA = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "area";
    static final String CONTENT_TYPE_ARRESTS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "arrests";




    public final static int KEY_ONE = 1;
    public final static int KEY_TWO = 2;
    public final static int KEY_THREE = 3;
    public final static int KEY_FOUR = 4;

}

//
//    protected SQLiteOpenHelper databaseHelper;
//
//
//    private static final UriMatcher uriMatcher;
//    static {
//        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_OFFICERS,KEY_ONE);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_OFFICERS +"/#",KEY_ONE);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_SUSPECTS,KEY_TWO);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_SUSPECTS + "/#",KEY_TWO);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_AREA, KEY_THREE);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_AREA +"/#", KEY_THREE);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_ARRESTS,KEY_FOUR);
//        uriMatcher.addURI(AUTHORITY, DatabaseHelper.TABLE_ARRESTS +"/#",KEY_FOUR);
//
//    }
//
//
//    @Override
//    public boolean onCreate() {
//        databaseHelper = new DatabaseHelper(getContext());
//        return true;
//    }
//
//    @Override
//    public Cursor query(Uri uri, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
//        SQLiteDatabase database = databaseHelper.getReadableDatabase();
//        Cursor cursor = null;
//        switch (uriMatcher.match(uri))
//        {
//            case KEY_ONE:
//
//                 cursor = database.query(DatabaseHelper.TABLE_OFFICERS, null, null, null, null, null, null);
//                cursor.setNotificationUri(getContext().getContentResolver(),
//                        OFFICERS_URI);
//
//                break;
//            case KEY_TWO:
//                 cursor = database.query(DatabaseHelper.TABLE_SUSPECTS, null, null, null, null, null, null);
//                cursor.setNotificationUri(getContext().getContentResolver(),
//                        SUSPECTS_URI);
//
//
//                break;
//            case KEY_THREE:
//                cursor = database.query(DatabaseHelper.TABLE_AREA, null, null, null, null, null, null);
//                cursor.setNotificationUri(getContext().getContentResolver(),
//                        AREA_URI);
//
//
//                break;
//            case KEY_FOUR:
//
//                String countQuery = "SELECT " +DatabaseHelper.TABLE_ARRESTS+"."+DatabaseHelper.KEY_ID +", "
//                        + DatabaseHelper.TABLE_OFFICERS + "." + DatabaseHelper.KEY_NAME + " as "
//                        + DatabaseHelper.KEY_OFFICER_NAME + ", " + DatabaseHelper.TABLE_SUSPECTS + "."
//                        + DatabaseHelper.KEY_NAME + " as " + DatabaseHelper.KEY_SUSPECTS_NAME + ", "
//                        + DatabaseHelper.TABLE_AREA + "." + DatabaseHelper.KEY_NAME + " as "
//                        + DatabaseHelper.KEY_AREA_NAME +  " FROM " + DatabaseHelper.TABLE_ARRESTS
//                        + " INNER JOIN " + DatabaseHelper.TABLE_OFFICERS + " ON officers._id = arrests.officer_id "
//                        + " INNER JOIN " + DatabaseHelper.TABLE_SUSPECTS + " ON suspects._id = arrests.suspect_id "
//                        + " INNER JOIN " + DatabaseHelper.TABLE_AREA     + " ON area._id = arrests.area_id";
//                 cursor = database.rawQuery(countQuery, null);
//                cursor.setNotificationUri(getContext().getContentResolver(),
//                        ARRESTS_URI);
//
//
//                break;
//            default:
//                break;
//
//        }
//
//
//        return cursor;
//    }
//
//    @Override
//    public String getType(Uri uri) {
//        return null;
//    }
//
//    @Override
//    public Uri insert(Uri uri, ContentValues values) {
//        long rowID;
//        Uri resultUri;
//        SQLiteDatabase database = databaseHelper.getReadableDatabase();
//
//        switch (uriMatcher.match(uri)) {
//            case KEY_ONE:
//               rowID = database.insert(DatabaseHelper.TABLE_OFFICERS, null, values);
//                  resultUri = ContentUris.withAppendedId(OFFICERS_URI, rowID);
//
//                getContext().getContentResolver().notifyChange(resultUri, null);
//
//                break;
//            case KEY_TWO:
//                rowID =  database.insert(DatabaseHelper.TABLE_SUSPECTS, null, values);
//                 resultUri = ContentUris.withAppendedId(SUSPECTS_URI, rowID);
//
//                getContext().getContentResolver().notifyChange(resultUri, null);
//
//                break;
//            case KEY_THREE:
//                rowID =  database.insert(DatabaseHelper.TABLE_AREA, null, values);
//                resultUri = ContentUris.withAppendedId(AREA_URI, rowID);
//
//                getContext().getContentResolver().notifyChange(resultUri, null);
//
//                break;
//            case KEY_FOUR:
//                rowID =   database.insert(DatabaseHelper.TABLE_ARRESTS, null, values);
//                resultUri = ContentUris.withAppendedId(ARRESTS_URI, rowID);
//
//                getContext().getContentResolver().notifyChange(resultUri, null);
//
//
//                break;
//            default:
//                break;
//
//        }
//
//        return uri;
//    }
//
//    @Override
//    public int delete(Uri uri, String selection, String[] selectionArgs){
//    SQLiteDatabase database = databaseHelper.getReadableDatabase();
//    String id;
//    switch (uriMatcher.match(uri))
//    {
//        case KEY_ONE:
//            id = uri.getLastPathSegment();
//            selection = "_id" + " = " + id;
//            database.delete(DatabaseHelper.TABLE_OFFICERS,selection, null);
//            getContext().getContentResolver().notifyChange(OFFICERS_URI, null);
//            break;
//        case KEY_TWO:
//            id = uri.getLastPathSegment();
//            selection = "_id" + " = " + id;
//            database.delete(DatabaseHelper.TABLE_SUSPECTS, selection, null);
//            getContext().getContentResolver().notifyChange(SUSPECTS_URI, null);
//
//            break;
//        case KEY_THREE:
//            id = uri.getLastPathSegment();
//            selection = "_id" + " = " + id;
//            database.delete(DatabaseHelper.TABLE_AREA, selection, null);
//            getContext().getContentResolver().notifyChange(AREA_URI, null);
//
//            break;
//        case KEY_FOUR:
//            id = uri.getLastPathSegment();
//            selection = "_id" + " = " + id;
//            database.delete(DatabaseHelper.TABLE_ARRESTS, selection, null);
//            getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
//            break;
//        default:
//            break;
//
//    }
//
//       return 0;
//    }
//
//    @Override
//    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
//        SQLiteDatabase database = databaseHelper.getReadableDatabase();
//        String id;
//        switch (uriMatcher.match(uri))
//        {
//            case KEY_ONE:
//                 id = uri.getLastPathSegment();
//                    selection = "_id" + " = " + id;
//                database.update(DatabaseHelper.TABLE_OFFICERS, values,  selection, null);
//                getContext().getContentResolver().notifyChange(OFFICERS_URI, null);
//                break;
//
//            case KEY_TWO:
//                 id = uri.getLastPathSegment();
//                selection = "_id" + " = " + id;
//                database.update(DatabaseHelper.TABLE_SUSPECTS, values, selection, null);
//                getContext().getContentResolver().notifyChange(SUSPECTS_URI, null);
//                break;
//
//            case KEY_THREE:
//                id = uri.getLastPathSegment();
//                selection = "_id" + " = " + id;
//                database.update(DatabaseHelper.TABLE_AREA, values, selection, null);
//                getContext().getContentResolver().notifyChange(AREA_URI, null);
//                break;
//
//            case KEY_FOUR:
//                id = uri.getLastPathSegment();
//                selection = "_id" + " = " + id;
//                Log.i("EEE",selection);
//                database.update(DatabaseHelper.TABLE_ARRESTS, values, selection, null);
//                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
//                break;
//            default:
//                break;
//        }
//              return 0;
//    }
//}
