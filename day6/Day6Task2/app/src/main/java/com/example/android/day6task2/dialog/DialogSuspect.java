package com.example.android.day6task2.dialog;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.android.day6task2.AsyncTask.AsyncSelect;
import com.example.android.day6task2.R;
import com.example.android.day6task2.dataBase.BaseContentProvider;
import com.example.android.day6task2.dataBase.DatabaseHelper;


public class DialogSuspect extends DialogFragment implements View.OnClickListener {
    private Activity activity;
    private EditText editText;
    private   int position;
    private   long id;
    String    name;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activity = getActivity();

        position = getArguments().getInt(DatabaseHelper.KEY);
        id = getArguments().getLong(DatabaseHelper.ID);
        name =  getArguments().getString(DatabaseHelper.KEY_NAME);


    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        getDialog().setTitle("Input Name");
        View v = inflater.inflate(R.layout.dialog_people_name,container, false);

        editText = (EditText) v.findViewById(R.id.editText);
        v.findViewById(R.id.btnAdd).setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);
        if(position == 2)
            editText.setText(name);
        return v;
    }

    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAdd :
                correctChis();
                dismiss();
                break;
            case R.id.btnCancel :
                dismiss();
                break;
            default:
                break;

        }

    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }
    public static DialogSuspect newInstance(int  key, long id, String name) {
        DialogSuspect fragment = new DialogSuspect();
        Bundle args = new Bundle();
        args.putLong(DatabaseHelper.ID, id);
        args.putInt(DatabaseHelper.KEY, key);
        args.putString(DatabaseHelper.KEY_NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    private void correctChis()
    {
        ContentValues cv = new ContentValues();
        switch (position)
        {
            case 1 :

                cv.put(DatabaseHelper.KEY_NAME, editText.getText().toString());
                new AsyncSelect(getActivity(), BaseContentProvider.SUSPECTS_URI, cv, DatabaseHelper.KEY_CREATE,null)
                        .execute();
                break;
            case 2 :
                cv.put(DatabaseHelper.KEY_NAME, editText.getText().toString());
                Uri uri = ContentUris.withAppendedId(BaseContentProvider.SUSPECTS_URI, id);
                new AsyncSelect(getActivity(),uri, cv, DatabaseHelper.KEY_UPDATE, null)
                        .execute();
                break;
            default:
                break;

        }
    }
}