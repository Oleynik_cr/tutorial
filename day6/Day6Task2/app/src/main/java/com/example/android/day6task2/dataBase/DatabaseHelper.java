package com.example.android.day6task2.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

/**
 * Created by android on 8/14/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

     static final int DATABASE_VERSION = 2;

    static final String DATABASE_NAME = "Police_Arrests_Information";


   public   static final String TABLE_SUSPECTS = "suspects";
   public   static final String TABLE_OFFICERS = "officers";
   public   static final String TABLE_AREA     = "area";
   public   static final String TABLE_ARRESTS  = "arrests";

   public   static final String KEY_ID    = "_id";
   public   static final String KEY_NAME  = "name";

   public   static final String KEY_SUSPECT_ID = "suspect_id";
   public   static final String KEY_OFFICER_ID = "officer_id";
   public   static final String KEY_AREA_ID    = "area_id";

   public   static final String KEY_OFFICER_NAME     = "officers_name";
   public   static final String KEY_SUSPECTS_NAME    = "suspects_name";
   public   static final String KEY_AREA_NAME        = "area_name";



   public   static final String KEY_CREATE = "create";
   public   static final String KEY_UPDATE = "update";
   public   static final String KEY_DELETE = "delete";


   public   static final int CONTEXTMENU_OPTION1 = 1;
   public   static final int CONTEXTMENU_OPTION2 = 2;

   public   static final String ID = "id";
   public   static final String KEY = "key";





    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        String CREATE_SUSPECTS_TABLE = "CREATE TABLE " + TABLE_SUSPECTS + "("
//                + KEY_ID + " INTEGER PRIMARY KEY" + " AUTOINCREMENT, "
//                + KEY_NAME + " VARCHAR(50) NOT NULL)";
//        db.execSQL(CREATE_SUSPECTS_TABLE);
//        ContentValues cv = new ContentValues();
//
//        for (int i = 1; i < 5; i++) {
//
//            cv.put(KEY_NAME, "mame" + i);
//            db.insert(TABLE_SUSPECTS, null, cv);
//
//        }
//
//
//        String CREATE_OFFICERS_TABLE = "CREATE TABLE " + TABLE_OFFICERS + "("
//                + KEY_ID + " INTEGER PRIMARY KEY" + " AUTOINCREMENT, "
//                + KEY_NAME + " VARCHAR(50) NOT NULL)";
//        db.execSQL(CREATE_OFFICERS_TABLE);
//        ContentValues cv1 = new ContentValues();
//
//        for (int i = 1; i < 5; i++) {
//
//            cv1.put(KEY_NAME, "officer" + i);
//            db.insert(TABLE_OFFICERS, null, cv1);
//
//        }
//
//
//        String CREATE_AREA_TABLE = "CREATE TABLE " + TABLE_AREA + "("
//                + KEY_ID + " INTEGER PRIMARY KEY" + " AUTOINCREMENT, "
//                + KEY_NAME + " VARCHAR(50) NOT NULL)";
//        db.execSQL(CREATE_AREA_TABLE);
//        ContentValues cv2 = new ContentValues();
//        for (int i = 1; i < 5; i++) {
//
//            cv2.put(KEY_NAME, "Area"+i);
//            db.insert(TABLE_AREA, null, cv2);
//        }
//
//
//        String CREATE_ARRESTS_TABLE = "CREATE TABLE " + TABLE_ARRESTS + "("
//                + KEY_ID         + " INTEGER PRIMARY KEY" + " AUTOINCREMENT, "
//                + KEY_SUSPECT_ID + " INTEGER REFERENCES " + TABLE_SUSPECTS + "(" +KEY_ID+ "), "
//                + KEY_OFFICER_ID + " INTEGER REFERENCES " + TABLE_OFFICERS + "(" +KEY_ID+ "), "
//                + KEY_AREA_ID    + " INTEGER REFERENCES " + TABLE_AREA     + "(" +KEY_ID+ "))";
//
//        db.execSQL(CREATE_ARRESTS_TABLE);
//        ContentValues cv3 = new ContentValues();
//
//
//            cv3.put(KEY_SUSPECT_ID, 2);
//            cv3.put(KEY_OFFICER_ID, 1);
//            cv3.put(KEY_AREA_ID, 1);
//            db.insert(TABLE_ARRESTS, null, cv3);
//


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUSPECTS);



        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFICERS);


        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AREA);


        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARRESTS);
        onCreate(db);
    }





}
