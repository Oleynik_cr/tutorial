package com.example.android.day6task2.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.android.day6task2.saportClass.Arrest;
import com.example.android.day6task2.R;
import com.example.android.day6task2.dataBase.DatabaseHelper;

import java.util.List;

public class RecyclerArrestAdapter extends RecyclerView.Adapter<RecyclerArrestAdapter.ViewHolder> {

    private List<Arrest> arrests;
    private int position;

    public RecyclerArrestAdapter(List<Arrest> arrests) {
        this.arrests = arrests;
    }


    @Override
    public int getItemCount() {
        return arrests.size();
    }

    public Arrest getItem(int position) {
        return  arrests.get(position) ;
    }

    @Override
    public void onBindViewHolder( final ViewHolder contactViewHolder, int i) {
        Arrest arrest = arrests.get(i);
        contactViewHolder.id.setText(""+arrest.getId());
        contactViewHolder.officerName.setText(arrest.getOfficerName());
        contactViewHolder.suspendName.setText(arrest.getSuspendName());
        contactViewHolder.areaName.setText(arrest.getAreaName());

        contactViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(contactViewHolder.getAdapterPosition());
                return false;
            }
        });


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.recycle_arrest_item, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        protected TextView id;
        protected TextView officerName;
        protected TextView suspendName;
        protected TextView areaName;


        public ViewHolder(View v) {
            super(v);
            id = (TextView) v.findViewById(R.id.textViewId);
            officerName = (TextView) v.findViewById(R.id.textViewOffName);
            suspendName = (TextView) v.findViewById(R.id.textNameSuspend);
            areaName = (TextView) v.findViewById(R.id.textAreaName);
            v.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION1, 0, "Delete");
            menu.add(Menu.NONE, DatabaseHelper.CONTEXTMENU_OPTION2, 0, "Update");
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
