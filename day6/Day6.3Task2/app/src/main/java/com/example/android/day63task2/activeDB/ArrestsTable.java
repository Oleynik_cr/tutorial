package com.example.android.day63task2.activeDB;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by android on 26.08.15.
 */
@Table(name = "arrests", id = "_id")
public class ArrestsTable extends Model {

    @Column(name = "officer_id",  onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public OfficersTable officersTable;

    @Column(name = "suspend_id",  onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public SuspendsTable suspendsTable;


    @Column(name = "area_id", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    public AreasTable areasTable;



    public ArrestsTable() {
        super();
    }


    public ArrestsTable(OfficersTable officersTable, SuspendsTable suspendsTable, AreasTable areasTable) {
        this.officersTable = officersTable;
        this.suspendsTable = suspendsTable;
        this.areasTable = areasTable;
    }
}
