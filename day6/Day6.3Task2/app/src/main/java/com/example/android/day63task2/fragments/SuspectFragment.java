package com.example.android.day63task2.fragments;


import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.example.android.day63task2.async_task.AsyncSelect;
import com.example.android.day63task2.R;
import com.example.android.day63task2.adapters.RecyclerPeopleAdapter;
import com.example.android.day63task2.contentProvider.BaseContentProvider;
import com.example.android.day63task2.dialog.DialogSuspect;
import com.example.android.day63task2.key.KeyWords;
import com.example.android.day63task2.saportClass.Person;

import java.util.ArrayList;
import java.util.List;

public class SuspectFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private RecyclerView recList;
    private Button button;
    private RecyclerPeopleAdapter recyclerPeopleAdapter;
    private Loader<Cursor> cursorLoader;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_people, container, false);
        setRetainInstance(true);
        button = (Button) view.findViewById(R.id.btnAdd);

        recList = (RecyclerView) view.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);


        getLoaderManager().initLoader(7, null, this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRecord();

            }
        });


        return view;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        if (getUserVisibleHint()) {
            switch (item.getItemId()) {
                case KeyWords.CONTEXTMENU_OPTION1:
                    deleteItem(recyclerPeopleAdapter.getItem(recyclerPeopleAdapter.getPosition()).getId());
                    break;
                case KeyWords.CONTEXTMENU_OPTION2:
                    updateItem(recyclerPeopleAdapter.getItem(recyclerPeopleAdapter.getPosition()).getId());
                    break;
                default:
                    break;
            }

            return true;
        } else
            return false;
    }

    private void deleteItem(long id) {


        Uri uri = ContentUris.withAppendedId(BaseContentProvider.SUSPECTS_URI, id);

        new AsyncSelect(getActivity(),uri, null, KeyWords.KEY_DELETE,null)
                .execute();

    }

    private void updateItem(long id) {
        String name = recyclerPeopleAdapter.getItem(recyclerPeopleAdapter.getPosition()).getName();
        DialogSuspect dialogSuspect = DialogSuspect.newInstance(KeyWords.CONTEXTMENU_OPTION2, id, name);
        dialogSuspect.show(getFragmentManager(), "0");


    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);





    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new android.support.v4.content.CursorLoader(getActivity(), BaseContentProvider.SUSPECTS_URI, null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        showList(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void showList(Cursor cursor)
    {
        List<Person> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Person person = new Person();
                person.setId(cursor.getLong(0));
                person.setName(cursor.getString(1));

                result.add(person);
            } while (cursor.moveToNext());
        }
            recyclerPeopleAdapter = new RecyclerPeopleAdapter(result);
            recList.setAdapter(recyclerPeopleAdapter);
        }

    public void addRecord()
    {
         DialogSuspect dialogSuspect = DialogSuspect.newInstance(KeyWords.CONTEXTMENU_OPTION1, 0, null);
        dialogSuspect.show(getFragmentManager(), "0");
    }


}
