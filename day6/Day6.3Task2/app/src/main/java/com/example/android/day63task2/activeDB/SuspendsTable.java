package com.example.android.day63task2.activeDB;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by android on 26.08.15.
 */
@Table(name = "suspends" , id = "_id")
public class SuspendsTable extends Model{



    @Column(name = "name")
    public String name;

    public SuspendsTable() {
        super();
    }

    public SuspendsTable(String name) {
        this.name = name;
    }
}
