package com.example.android.day63task2.contentProvider;


import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.activeandroid.Cache;
import com.activeandroid.content.ContentProvider;
import com.example.android.day63task2.key.KeyWords;


public class BaseContentProvider extends ContentProvider {

    public static final String AUTHORITY = "com.example.android.day63task2";
    public static final Uri OFFICERS_URI = Uri.parse("content://" + AUTHORITY + "/officers");
    public static final Uri SUSPECTS_URI = Uri.parse("content://" + AUTHORITY + "/suspends");
    public static final Uri AREA_URI = Uri.parse("content://" + AUTHORITY + "/areas");
    public static final Uri ARRESTS_URI = Uri.parse("content://" + AUTHORITY + "/arrests");

    static final String CONTENT_TYPE_OFFICERS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "officers";

    static final String CONTENT_TYPE_SUSPECTS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "suspects";
    static final String CONTENT_TYPE_AREA = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "area";
    static final String CONTENT_TYPE_ARRESTS = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + "arrests";

    public final static int KEY_ONE = 1;
    public final static int KEY_TWO = 2;
    public final static int KEY_THREE = 3;
    public final static int KEY_FOUR = 4;







    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_OFFICERS,KEY_ONE);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_OFFICERS +"/#",KEY_ONE);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_SUSPECTS,KEY_TWO);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_SUSPECTS + "/#",KEY_TWO);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_AREA, KEY_THREE);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_AREA +"/#", KEY_THREE);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_ARRESTS,KEY_FOUR);
        uriMatcher.addURI(AUTHORITY, KeyWords.TABLE_ARRESTS +"/#",KEY_FOUR);

    }

    @Override
    public boolean onCreate() {

        return true;
    }


    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri))
        {
            case KEY_ONE:
                return   CONTENT_TYPE_OFFICERS;

            case KEY_TWO:
                return   CONTENT_TYPE_SUSPECTS;

            case KEY_THREE:
                return   CONTENT_TYPE_AREA;

            case KEY_FOUR:
                return   CONTENT_TYPE_ARRESTS;

            default:
                break;

        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = Cache.openDatabase();
        long rowID;
        Uri resultUri;


        switch (uriMatcher.match(uri)) {
            case KEY_ONE:

                rowID = db.insert(KeyWords.TABLE_OFFICERS, null, values);
                resultUri = ContentUris.withAppendedId(OFFICERS_URI, rowID);

                getContext().getContentResolver().notifyChange(resultUri, null);

                break;
            case KEY_TWO:
                rowID =  db.insert(KeyWords.TABLE_SUSPECTS, null, values);
                resultUri = ContentUris.withAppendedId(SUSPECTS_URI, rowID);

                getContext().getContentResolver().notifyChange(resultUri, null);

                break;
            case KEY_THREE:
                rowID =  db.insert(KeyWords.TABLE_AREA, null, values);
                resultUri = ContentUris.withAppendedId(AREA_URI, rowID);

                getContext().getContentResolver().notifyChange(resultUri, null);

                break;
            case KEY_FOUR:
                rowID =   db.insert(KeyWords.TABLE_ARRESTS, null, values);
                resultUri = ContentUris.withAppendedId(ARRESTS_URI, rowID);

                getContext().getContentResolver().notifyChange(resultUri, null);


                break;
            default:
                break;

        }

        return uri;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = Cache.openDatabase();
        String id;
        switch (uriMatcher.match(uri))
        {
            case KEY_ONE:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.update(KeyWords.TABLE_OFFICERS, values,  selection, null);
                getContext().getContentResolver().notifyChange(OFFICERS_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;

            case KEY_TWO:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.update(KeyWords.TABLE_SUSPECTS, values, selection, null);
                getContext().getContentResolver().notifyChange(SUSPECTS_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;

            case KEY_THREE:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.update(KeyWords.TABLE_AREA, values, selection, null);
                getContext().getContentResolver().notifyChange(AREA_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;

            case KEY_FOUR:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;

                db.update(KeyWords.TABLE_ARRESTS, values, selection, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;
            default:
                break;
        }
        return 0;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = Cache.openDatabase();
        String id;
        switch (uriMatcher.match(uri))
        {
            case KEY_ONE:

                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;

                db.delete(KeyWords.TABLE_OFFICERS,selection, null);
                getContext().getContentResolver().notifyChange(OFFICERS_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;
            case KEY_TWO:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.delete(KeyWords.TABLE_SUSPECTS, selection, null);
                getContext().getContentResolver().notifyChange(SUSPECTS_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);

                break;
            case KEY_THREE:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.delete(KeyWords.TABLE_AREA, selection, null);
                getContext().getContentResolver().notifyChange(AREA_URI, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);

                break;
            case KEY_FOUR:
                id = uri.getLastPathSegment();
                selection = "_id" + " = " + id;
                db.delete(KeyWords.TABLE_ARRESTS, selection, null);
                getContext().getContentResolver().notifyChange(ARRESTS_URI, null);
                break;
            default:
                break;

        }

        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = Cache.openDatabase();
        Cursor cursor = null;
        switch (uriMatcher.match(uri))
        {
            case KEY_ONE:
                cursor = db.query(KeyWords.TABLE_OFFICERS, null, null, null, null, null, null);
                cursor.setNotificationUri(getContext().getContentResolver(),
                        OFFICERS_URI);

                break;
            case KEY_TWO:
                cursor = db.query(KeyWords.TABLE_SUSPECTS, null, null, null, null, null, null);
                cursor.setNotificationUri(getContext().getContentResolver(),
                        SUSPECTS_URI);


                break;
            case KEY_THREE:
                cursor = db.query(KeyWords.TABLE_AREA, null, null, null, null, null, null);
                cursor.setNotificationUri(getContext().getContentResolver(),
                        AREA_URI);


                break;
            case KEY_FOUR:
                cursor = db.rawQuery(KeyWords.countQuery, null);
                cursor.setNotificationUri(getContext().getContentResolver(), ARRESTS_URI);
                break;
            default:
                break;

        }


        return cursor;
    }


}
