package com.example.android.day63task2.dialog;



import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.day63task2.async_task.AsyncSelect;
import com.example.android.day63task2.R;
import com.example.android.day63task2.contentProvider.BaseContentProvider;
import com.example.android.day63task2.key.KeyWords;
import com.example.android.day63task2.saportClass.Arrest;


public class DialogArrest extends DialogFragment implements OnClickListener {

    private   Activity     activity;
    private Spinner        spinnerOfficers;
    private Spinner        spinnerSuspects;
    private Spinner        spinnerArea;
    private SimpleCursorAdapter officersAdapter;
    private SimpleCursorAdapter suspectsAdapter;
    private SimpleCursorAdapter areaAdapter;
    private String              idOfficer;
    private String              idSuspend;
    private String              idArea;
    private Cursor              cursor;
    private int                 key;
    private Long                id;
    private Button              btnAdd;
    private Arrest              arrest;



    public DialogArrest( int key,  Long id, Arrest arrest) {

        this.key    = key;
        this.id     = id;
        this.arrest = arrest;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activity = getActivity();

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
                           setRetainInstance(true);
        getDialog().setTitle("Input Name");
        View v = inflater.inflate(R.layout.dialogh_add_arests, container, false);
         spinnerOfficers = (Spinner) v.findViewById(R.id.spinnerOfficer);
         spinnerSuspects = (Spinner) v.findViewById(R.id.spinnerSuspend);
         spinnerArea =     (Spinner) v.findViewById(R.id.spinnerArea);




        btnAdd = (Button) v.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(this);
        v.findViewById(R.id.btnCancel).setOnClickListener(this);

        String[] from = new String[]{KeyWords.KEY_NAME, KeyWords.KEY_ID};
        int[] to = new int[]{R.id.textView2, R.id.textView1};


        officersAdapter = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);
        suspectsAdapter = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);
        areaAdapter     = new SimpleCursorAdapter(getActivity(), R.layout.people_item, null, from, to, 0);

        spinnerOfficers.setAdapter(officersAdapter);
        getLoaderManager().initLoader(0, null, cursorLoaderCallOfficer);
        spinnerSuspects.setAdapter(suspectsAdapter);
        getLoaderManager().initLoader(1, null, cursorLoaderCllSuspects);
        spinnerArea.setAdapter(areaAdapter);
        getLoaderManager().initLoader(2, null, cursorLoaderCllArea);



        spinnerOfficers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cursor = (Cursor) officersAdapter.getItem(position);
                idOfficer = cursor.getString(cursor.getColumnIndex(KeyWords.KEY_ID));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });



        spinnerSuspects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cursor = (Cursor) suspectsAdapter.getItem(position);
                idSuspend = cursor.getString(cursor.getColumnIndex(KeyWords.KEY_ID));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });

        spinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                cursor = (Cursor) areaAdapter.getItem(position);
                idArea = cursor.getString(cursor.getColumnIndex(KeyWords.KEY_ID));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        return v;
    }

    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnAdd :
                callCorrectChois ();
                dismiss();
                break;
            case R.id.btnCancel :
                dismiss();
                break;
            default:
                break;

        }

    }

    private  void callCorrectChois (){


        ContentValues cv = new ContentValues();

        switch (key)
        {
            case 1:

                cv.put(KeyWords.KEY_OFFICER_ID, Long.parseLong(idOfficer));
                cv.put(KeyWords.KEY_SUSPECT_ID, Long.parseLong(idSuspend));
                cv.put(KeyWords.KEY_AREA_ID,    Long.parseLong(idArea));
                new AsyncSelect(getActivity(), BaseContentProvider.ARRESTS_URI, cv, KeyWords.KEY_CREATE,null)
                        .execute();
                  break;
            case 2:

                cv.put(KeyWords.KEY_OFFICER_ID, Long.parseLong(idOfficer));
                cv.put(KeyWords.KEY_SUSPECT_ID, Long.parseLong(idSuspend));
                cv.put(KeyWords.KEY_AREA_ID,    Long.parseLong(idArea));
                Uri uri = ContentUris.withAppendedId(BaseContentProvider.ARRESTS_URI, id);

                new AsyncSelect(getActivity(),uri, cv, KeyWords.KEY_UPDATE,null)
                        .execute();
                break;
            default:
                break;
        }


    }




    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

    }

    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallOfficer = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new android.support.v4.content.CursorLoader(getActivity(), BaseContentProvider.OFFICERS_URI, null,null,null,null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if (cursor.getCount() != 0) {
                if (arrest != null) {
                    while (cursor.moveToNext()) {
                        if (cursor.getString(1).equals(arrest.getOfficerName())) {
                            spinnerOfficers.setSelection(cursor.getPosition());
                            break;
                        }

                    }
                }
                officersAdapter.swapCursor(cursor);
            } else {
                Toast.makeText(getActivity(), "Table Officers is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }
        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            officersAdapter.swapCursor(null);

        }
    };


    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCllSuspects = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new android.support.v4.content.CursorLoader(getActivity(), BaseContentProvider.SUSPECTS_URI, null,null,null,null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if(cursor.getCount() != 0) {
                if(arrest != null) {
                    while (cursor.moveToNext()) {
                        if (cursor.getString(1).equals(arrest.getSuspendName())) {
                            spinnerSuspects.setSelection(cursor.getPosition());
                            break;
                        }

                    }
                }
                suspectsAdapter.swapCursor(cursor);
            }
            else {
                Toast.makeText(getActivity(), "Table Suspects is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }

        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            suspectsAdapter.swapCursor(null);

        }
    };

    LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCllArea = new   LoaderManager.LoaderCallbacks<Cursor>()

    {


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new android.support.v4.content.CursorLoader(getActivity(), BaseContentProvider.AREA_URI, null,null,null,null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if(cursor.getCount() != 0) {
                if(arrest != null) {
                    while (cursor.moveToNext()) {
                        if (cursor.getString(1).equals(arrest.getAreaName())) {
                            spinnerArea.setSelection(cursor.getPosition());
                            break;
                        }

                    }
                }
                areaAdapter.swapCursor(cursor);
            }
            else {
                Toast.makeText(getActivity(), "Table Areas is empty", Toast.LENGTH_LONG).show();
                btnAdd.setClickable(false);
            }
        }



        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            areaAdapter.swapCursor(null);

        }
    };



}