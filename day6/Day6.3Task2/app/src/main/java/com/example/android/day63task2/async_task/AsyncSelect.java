package com.example.android.day63task2.async_task;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.example.android.day63task2.key.KeyWords;

public class AsyncSelect extends AsyncTask <Void, Void, Void> {

    private  Uri uri;
    private  ContentValues cv;
    private String choice;
    private Long id;
    private Context context;

    public AsyncSelect(Context context, Uri uri, ContentValues cv, String choice, Long id) {
        this.context = context;
        this.cv      = cv;
        this.choice  = choice;
        this.id      = id;
        this.uri     = uri;
    }

    @Override
    protected Void doInBackground(Void... params) {
        switch (choice) {
            case KeyWords.KEY_CREATE:
                context.getContentResolver().insert(uri, cv);
                break;
            case KeyWords.KEY_UPDATE:
                context.getContentResolver().update(uri, cv, null, null);
                break;
            case KeyWords.KEY_DELETE:
                context.getContentResolver().delete(uri, null, null);
              break;
            default:
                break;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

    }
}

