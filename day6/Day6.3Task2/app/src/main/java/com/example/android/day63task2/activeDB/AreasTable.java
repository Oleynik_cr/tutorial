package com.example.android.day63task2.activeDB;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by android on 26.08.15.
 */
@Table(name = "areas", id = "_id")
public class AreasTable extends Model {


    @Column(name = "name")
    public String name;

    public AreasTable() {
        super();
    }

    public AreasTable(String name) {
        this.name = name;
    }
}
