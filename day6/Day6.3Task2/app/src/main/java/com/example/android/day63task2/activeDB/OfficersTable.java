package com.example.android.day63task2.activeDB;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;


@Table(name = "officers" , id = "_id")
public class OfficersTable extends Model  {


    @Column(name = "name")
    public String name;

    public OfficersTable() {
        super();
    }

    public OfficersTable( String name) {
        super();
        this.name = name;
    }
}

