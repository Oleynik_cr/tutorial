package com.example.android.day2task2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

public class ImageActivity extends AppCompatActivity {
    private static int RESULT_LOAD_IMG = 1;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        dispatchTakeImageIntent();
    }

       private  void dispatchTakeImageIntent() {
           intent = new Intent();
           intent.setType("image/*");
           intent.setAction(Intent.ACTION_GET_CONTENT);
           startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
       }


@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data)
{

    if(resultCode==RESULT_OK)
    {
        ImageView imgView = (ImageView) findViewById(R.id.imgView);
        Uri selectedimg = data.getData();
        try {
            imgView.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedimg));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

}