package com.example.android.day2task2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnGetContact:
                intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);

                break;
            case R.id.btnGetImage:
                intent = new Intent(MainActivity.this, ImageActivity.class);
                startActivity(intent);
                break;
            case R.id.btnGetVideo:
                intent = new Intent(MainActivity.this, VideoActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }
}
