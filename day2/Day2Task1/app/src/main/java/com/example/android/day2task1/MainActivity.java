package com.example.android.day2task1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editNumberPhoneFromCall;
    private EditText editNumberPhoneFromSMS;
    private EditText editTextSMS;
    private EditText editLink;
    private EditText editAddressEmail;

    private Intent   intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNumberPhoneFromCall = (EditText) findViewById(R.id.editNumberPhoneFromCall);
        editNumberPhoneFromSMS = (EditText) findViewById(R.id.editNumberPhoneFromSMS);
        editTextSMS = (EditText) findViewById(R.id.editTextSMS);
        editLink = (EditText) findViewById(R.id.editLink);
        editAddressEmail = (EditText) findViewById(R.id.editAddressEmail);


    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnCall:

                intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +
                        editNumberPhoneFromCall.getText().toString()));
                startActivity(intent);
                break;
            case R.id.btnSendSMS:

                Uri smsUri = Uri.parse("tel:" + editNumberPhoneFromSMS.getText().toString());
                intent = new Intent(Intent.ACTION_VIEW, smsUri);
                intent.putExtra("address", editNumberPhoneFromSMS.getText().toString());
                intent.putExtra("sms_body", editTextSMS.getText().toString());
                        intent.setType("vnd.android-dir/mms-sms");
                startActivity(intent);

                break;
            case R.id.btnOpenLink:
                intent  = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www."+editLink.getText().toString()));
                startActivity(intent);
                break;
            case R.id.btnSendEmail:
                 intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{editAddressEmail.getText().toString()});

                intent.putExtra(Intent.EXTRA_SUBJECT, "subject");

                intent.putExtra(Intent.EXTRA_TEXT, "message");

                intent.setType("message/rfc822");

                startActivity(Intent.createChooser(intent, "Выберите email клиент :"));
                break;
            default:
                break;
        }
    }
}