package com.example.android.day2task3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by android on 8/12/15.
 */
public class NotRegistrationBR extends BroadcastReceiver {
    public NotRegistrationBR() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, R.string.message_fron_not_reg_br, Toast.LENGTH_SHORT).show();
    }
}
