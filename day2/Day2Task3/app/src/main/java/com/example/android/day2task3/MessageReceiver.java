package com.example.android.day2task3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MessageReceiver extends BroadcastReceiver {
    public static final String NAME_MASSAGE= "broadcast.Message";
    public MessageReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,
                        intent.getStringExtra(NAME_MASSAGE),
                Toast.LENGTH_LONG).show();
    }
}
