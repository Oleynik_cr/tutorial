package com.example.android.day2task3;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String ACTION_NAME = "com.example.android.day2task3";
    public static final String MESSAGE = "Massage";
    public static final String NAME_MASSAGE= "broadcast.Message";
    public static final String INTENT_ACTION_TIME_TICK = "android.intent.action.TIME_TICK";

    private Intent intent;
    private NotRegistrationBR notRegistrationBR = new NotRegistrationBR();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void sendMessage(View view) {
        intent = new Intent();
        intent.setAction(ACTION_NAME);
        intent.putExtra( NAME_MASSAGE, MESSAGE);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }


    public void registerBroadcastReceiver(View view) {
        this.registerReceiver(notRegistrationBR, new IntentFilter(
                INTENT_ACTION_TIME_TICK));
        Toast.makeText(getApplicationContext(), R.string.brodcast_is_on,
                Toast.LENGTH_SHORT).show();
    }


}
