package com.example.android.day11task1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.text.DecimalFormat;

public class MainActivity extends Activity implements View.OnClickListener {

    private TextView mCalculatorDisplay;
    private Boolean userIsInTheMiddleOfTypingANumber = false;
    private double operand;
    private static final String DIGITS = "0123456789.";
    private long calcObj;
    private final static String COUNT ="count";
    private final static String OPERAND ="operand";
    private final static String MEMORY ="memory";
    private final static String HELLOJINI ="hellojni";
    private final static String ZERO ="0";
    private final static String DOUBLE_ZERO ="0,0";
    private final static String POINT =".";

    DecimalFormat df = new DecimalFormat("@###########");

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        calcObj =  getObjCalc();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCalculatorDisplay = (TextView) findViewById(R.id.textView1);
        df.setMinimumFractionDigits(0);
        df.setMinimumIntegerDigits(1);
        df.setMaximumIntegerDigits(8);

        findViewById(R.id.button0).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
        findViewById(R.id.button9).setOnClickListener(this);

        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.buttonSubtract).setOnClickListener(this);
        findViewById(R.id.buttonMultiply).setOnClickListener(this);
        findViewById(R.id.buttonDivide).setOnClickListener(this);
        findViewById(R.id.buttonToggleSign).setOnClickListener(this);
        findViewById(R.id.buttonDecimalPoint).setOnClickListener(this);
        findViewById(R.id.buttonEquals).setOnClickListener(this);
        findViewById(R.id.buttonClear).setOnClickListener(this);
        findViewById(R.id.buttonClearMemory).setOnClickListener(this);
        findViewById(R.id.buttonAddToMemory).setOnClickListener(this);
        findViewById(R.id.buttonSubtractFromMemory).setOnClickListener(this);
        findViewById(R.id.buttonRecallMemory).setOnClickListener(this);
    }

    public native long getObjCalc();
    public native double getResult(long calculate);
    public native void setOperand(long calculate,double operand);
    public native void setMemory(long calculate,double calculatorMemory);
    public native double getMemory(long calculate);
    public native double performOperation(long calculate,String operation);

    static {
        System.loadLibrary(HELLOJINI);
    }
    @Override
    public void onClick(View v) {

        String buttonPressed = ((Button) v).getText().toString();

        if (DIGITS.contains(buttonPressed)) {
            if (userIsInTheMiddleOfTypingANumber) {
                if (buttonPressed.equals(POINT) && mCalculatorDisplay.getText().toString().contains(".")) {
                } else {
                    mCalculatorDisplay.append(buttonPressed);
                }
            } else {
                if (buttonPressed.equals(POINT)) {
                    mCalculatorDisplay.setText(0 + buttonPressed);
                } else {
                    mCalculatorDisplay.setText(buttonPressed);
                }
                userIsInTheMiddleOfTypingANumber = true;
            }
        } else {
            if (userIsInTheMiddleOfTypingANumber) {
                operand = Double.parseDouble(mCalculatorDisplay.getText().toString());
                setOperand(calcObj,operand );
                userIsInTheMiddleOfTypingANumber = false;
            }
            performOperation(calcObj,buttonPressed);
            mCalculatorDisplay.setText(df.format(getResult(calcObj)));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if( (mCalculatorDisplay.getText().toString().equals(DOUBLE_ZERO))) {
            outState.putString(COUNT, ZERO);
        }else {
            outState.putString(COUNT, mCalculatorDisplay.getText().toString());
            outState.putDouble(OPERAND, getResult(calcObj));
            outState.putDouble(MEMORY, getMemory(calcObj));
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(getResult(calcObj)== 0) {
            operand = Double.parseDouble(savedInstanceState.getString(COUNT));
            mCalculatorDisplay.setText(operand+"");
            userIsInTheMiddleOfTypingANumber = true;
        }else {
            setOperand(calcObj, savedInstanceState.getDouble(OPERAND));
            setMemory(calcObj, savedInstanceState.getDouble(MEMORY));
            mCalculatorDisplay.setText(df.format(getResult(calcObj)));
        }
    }
}