#include <android/log.h>
#include "Calculate.h"

extern "C" {
using namespace std;

JNIEXPORT jlong  JNICALL Java_com_example_android_day11task1_MainActivity_getObjCalc(JNIEnv* env, jobject inThis) {
    return (jlong) new Calculate();
}
JNIEXPORT jdouble  JNICALL Java_com_example_android_day11task1_MainActivity_getResult(JNIEnv* env, jobject inThis, jlong calculate) {
    Calculate *calculator = (Calculate*)calculate;
    jdouble res =  calculator->getResult();
    return res;
}
JNIEXPORT void  JNICALL Java_com_example_android_day11task1_MainActivity_setOperand(JNIEnv* env, jobject inThis, jlong calculate,jdouble operand) {
    Calculate *calculator = (Calculate*)calculate;
    calculator->setOperand(operand);
    }

JNIEXPORT void  JNICALL Java_com_example_android_day11task1_MainActivity_setMemory(JNIEnv* env, jobject inThis, jlong calculate,jdouble calculatorMemory) {
    Calculate *calculator = (Calculate*)calculate;
      calculator->setMemory(calculatorMemory);
    }
JNIEXPORT jdouble  JNICALL Java_com_example_android_day11task1_MainActivity_getMemory(JNIEnv* env, jobject inThis, jlong calculate) {
    Calculate *calculator = (Calculate*)calculate;
    jdouble res = calculator->getMemory();
    return res;
    }
JNIEXPORT jdouble  JNICALL Java_com_example_android_day11task1_MainActivity_performOperation(JNIEnv* env, jobject inThis, jlong calculate,jstring  operation) {
    const char *mathOps = env->GetStringUTFChars( operation, 0);
    Calculate *calculator = (Calculate*)calculate;
    jdouble res = calculator->performOperation(mathOps);
    return res;
    }


}







