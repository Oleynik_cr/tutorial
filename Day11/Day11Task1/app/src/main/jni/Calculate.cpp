//
// Created by android on 30/09/15.
//

#include <iostream>
#include "Calculate.h"
#include <android/log.h>
#define DEBUG_TAG "NDKSetupActivity"
using namespace std;
namespace {
    double mOperand = 0;
    double mWaitingOperand = 0;
    const char *mWaitingOperator = "";
    double mCalculatorMemory = 0;
    const char *ADD = "+";
    const char *SUBTRACT = "-";
    const char *MULTIPLY = "*";
    const char *DIVIDE = "/";
    const char *CLEAR = "C";
    const char *CLEARMEMORY = "MC";
    const char *ADDTOMEMORY = "M+";
    const char *SUBTRACTFROMMEMORY = "M-";
    const char *RECALLMEMORY = "MR";
    const char *TOGGLESIGN = "+/-";
}

void Calculate::setOperand(double operand) {
    mOperand = operand;
    // __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "test double = %f", operand);
}

double Calculate::getResult() {

    return mOperand;
}

void Calculate::setMemory(double calculatorMemory) {
    mCalculatorMemory = calculatorMemory;
}

double Calculate::getMemory() {
    return mCalculatorMemory;
}

double Calculate::performOperation(const char *operat) {
    if (strcmp(operat,CLEAR)== 0) {
        mOperand = 0;
        mWaitingOperator = "";
        mWaitingOperand = 0;
    } else if (strcmp(operat,CLEARMEMORY)== 0) {
        mCalculatorMemory = 0;
    } else if (strcmp(operat,ADDTOMEMORY)== 0) {
        mCalculatorMemory = mCalculatorMemory + mOperand;
    } else if (strcmp(operat,SUBTRACTFROMMEMORY)== 0) {
        mCalculatorMemory = mCalculatorMemory - mOperand;
    } else if (strcmp(operat,TOGGLESIGN)== 0) {
        mOperand = -mOperand;
    } else if (strcmp(operat,RECALLMEMORY)== 0) {
        mOperand = mCalculatorMemory;
    } else {
        performWaitingOperation();
        mWaitingOperator = operat;
        mWaitingOperand = mOperand;
    }
    return mOperand;
}

void Calculate::performWaitingOperation() {

    if (strcmp(mWaitingOperator,ADD)== 0) {
        mOperand = mWaitingOperand + mOperand;
    } else if (strcmp(mWaitingOperator,SUBTRACT)== 0) {
        mOperand = mWaitingOperand - mOperand;
    } else if (strcmp(mWaitingOperator,MULTIPLY)== 0) {
        mOperand = mWaitingOperand * mOperand;
    } else if (strcmp(mWaitingOperator,DIVIDE)== 0) {
        mOperand = mWaitingOperand / mOperand;
    }
}

