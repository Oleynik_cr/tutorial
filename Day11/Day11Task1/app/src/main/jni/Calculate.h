//
// Created by android on 30/09/15.
//

#ifndef DAY11TASK1_CALCULATE_H
#define DAY11TASK1_CALCULATE_H
#include "string"
#include "jni.h"

class Calculate {
private:
    void performWaitingOperation();

public:
    double getResult();
    void setOperand(double operand);
    void setMemory(double calculatorMemory);
    double getMemory();
    double performOperation(const char *operat);
   };


#endif //DAY11TASK1_CALCULATE_H
