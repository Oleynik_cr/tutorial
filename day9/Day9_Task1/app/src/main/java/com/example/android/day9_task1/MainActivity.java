package com.example.android.day9_task1;

import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import yuku.ambilwarna.AmbilWarnaDialog;


public class MainActivity extends AppCompatActivity {
    public  static boolean isRotate;
    private EditText editText;
    private Button btPause;
    private Button btStop;
    private Button btStart;
    private OrientationEventListener mOrientationListener;
    private DrawView drawView;
    private int color;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        drawView = (DrawView) findViewById(R.id.view);
        btPause = (Button) findViewById(R.id.btPause);
        btStop = (Button) findViewById(R.id.btStop);
        btStart = (Button) findViewById(R.id.btStart);
        btPause.setEnabled(false);
        btStop.setEnabled(false);
        isRotate = true;

        mOrientationListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {

            @Override
            public void onOrientationChanged(int orientation) {
                orientation = (orientation + 45)/ 90*90;
                if(!isRotate && (orientation == 0 || orientation == 360)) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    drawView.reversClock();
                    isRotate = true;
                }
                if(isRotate && orientation == 180) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                    drawView.reversClock();
                    isRotate = false;
                }
            }
        };
        if (mOrientationListener.canDetectOrientation() == true) {
            mOrientationListener.enable();
        } else {
            mOrientationListener.disable();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.glases:
                getColor(true);
                return true;
            case R.id.send:
                getColor(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  void onClick(View view) {
        switch (view.getId()){
            case R.id.btStart:
                if(!editText.getText().toString().equals("") && !editText.getText().toString().equals("0")) {
                    int  time = Integer.parseInt(editText.getText().toString());
                    drawView.startClock(time);
                    btPause.setEnabled(true);
                    btStop.setEnabled(true);
                    btStart.setEnabled(false);
                }else {
                    Toast toast = Toast.makeText(this, R.string.input_number,Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            case R.id.btStop:
                drawView.stopClock();
                btStart.setEnabled(true);
                break;
            case R.id.btPause:
                drawView.pauseClock();
                btStart.setEnabled(true);
                break;
            default:
                break;
        }
    }
    private void getColor(final boolean choice){
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, 0xff0000ff, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onCancel(AmbilWarnaDialog dialog){
            }
            @Override
            public void onOk(AmbilWarnaDialog dialog, int colors) {
                if(choice) {
                    drawView.setGlassColor(colors);
                }else {
                    drawView.setSandColor(colors);
                }
            }
        });
        dialog.show();
    }
    public void visibleButtonStart() {
        btStart.setEnabled(true);
    }
}