package com.example.android.day9_task1;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Handler;

import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class DrawView extends View {

    private int glassColor;
    private int sendColor;
    private int glassStrokeWidth;
    private int sendStrokeWidth;
    private int hadStrokeWidth;
    private int time;

    private Paint paintHand = new Paint();
    private Paint paintGlass = new Paint();
    private Paint paintSand = new Paint();
    private Path pathDrawGlaces = new Path();
    private Path pathDrawSend = new Path();
    private RectF rectHad;

    private float [] points;
    private static float DX = 0.01f;
    private volatile float LEVEL_SEND =  0.40f;
    private float MAX_LEVEL_SEND =  1 - LEVEL_SEND;
    private static int TIME_UPDATE = 50;
    private volatile   float T_DOWN =  1f;

    final Handler handler = new Handler();
    Timer timer;
    boolean isVisible = false;

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        init(attrs, 0);
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }


    private void init(AttributeSet attrs, int defStyle){

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.DrawView, defStyle, 0);
        glassColor = a.getColor(
                R.styleable.DrawView_glassColor,
                glassColor);
        sendColor = a.getColor(
                R.styleable.DrawView_sendColor,
                sendColor);
        glassStrokeWidth = a.getInteger(
                R.styleable.DrawView_glassStrokeWidth,
                glassStrokeWidth);
        sendStrokeWidth = a.getInteger(
                R.styleable.DrawView_sendStrokeWidth,
                sendStrokeWidth);
        hadStrokeWidth = a.getInteger(
                R.styleable.DrawView_hadStrokeWidth,
                hadStrokeWidth);
        time = a.getInteger(
                R.styleable.DrawView_hadStrokeWidth,
                time);
        a.recycle();
        defaultPaintAndMeasurements();
    }

    private void defaultPaintAndMeasurements() {
        paintGlass.setColor(glassColor);
        paintGlass.setStrokeWidth(glassStrokeWidth);
        paintSand.setColor(sendColor);
        paintSand.setStrokeWidth(sendStrokeWidth);
        paintHand.setColor(glassColor);
        paintHand.setStrokeWidth(hadStrokeWidth);
        setTime(time);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        final float  RIGHT_WIDTH_SCREEN =  width / 4;
        final float  HALF_WIDTH_SCREEN = width / 2;
        final float  FUL_WIDTH_SCREEN = width;
        final float  LEFT_WIDTH_SCREEN = width - (width / 4);

        final float   TOP_HEIGHT_SCREEN = height / 4;
        final float   HALF_HEIGHT_SCREEN =  height / 2;
        final float   BOTTOM_HEIGHT_SCREEN = height - (height / 4);
        final float   BOTTOM_INITIAL_HEIGHT_SCREEN = height - (height / 10);
        final float   TOP_INITIAL_HEIGHT_SCREEN = height / 10;

        rectHad = new RectF( FUL_WIDTH_SCREEN / 10, height / 15,  FUL_WIDTH_SCREEN - (
                FUL_WIDTH_SCREEN / 10), TOP_INITIAL_HEIGHT_SCREEN + 5);
        paintHand.setStyle(Paint.Style.FILL);
        canvas.drawRect(rectHad, paintHand);
        rectHad.offset(0, height - (height / 6) - 5);
        canvas.drawRect(rectHad, paintHand);

        paintGlass.setStyle(Paint.Style.STROKE);
        drawGlass(RIGHT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN, 0, TOP_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN - 10, HALF_HEIGHT_SCREEN);
        drawGlass(LEFT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN,  FUL_WIDTH_SCREEN,
                TOP_HEIGHT_SCREEN, HALF_WIDTH_SCREEN + 10, HALF_HEIGHT_SCREEN);
        drawGlass(RIGHT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, 0, BOTTOM_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN - 10, HALF_HEIGHT_SCREEN);
        drawGlass(LEFT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, FUL_WIDTH_SCREEN,
                BOTTOM_HEIGHT_SCREEN, HALF_WIDTH_SCREEN + 10, HALF_HEIGHT_SCREEN);
        canvas.drawPoint(width / 2 - 12, height / 2, paintGlass);
        canvas.drawPoint(width / 2 + 12, height / 2, paintGlass);
        canvas.drawPath(pathDrawGlaces, paintGlass);

        paintSand.setStyle(Paint.Style.FILL);
        points =  getPoint(RIGHT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN, 0, TOP_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, LEVEL_SEND);
        pathDrawSend.moveTo(points[0], points[1]);
        drawUpSend(RIGHT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN, 0, TOP_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, LEVEL_SEND, 0, DX);
        drawUpSend(HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN,FUL_WIDTH_SCREEN, TOP_HEIGHT_SCREEN,
                LEFT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN, 0, LEVEL_SEND, DX);
        points =  getPoint(LEFT_WIDTH_SCREEN, TOP_INITIAL_HEIGHT_SCREEN,  FUL_WIDTH_SCREEN,
                TOP_HEIGHT_SCREEN, HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, LEVEL_SEND);
        pathDrawSend.lineTo(points[0], points[1]);
        pathDrawSend.close();
        canvas.drawPath(pathDrawSend, paintSand);

        points =  getPoint(LEFT_WIDTH_SCREEN,BOTTOM_INITIAL_HEIGHT_SCREEN, FUL_WIDTH_SCREEN,
                BOTTOM_HEIGHT_SCREEN, HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN ,1 - T_DOWN);
        pathDrawSend.moveTo(points[0], points[1]);
        pathDrawSend.lineTo(RIGHT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN);
        drawBottomSend(LEFT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, FUL_WIDTH_SCREEN,
                BOTTOM_HEIGHT_SCREEN, HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, T_DOWN, DX);
        pathDrawSend.close();
        drawBottomSend(RIGHT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, 0, BOTTOM_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, T_DOWN, DX);
        points =  getPoint(RIGHT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, 0, BOTTOM_HEIGHT_SCREEN,
                HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN,1 - T_DOWN);
        pathDrawSend.lineTo(points[0], points[1]);
        pathDrawSend.close();

        if(isVisible) {
            points = getPoint(LEFT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, FUL_WIDTH_SCREEN,
                    BOTTOM_HEIGHT_SCREEN,  HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, 1 - T_DOWN);
            pathDrawSend.moveTo(points[0], points[1]);
            points = getPoint(RIGHT_WIDTH_SCREEN, BOTTOM_INITIAL_HEIGHT_SCREEN, 0,
                    BOTTOM_HEIGHT_SCREEN, HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN, 1 - T_DOWN-0.01f);
            pathDrawSend.lineTo(points[0], points[1]);
            pathDrawSend.lineTo(HALF_WIDTH_SCREEN, points[1] - TOP_INITIAL_HEIGHT_SCREEN / 3);
            pathDrawSend.close();
            canvas.drawPath(pathDrawSend, paintSand);
        }
        if(LEVEL_SEND < 1 && LEVEL_SEND != 0.40f) {
            canvas.drawLine(HALF_WIDTH_SCREEN, HALF_HEIGHT_SCREEN - 3, HALF_WIDTH_SCREEN,
                    BOTTOM_INITIAL_HEIGHT_SCREEN, paintSand);
        }
        pathDrawGlaces.reset();
        pathDrawSend.reset();
    }

    private  void  drawGlass(float P0x, float P0y, float P1x, float P1y, float P2x, float P2y){
        float resX;
        float resY;
        pathDrawGlaces.moveTo(P0x, P0y);
        for( float t = 0; t<=1 + DX /2 ; t+=DX) {
            resX = (float) (Math.pow((1 - t), 2) * P0x + 2 * (1 - t) * (t * P1x) + Math.pow(t, 2) * P2x);
            resY = (float) (Math.pow((1 - t), 2) * P0y + 2 * (1 - t) * (t * P1y) + Math.pow(t, 2) * P2y);
            pathDrawGlaces.lineTo(resX, resY);
        }
    }

    private float[] getPoint(float P0x, float P0y, float P1x, float P1y, float P2x, float P2y, float t) {
        float [] points = new float[2];
        float resX;
        float resY;
        resX = (float) (Math.pow((1 - t), 2) * P0x + 2 * (1 - t) * (t * P1x) + Math.pow(t, 2) * P2x);
        resY = (float) (Math.pow((1 - t), 2) * P0y + 2 * (1 - t) * (t * P1y) + Math.pow(t, 2) * P2y);
        points[0] = resX;
        points[1] = resY;
        return points;
    }

    private  void drawUpSend(float P0x, float P0y, float P1x, float P1y, float P2x, float P2y,
                             float from, float to, float DX) {
        float resX;
        float resY;
        for (float t = from; t <=1- to + DX / 2; t += DX) {
            resX = (float) (Math.pow((1 - t), 2) * P0x + 2 * (1 - t) * (t * P1x) + Math.pow(t, 2) * P2x);
            resY = (float) (Math.pow((1 - t), 2) * P0y + 2 * (1 - t) * (t * P1y) + Math.pow(t, 2) * P2y);
            pathDrawSend.lineTo(resX, resY);
        }
    }
    private  void drawBottomSend(float P0x, float P0y, float P1x, float P1y, float P2x, float P2y,
                                 float to, float DX) {
        float resX;
        float resY;
        for (float t = 0; t <= 1-to; t += DX) {
            resX = (float) (Math.pow((1 - t), 2) * P0x + 2 * (1 - t) * (t * P1x) + Math.pow(t, 2) * P2x);
            resY = (float) (Math.pow((1 - t), 2) * P0y + 2 * (1 - t) * (t * P1y) + Math.pow(t, 2) * P2y);
            pathDrawSend.lineTo(resX, resY);
        }
    }
    public void startClock(final int time) {
        this.time = time;
        defaultPaintAndMeasurements();
        isVisible = true;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible && (LEVEL_SEND >= 0.4f && LEVEL_SEND <= 0.6f)) {
                            LEVEL_SEND += getCorrectStep() - (getCorrectStep() / 5);
                            T_DOWN -= getCorrectStep() + getCorrectStep()/5  ;
                        }
                        if (isVisible && (LEVEL_SEND >= 0.6f && LEVEL_SEND <= 0.8f)) {
                            LEVEL_SEND += getCorrectStep() ;
                            T_DOWN -= getCorrectStep() - getCorrectStep()/5;
                        }
                        if (isVisible && (LEVEL_SEND >= 0.8f && LEVEL_SEND < 1f)) {
                            LEVEL_SEND += getCorrectStep() + (getCorrectStep() / 5);
                            T_DOWN -= getCorrectStep() - getCorrectStep()/5;
                        }
                        if (LEVEL_SEND >= 1) {
                            timer.cancel();
                            stopClock();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    ((MainActivity) getContext()).visibleButtonStart();
                                }
                            });
                        }
                        invalidate();
                    }
                });
            }
        }, 0, TIME_UPDATE);
    }
    public void stopClock() {
        if(timer != null) {
            isVisible = false;
            LEVEL_SEND = 0.40f;
            T_DOWN = 1f;
            invalidate();
            timer.cancel();
        }
    }
    public void pauseClock() {
        timer.cancel();
    }

    public void setGlassColor(int color){
        glassColor = color;
        defaultPaintAndMeasurements();
        invalidate();
    }

    public void setSandColor(int color){
        sendColor = color;
        defaultPaintAndMeasurements();
        invalidate();
    }

    public void reversClock() {
        if(T_DOWN == 1f) {
            LEVEL_SEND = 0.40f;
            T_DOWN = 1f;
            invalidate();
        }else {
            float byf = LEVEL_SEND;
            LEVEL_SEND = T_DOWN;
            T_DOWN = byf;
            invalidate();
        }
    }
    private  void setTime(int time) {
        this.time = time * 1000;
    }
    private int getTime(){
        return time;
    }
    private float getCorrectStep(){
        return MAX_LEVEL_SEND / (getTime() / TIME_UPDATE);
    }

}