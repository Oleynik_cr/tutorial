package com.example.android.day7task1.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.content.ContentProvider;
import com.activeandroid.util.SQLiteUtils;
import com.example.android.day7task1.R;
import com.example.android.day7task1.active_db.Photos;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;


public class LookMapFragment extends Fragment implements OnMapReadyCallback ,GoogleMap.OnMarkerClickListener,LoaderManager.LoaderCallbacks<Cursor> {

    private List<Photos> photoses;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private LayoutInflater myinflater;
    private TextView title;
    private ImageView photoImage;
    private HashMap<Marker,Photos > hmap;
    private View view;
    private OnHeadlineSelectedListener onHeadlineSelectedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getSupportLoaderManager().initLoader(2, null, this);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myinflater = inflater;
        view = myinflater.inflate(R.layout.map_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment =  (SupportMapFragment)
                this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        onHeadlineSelectedListener.screenName("Map screen", true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        LatLng startCoordinat = new LatLng(48.471, 35.023);
        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(startCoordinat, 10));
        map.setOnMarkerClickListener(this);
        hmap = new HashMap<>();
        for (Photos photo : photoses) {
            Marker melbourne  = map.addMarker(new MarkerOptions()
                    .position(new LatLng(photo.getCoordLatitude(), photo.getCoordLongitude())));
            hmap.put(melbourne,photo);
        }
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                goPreViewPhoto(hmap.get(marker));
                           }
        });
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                view = myinflater.inflate(R.layout.info_windov_item, null);
                title = (TextView) view.findViewById(R.id.text);
                title.setText(hmap.get(marker).getUrlPhoto());
                photoImage = (ImageView) view.findViewById(R.id.imageMarker);
                photoImage.setImageBitmap(PreviewPhotoFragment.miniImage(hmap.get(marker).getUrlPhoto(), 100, 100));
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    private  void goPreViewPhoto(Photos photo){
        PreviewPhotoFragment previewPhoto = new PreviewPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PreviewPhotoFragment.KEY_PHOTOS, photo);
        previewPhoto.setArguments(bundle);
        getFragmentManager().beginTransaction().add(R.id.fragment,  previewPhoto).addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.CursorLoader(getActivity(), ContentProvider.createUri(Photos.class, null), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        showList(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private void showList(Cursor cursor) {
              photoses = SQLiteUtils.processCursor(Photos.class, cursor);
    }

    @Override
    public void onDestroyView() {
       getFragmentManager().beginTransaction().remove(mapFragment).commit();
        super.onDestroyView();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener =null;
    }
}
