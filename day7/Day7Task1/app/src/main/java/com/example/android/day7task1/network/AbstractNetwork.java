package com.example.android.day7task1.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.widget.Toast;

import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.async_task.AsyncSetPhoto;
import com.example.android.day7task1.fragments.PreviewPhotoFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * Created by android on 07.09.15.
 */
public abstract class AbstractNetwork<T>  {
    private Context context;
    private PreviewPhotoFragment previewPhotoFragment;
    private Photos photos;
    private static final String TAG_FILE = "File";
    private static final String TAG_TIPE = "__type";
    private static final String TAG_GEOPOINT = "GeoPoint";
    private static final String TAG_UNIQ_NUM = "UniqueNumber";
    public  static final String URL ="https://api.parse.com/1/classes/PhotoStorage";
    public  static final String URL_QUERY ="https://api.parse.com/1/classes/PhotoStorage/?where=" + URLEncoder.encode("{\"UniqueNumber\":" + getUniqueNumber() + "}");
    public  static final String URL_PHOTO ="https://api.parse.com/1/files/pic.jpg";
    public   static final String X_PARSE_ID ="X-Parse-Application-Id";
    public  static final String X_PARSE_KEY ="k7AJ86gMYbCrvZ9XMg6HNEajyyP05jpnwwy3qIW6";
    public  static final String X_PARSE_REST ="X-Parse-REST-API-Key";
    public  static final String X_PARSE_REST_KEY  ="kZAHnx2Summ30toRo6hsY4okMEZIdvO9EKJRidEB";
    public  static final String CONTENT_TYPE ="Content-Type";
    public  static final String JSON_APLICK ="aplication/json";
    public  static final String IMAGE_JPEG ="image/jpeg";

    public AbstractNetwork(Context context,PreviewPhotoFragment previewPhotoFragment, Photos photos) {
        this.context = context;
        this.previewPhotoFragment = previewPhotoFragment;
        this.photos = photos;
    }

    public void startUploading() {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new AsyncSetPhoto(previewPhotoFragment,context,this).execute();
        } else {
            Toast toast = Toast.makeText(context, "Internet is no excuse", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public JSONObject writeJSONInfoPhoto(String jsonPhoto) {
        JSONObject object = new JSONObject();
        JSONObject coordinate = new JSONObject();
        JSONObject photo = null;
             try {
            photo = new JSONObject(jsonPhoto);
            photo.put(TAG_TIPE, TAG_FILE);
            coordinate.put(TAG_TIPE, TAG_GEOPOINT);
            coordinate.put(JSONParser.TAG_LATITUBE,photos.getCoordLatitude());
            coordinate.put(JSONParser.TAG_LONGITUBE,photos.getCoordLongitude());
            object.put(TAG_UNIQ_NUM,getUniqueNumber());
            object.put(JSONParser.TAG_PHOTO,photo);
            object.put(JSONParser.TAG_SRC_IN_PHONE,photos.getUrlPhoto());
            object.put(JSONParser.TAG_STATUS, true);
            object.put(JSONParser.TAG_COORDINATE,coordinate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public abstract T getInitialize(URL url);
    public abstract void setInfoPhoto(String jsonPhoto);
    public abstract void setPhoto();

    public static int getUniqueNumber() {
        String devInfo = Build.BOARD + Build.BRAND + Build.DEVICE
                + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT;
        String serial = null;
        try {
            serial = Build.class.getField("SERIAL").get(null).toString();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return new UUID(devInfo.hashCode() << 8, serial.hashCode() << 8).hashCode();
    }

}
