package com.example.android.day7task1.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.day7task1.MainActivity;
import com.example.android.day7task1.R;
import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.dialogs.DialogGetCoordinat;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class СhiefFragment extends Fragment  {

    private Button btnGetPhoto;
    private Button btnLookPhoto;
    private Button btnLookMap;
    private ImageView imageView;
    public static final int TAKE_COORD = 11;
    public static String URI ="uri";
    public static String LATITUBE ="latitybe";
    public static String LONGITUBE ="longitube";
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_TAKE_GPS = 2;
    private  Double latitude;
    private  Double longitude;
    private File photoFile;
    private String mCurrentPhotoPath;
    private LocationManager locationManager;
    private boolean isDialogShowed;
    private AlertDialog alertDialog;
    private OnHeadlineSelectedListener onHeadlineSelectedListener;
    DialogGetCoordinat dialogGetCoordinat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chief_fragment, container, false);
        setRetainInstance(true);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        btnGetPhoto = (Button) view.findViewById(R.id.btnGetPhoto);
        btnLookMap = (Button) view.findViewById(R.id.btnLookMap);
        btnLookPhoto = (Button) view.findViewById(R.id.btnLookPhoto);
        onHeadlineSelectedListener.screenName("Main screen",false);
        if (savedInstanceState != null) {
            isDialogShowed = savedInstanceState.getBoolean(MainActivity.IS_DIALOG_SHOW);
            if (isDialogShowed)
                showSettingsAlert();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnGetPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                locationManager = (LocationManager) getActivity()
                        .getSystemService(Activity.LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                        locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    startDialogCoord();
                }else {
                    showSettingsAlert();
                }
            }
        });
        btnLookMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goLookMap();
            }
        });
        btnLookPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LookPhotoFragment lookPhotoFragment = new LookPhotoFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment, lookPhotoFragment).addToBackStack(null)
                        .commit();
            }
        });
    }

    private void goLookMap() {
        LookMapFragment lookMapFragment = new LookMapFragment();
        getFragmentManager().beginTransaction().replace(R.id.fragment, lookMapFragment).addToBackStack(null)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_TAKE_GPS)
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                    locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                startDialogCoord();
            }else {
                Toast toast = Toast.makeText(getActivity(), "You did not include GPS or Internet, please include", Toast.LENGTH_LONG);
                toast.show();
            }
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_COORD: {
                    latitude = data.getDoubleExtra(LATITUBE, 0);
                    longitude = data.getDoubleExtra(LONGITUBE, 0);
                    dispatchTakePictureIntent();
                    break;
                }
                case REQUEST_TAKE_PHOTO: {
                    addDB();
                    break;
                }
                default:
                    break;
            }
        }
    }

    private File createImageFile()  {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            photoFile = createImageFile();
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void addDB() {
        Photos photos = new Photos();
        photos.setUrlPhoto(photoFile.getAbsolutePath());
        photos.setCoordLatitude(latitude);
        photos.setCoordLongitude(longitude);
        photos.setStatus(false);
        photos.save();
        goPreviewPhoto(photos);
    }

    private void goPreviewPhoto(Photos photos) {
        PreviewPhotoFragment previewPhoto = new PreviewPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PreviewPhotoFragment.KEY_PHOTOS, photos);
        previewPhoto.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.fragment, previewPhoto).addToBackStack(null)
                .commit();
    }

    private void startDialogCoord() {
        dialogGetCoordinat = new DialogGetCoordinat();
        dialogGetCoordinat.setTargetFragment(СhiefFragment.this, TAKE_COORD);
        dialogGetCoordinat.show(getFragmentManager(), null);
        dialogGetCoordinat.setCancelable(false);
    }
    public void showSettingsAlert() {
        AlertDialog.Builder  alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle("Attention");
        alertDialogBuilder
                .setTitle("GPS is settings")
                .setMessage("GPS is not enabled. Do you want to go to settings menu?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_TAKE_GPS);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(MainActivity.IS_DIALOG_SHOW, alertDialog != null && alertDialog.isShowing());
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }
    private boolean checkInternet() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast toast = Toast.makeText(getActivity(), "Internet is no excuse", Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener =null;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }

}

