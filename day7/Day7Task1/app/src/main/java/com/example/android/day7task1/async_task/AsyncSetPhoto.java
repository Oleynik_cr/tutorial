package com.example.android.day7task1.async_task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.android.day7task1.fragments.PreviewPhotoFragment;
import com.example.android.day7task1.network.AbstractNetwork;


public class AsyncSetPhoto extends AsyncTask<String, Void, String> {
    private Context context;
    private  PreviewPhotoFragment previewPhotoFragment;
    private AbstractNetwork network;
    public AsyncSetPhoto(PreviewPhotoFragment previewPhotoFragment,Context context, AbstractNetwork network) {
        this.previewPhotoFragment = previewPhotoFragment;
        this.context = context;
        this.network = network;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        previewPhotoFragment.startDialog();
    }

    @Override
    protected String doInBackground(String... urls) {
            network.setPhoto();
               return null;
    }
    @Override
    protected void onPostExecute(String result) {
        previewPhotoFragment.closeDialog();
        previewPhotoFragment.updateStatusPhoto();
        Toast toast = Toast.makeText(context, "Photo uploaded", Toast.LENGTH_LONG);
        toast.show();
    }
}

