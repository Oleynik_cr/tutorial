package com.example.android.day7task1.network.RetroFit;

import android.content.Context;

import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.fragments.PreviewPhotoFragment;
import com.example.android.day7task1.network.AbstractNetwork;

import java.io.File;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;

/**
 * Created by android on 9/11/15.
 */
public class RetrifitConection  extends AbstractNetwork {

    Photos photos;

    public RetrifitConection(Context context, PreviewPhotoFragment previewPhotoFragment, Photos photos) {
        super(context, previewPhotoFragment, photos);
        this.photos = photos;
    }

    @Override
       public RestAdapter getInitialize(java.net.URL url) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.parse.com")
                .setRequestInterceptor(getIntercepter())
                .build();
        return restAdapter;

    }
    @Override
    public void setInfoPhoto(String jsonPhoto) {

        TypedInput in = new TypedByteArray("aplication/json", writeJSONInfoPhoto(jsonPhoto).toString().getBytes());
        API service = getInitialize(null).create(API.class);
        Response r = service.setPhotoInfo(in);
    }

    @Override
    public void setPhoto() {
        File file = new File(photos.getUrlPhoto());
        API service = getInitialize(null).create(API.class);
        Response r = service.setPhoto(new TypedFile(IMAGE_JPEG, file));
        setInfoPhoto(new String(((TypedByteArray) r.getBody()).getBytes()));
    }

    public  static  RequestInterceptor   getIntercepter() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(X_PARSE_ID, X_PARSE_KEY);
                request.addHeader(X_PARSE_REST, X_PARSE_REST_KEY);
            }
        };
        return requestInterceptor;
    }
}

