package com.example.android.day7task1.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.day7task1.MainActivity;
import com.example.android.day7task1.R;
import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.fragments.PreviewPhotoFragment;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;


public class PhotoGrideView extends ArrayAdapter<Photos> {
    Context context;
    Photos photos;


    public PhotoGrideView(Context context,int resource, List<Photos> dataset) {
        super(context,resource,dataset);
        this.context = context;

    }
    static class ViewHolder {
        TextView textView;
        ImageView imageView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
         photos = getItem(position);

        if(convertView == null) {
            LayoutInflater ltInflate = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            convertView = ltInflate.inflate(R.layout.photo_card, parent,false);
            vh = new ViewHolder();
            vh.textView =  (TextView)   convertView.findViewById(R.id.tv_recycler_item);
            vh.imageView = (ImageView) convertView.findViewById(R.id.imagePhoto);
            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.textView.setText(photos.getCoordLatitude() + " " + photos.getCoordLongitude());

        Picasso.with(context)
                .load(new File(photos.getUrlPhoto()))
                .resize(250,250)
                .centerCrop()
                .into(vh.imageView);
        //  new CustomAsyncTask(vh).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, position);
        return convertView;
    }

    private class CustomAsyncTask extends AsyncTask<Integer, Void, Bitmap> {
        private ViewHolder holder;
        CustomAsyncTask(ViewHolder holder) {
            this.holder = holder;
        }
        @Override
        protected Bitmap doInBackground(Integer... params) {
            String filename = photos.getUrlPhoto();
            Bitmap b;
            if ((b = MainActivity.cache.get(filename)) != null)
                return b;

            b = PreviewPhotoFragment.miniImage(filename, 350, 250);
            MainActivity.cache.put(filename, b);
            return b;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            holder.imageView.setImageBitmap(bitmap);
        }
    }
}
