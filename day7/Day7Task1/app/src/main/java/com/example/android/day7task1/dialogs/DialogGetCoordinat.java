package com.example.android.day7task1.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.example.android.day7task1.R;
import com.example.android.day7task1.fragments.СhiefFragment;
import com.example.android.day7task1.gps.GPS;


public class DialogGetCoordinat extends DialogFragment {
    GPS gps;
    private boolean isEnable;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        setRetainInstance(true);
        final View v = inflater.inflate(R.layout.dialog_get_coordinat, null);
        if (savedInstanceState == null) {
            gps = new GPS(getActivity(), this).start();
            isEnable = true;
        }
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setCancelable(true)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        isEnable = false;
                    }
                }).create();
    }

    public void setLocation(Location location) {
        if(isEnable) {
            Intent intent = new Intent();
            intent.putExtra(СhiefFragment.LATITUBE, location.getLatitude());
            intent.putExtra(СhiefFragment.LONGITUBE, location.getLongitude());

            getTargetFragment().onActivityResult(
                    getTargetRequestCode(),
                    Activity.RESULT_OK,
                    intent
            );
            dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
        gps = null;
    }

}