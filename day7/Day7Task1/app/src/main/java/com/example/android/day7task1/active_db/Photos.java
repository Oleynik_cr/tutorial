package com.example.android.day7task1.active_db;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

@Table(name = "photos", id = "_id")
public class Photos extends Model implements Serializable {

    @Column(name = "url_photo")
    public String urlPhoto;

    @Column(name = "coordinates_latitude")
    public double coordLatitude;

    @Column(name = "coordinates_longitude")
    public double coordLongitude;

    @Column(name = "status")
    public boolean status;

    public String urlDounLoadImage;

    public Photos() {
        super();
    }

    public Photos(String urlPhoto, double coordLatitude, double coordLongitude, boolean status) {
        this.urlPhoto = urlPhoto;
        this.coordLatitude = coordLatitude;
        this.coordLongitude = coordLongitude;
        this.status = status;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public double getCoordLatitude() {
        return coordLatitude;
    }

    public void setCoordLatitude(double coordLatitude) {
        this.coordLatitude = coordLatitude;
    }

    public double getCoordLongitude() {
        return coordLongitude;
    }

    public void setCoordLongitude(double coordLongitude) {
        this.coordLongitude = coordLongitude;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus( boolean status) {
        this.status = status;
    }

    public String getUrlDounLoadImage() {
        return urlDounLoadImage;
    }

    public void setUrlDounLoadImage(String urlDounLoadImage) {
        this.urlDounLoadImage = urlDounLoadImage;
    }
}
