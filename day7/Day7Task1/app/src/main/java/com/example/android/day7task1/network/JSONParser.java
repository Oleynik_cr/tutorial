package com.example.android.day7task1.network;

import com.example.android.day7task1.active_db.Photos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by android on 08.09.15.
 */
public class JSONParser {
    private static final String TAG_RESULTS = "results";
    public static final  String TAG_COORDINATE ="coordinate";
    public static final  String TAG_LATITUBE ="latitude";
    public static final  String TAG_LONGITUBE ="longitude";
    public static final  String TAG_PHOTO ="photo";
    private static final  String TAG_PHOTO_NAME ="name";
    private static final  String TAG_DOUNLAD_PHOTO_URL ="url";
    public static final  String TAG_SRC_IN_PHONE ="src";
    public static final  String TAG_STATUS ="status";
    private static final  int STATUS = 1;

    public ArrayList<Photos> startParse(String jsonStr) {
        JSONArray results;
        ArrayList<Photos> photoses = new ArrayList<>();
        if (jsonStr != null) {
            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(jsonStr);
                results = jsonObj.getJSONArray(TAG_RESULTS);

                for (int i = 0; i < results.length(); i++) {
                    Photos photos = new Photos();
                    JSONObject res = results.getJSONObject(i);
                    photos.setUrlPhoto(res.getString(TAG_SRC_IN_PHONE));
                    photos.setStatus(true);

                    JSONObject coordinate = res.getJSONObject(TAG_COORDINATE);
                    photos.setCoordLatitude(coordinate.getDouble(TAG_LATITUBE));
                    photos.setCoordLongitude(coordinate.getDouble(TAG_LONGITUBE));

                    JSONObject photo = res.getJSONObject(TAG_PHOTO);
                    photos.setUrlDounLoadImage(photo.getString(TAG_DOUNLAD_PHOTO_URL));
                    photoses.add(photos);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return photoses;
    }
}
