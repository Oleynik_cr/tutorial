package com.example.android.day7task1.network;

import com.example.android.day7task1.dialogs.DialogSynchronization;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


public class URLSynchronization extends AbstractSynchronization {
    public URLSynchronization(DialogSynchronization dialogSynchronization) {
        super(dialogSynchronization);
    }

    public void getData() {
        HttpURLConnection connection = null;
        BufferedReader rd;
        StringBuilder sb;
        String line;
        URL serverAddress;
        try {
            serverAddress = new URL(URlConnections.URL_QUERY);
            connection = null;
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty(URlConnections.X_PARSE_ID, URlConnections.X_PARSE_KEY);
            connection.setRequestProperty(URlConnections.X_PARSE_REST, URlConnections.X_PARSE_REST_KEY);
            connection.setReadTimeout(10000);
            connection.connect();
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line + '\n');
            }
            downloadImage(new JSONParser().startParse(sb.toString()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            connection.disconnect();
            rd = null;
            sb = null;
            connection = null;
        }
    }


}
