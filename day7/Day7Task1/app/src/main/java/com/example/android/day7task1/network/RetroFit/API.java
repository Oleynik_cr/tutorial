package com.example.android.day7task1.network.RetroFit;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;

/**
 * Created by android on 9/11/15.
 */
public interface API  {


    @GET("/1/classes/PhotoStorage/")
    Response getPhotos(@Query("where") String where);

    @POST("/1/files/pic.jpg")
    Response setPhoto(@Body TypedFile file);

    @POST("/1/classes/PhotoStorage/")
   Response setPhotoInfo(@Body TypedInput body);

}
