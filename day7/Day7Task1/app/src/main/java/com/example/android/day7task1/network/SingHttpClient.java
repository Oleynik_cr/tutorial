package com.example.android.day7task1.network;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;


public class SingHttpClient {
    private static HttpClient instance = null;

    public  static HttpClient getInstance() {
        if(instance == null) {
            instance = new DefaultHttpClient();
        }
        return instance;
    }
}
