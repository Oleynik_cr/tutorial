package com.example.android.day7task1.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.activeandroid.content.ContentProvider;
import com.activeandroid.util.SQLiteUtils;
import com.example.android.day7task1.R;
import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.adapters.PhotoGrideView;

import java.util.List;


public class LookPhotoFragment extends Fragment implements   LoaderManager.LoaderCallbacks<Cursor> {

    private List<Photos> myDataset;
    private GridView gridView;
    private PhotoGrideView photoGrideView;
    private OnHeadlineSelectedListener onHeadlineSelectedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vew_photos, container, false);
       // setRetainInstance(true);
        gridView = (GridView) view.findViewById(R.id.gridView);
        onHeadlineSelectedListener.screenName("All photos",true);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                PreviewPhotoFragment previewPhoto = new PreviewPhotoFragment();
                Photos photos = photoGrideView.getItem(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable(PreviewPhotoFragment.KEY_PHOTOS, photos);
                previewPhoto.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment,  previewPhoto).addToBackStack(null)
                        .commit();
            }
        });
        getActivity().getSupportLoaderManager().initLoader(1, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.CursorLoader(getActivity(), ContentProvider.createUri(Photos.class, null), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        showList(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void showList(Cursor cursor) {
        if(isAdded()) {
            myDataset = SQLiteUtils.processCursor(Photos.class, cursor);
            photoGrideView = new PhotoGrideView(getActivity(), 0, myDataset);
            gridView.setAdapter(photoGrideView);
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener =null;
    }
}



