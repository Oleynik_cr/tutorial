package com.example.android.day7task1.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.fragments.PreviewPhotoFragment;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class URlConnections extends AbstractNetwork {


    private Photos photos;
    public URlConnections(Context context, PreviewPhotoFragment previewPhoto, Photos photos) {
        super(context, previewPhoto, photos);
        this.photos = photos;
    }

    public   HttpURLConnection getInitialize(URL url){
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty(X_PARSE_ID, X_PARSE_KEY);
            conn.setRequestProperty(X_PARSE_REST, X_PARSE_REST_KEY );
            conn.setRequestProperty(CONTENT_TYPE, JSON_APLICK);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setDoOutput(true);
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void setInfoPhoto(String jsonPhoto)  {
        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(URL);
            conn =  getInitialize(url);
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(writeJSONInfoPhoto(jsonPhoto).toString());
            out.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            conn.disconnect();
        }
    }

    public void setPhoto()  {

        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(URL_PHOTO);
            conn =  getInitialize(url);
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.write(getBite());
            out.close();
            out.flush();
            InputStream responseStream = new BufferedInputStream(conn.getInputStream());
            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null) {
                stringBuilder.append(line).append("");
            }
            responseStreamReader.close();
            setInfoPhoto(stringBuilder.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            conn.disconnect();
        }
    }

    private  byte [] getBite() {
        File file = new File(photos.getUrlPhoto());
        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


}
