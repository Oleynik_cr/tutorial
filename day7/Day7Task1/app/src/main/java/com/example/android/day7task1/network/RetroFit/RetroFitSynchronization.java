package com.example.android.day7task1.network.RetroFit;

import com.example.android.day7task1.dialogs.DialogSynchronization;
import com.example.android.day7task1.network.AbstractNetwork;
import com.example.android.day7task1.network.AbstractSynchronization;
import com.example.android.day7task1.network.JSONParser;

import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by android on 9/11/15.
 */
public class RetroFitSynchronization extends AbstractSynchronization {

    private  static  final String QUERY = "{\"UniqueNumber\":" + AbstractNetwork.getUniqueNumber() + "}";

    public RetroFitSynchronization(DialogSynchronization dialogSynchronization) {
        super(dialogSynchronization);
    }
    public   void getData(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.parse.com")
                .setRequestInterceptor(RetrifitConection.getIntercepter())
                .build();
        API service = restAdapter.create(API.class);
        Response r = service.getPhotos(QUERY);
        downloadImage(new JSONParser().startParse(new String(((TypedByteArray) r.getBody()).getBytes())));
    }
}
