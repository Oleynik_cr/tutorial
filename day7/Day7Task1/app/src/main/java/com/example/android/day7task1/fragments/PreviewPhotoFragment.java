package com.example.android.day7task1.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.android.day7task1.R;
import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.network.AbstractNetwork;
import com.example.android.day7task1.network.RetroFit.RetrifitConection;

import java.io.File;
import java.io.IOException;

/**
 * Created by android on 27.08.15.
 */
public class PreviewPhotoFragment extends Fragment
{
    private  ImageView imageView;
    private  Button btnCommit;
    private ProgressDialog progressDialog;
    private Photos photos;
    public static String KEY_PHOTOS ="photos";
    private static final String IS_DIALOG_SHOW = "isDialogShow";
    private boolean isDialogShowed;
    private OnHeadlineSelectedListener onHeadlineSelectedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle bundle = getArguments();
        if (bundle != null)
            photos = (Photos) bundle.getSerializable(KEY_PHOTOS);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_DIALOG_SHOW, progressDialog != null && progressDialog.isShowing());
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preview_photo_fragment, container, false);
        imageView = (ImageView) view.findViewById(R.id.prevPhoto);
        btnCommit = (Button) view.findViewById(R.id.btnParseCom);
        onHeadlineSelectedListener.screenName("Preview Photo",true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayMetrics displaymetrics = getResources().getDisplayMetrics();
        if (savedInstanceState != null) {
            isDialogShowed = savedInstanceState.getBoolean(IS_DIALOG_SHOW);
            if (isDialogShowed)
                startDialog();
        }
        imageView.setImageBitmap(miniImage(photos.getUrlPhoto(), displaymetrics.heightPixels, displaymetrics.widthPixels));
        imageView.setRotation(getExifOrientation(photos.getUrlPhoto()));
        if(photos.status)
            btnCommit.setEnabled(false);
        btnCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPhoto();
            }
        });
    }

    public static Bitmap miniImage(String uri, int newWidth, int newHeight) {
        File file = new File(uri);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        options.inSampleSize = calculateInSampleSize(options, newWidth, newHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 2;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private void  uploadPhoto() {
        AbstractNetwork myHttpClient = new RetrifitConection(getActivity(), this, photos);
        myHttpClient.startUploading();

    }
    public  void updateStatusPhoto() {
        photos.setStatus(true);
        photos.save();
        btnCommit.setEnabled(false);
    }

    public static int getExifOrientation(String filepath){
        int degree = 0;
        ExifInterface exif=null;
        try {
            exif=new ExifInterface(filepath);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        if (exif != null) {
            int orientation=exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,-1);
            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree=90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree=180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree=270;
                        break;
                }
            }
        }
        return degree;
    }

    public void startDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Attention");
        progressDialog.setMessage("upload photos to Parse.com");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
    public  void closeDialog() {
        if (!isAdded()) {
            isDialogShowed = false;
            return;
        }
        if (progressDialog == null || !progressDialog.isShowing())
            return;
        progressDialog.dismiss();
        progressDialog = null;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener = null;
    }
}




