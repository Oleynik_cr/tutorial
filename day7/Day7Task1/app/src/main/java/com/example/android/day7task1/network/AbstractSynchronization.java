package com.example.android.day7task1.network;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.dialogs.DialogSynchronization;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public abstract class AbstractSynchronization {

    Handler handler = new Handler();
    DialogSynchronization dialogSynchronization;

    public AbstractSynchronization( DialogSynchronization dialogSynchronization) {
        this.dialogSynchronization = dialogSynchronization;
    }

    public void downloadImage(final ArrayList<Photos> photoses) {
        URL url;
        InputStream in;
        ByteArrayOutputStream out;
        FileOutputStream fos;
        byte[] buf;
        byte[] response;
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialogSynchronization.setMaxProgres(photoses.size());
            }
        });
        int counter = 0;
        for(Photos photos:photoses)
        {
            final int temp = counter;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    dialogSynchronization.updateProgres(temp);
                }
            });
            counter++;
            try {
                url = new URL(photos.getUrlDounLoadImage());
                in = new BufferedInputStream(url.openStream());
                out = new ByteArrayOutputStream();
                buf = new byte[1024];
                int n = 0;
                while (-1 != (n = in.read(buf))) {
                    out.write(buf, 0, n);
                }
                out.close();
                in.close();
                response = out.toByteArray();
                fos = new FileOutputStream(photos.getUrlPhoto());
                fos.write(response);
                fos.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialogSynchronization.downLoadFailed();
                    }
                });

                e.printStackTrace();
            }
            finally {
                url = null;
                in = null;
                out = null;
                fos = null;
                buf = null;
                response = null;
            }
            photos.save();
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                dialogSynchronization.closeDialog();
            }
        });
    }

    public abstract void getData();
}
