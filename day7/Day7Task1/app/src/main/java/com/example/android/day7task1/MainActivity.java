package com.example.android.day7task1;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.android.day7task1.dialogs.DialogSynchronization;
import com.example.android.day7task1.fragments.OnHeadlineSelectedListener;
import com.example.android.day7task1.fragments.СhiefFragment;

import static com.activeandroid.ActiveAndroid.initialize;

public class MainActivity extends AppCompatActivity implements OnHeadlineSelectedListener {

    public static final String PREFS_NAME = "AOP_PREFS";
    public static final String PREFS_KEY = "AOP_PREFS_Boolen";
    public static final String IS_DIALOG_SHOW = "isDialogShow";
    private boolean isDialogShowed;
    private AlertDialog alertDialog;
    android.support.v7.app.ActionBar actionBar;


    public static final LruCache<String, Bitmap> cache =
            new LruCache<>(getDefaultSizeCache());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize(this);
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);

        if(savedInstanceState == null) {
            СhiefFragment сhiefFragment = new СhiefFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment, сhiefFragment).commit();
            getFlagLancher();
        }else{
            isDialogShowed = savedInstanceState.getBoolean(IS_DIALOG_SHOW);
            if (isDialogShowed)
                startDialog();
        }
    }

    private  void setFlagLancher() {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = settings.edit();
        editor.putBoolean(PREFS_KEY, false);
        editor.commit();
    }
    public void getFlagLancher() {
        SharedPreferences settings;
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        if (settings.getBoolean(PREFS_KEY, true)) {
            if(checkInternet()) {
                startDialog();
            }
        }
    }
    public void startDialog() {
        AlertDialog.Builder  alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle("Attention");
        alertDialogBuilder
                .setMessage("Synchronize with Parse.com?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DialogSynchronization dialogSynchronization = new DialogSynchronization();
                        dialogSynchronization.show(getFragmentManager(), null);
                        dialogSynchronization.setCancelable(false);
                        dialog.dismiss();
                         setFlagLancher();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getSupportFragmentManager().popBackStack();
        return false;
    }

    private static int getDefaultSizeCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
               final int cacheSize = maxMemory / 4;
        return cacheSize;
    }
    @Override
    public void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_DIALOG_SHOW, alertDialog != null && alertDialog.isShowing());
        super.onSaveInstanceState(outState);
    }
    private boolean checkInternet() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast toast = Toast.makeText(this, "Internet is no excuse, you need to synchronize ", Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
    }

    @Override
    public void screenName(String name, boolean visible) {
        actionBar.setTitle(name);
        actionBar.setDisplayHomeAsUpEnabled(visible);
    }

    @Override
    protected void onDestroy() {
        if(alertDialog != null)
            alertDialog.dismiss();
        super.onDestroy();
    }
}
