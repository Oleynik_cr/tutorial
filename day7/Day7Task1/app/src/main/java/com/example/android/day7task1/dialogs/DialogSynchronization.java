package com.example.android.day7task1.dialogs;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.day7task1.MainActivity;
import com.example.android.day7task1.R;
import com.example.android.day7task1.network.AbstractSynchronization;
import com.example.android.day7task1.network.RetroFit.RetroFitSynchronization;


/**
 * Created by android on 07.09.15.
 */
public class DialogSynchronization  extends DialogFragment {
    ProgressBar progressBar;
    TextView textView;

    private Integer progressMax;
    private boolean isDialogShowed;
    private static final String IS_DIALOG_SHOW = "isDialogShow";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
           if (savedInstanceState == null) {
               final AbstractSynchronization sync = new RetroFitSynchronization(this);
               new Thread(new Runnable() {
                   public void run() {
                       sync.getData();
                   }
               }).start();
           }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        getDialog().setTitle("Attention");
        View v = inflater.inflate(R.layout.dialog_get_synchronization, container, false);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        textView = (TextView) v.findViewById(R.id.textProgres);
        progressBar.setProgress(0);
        if (progressMax != null)
            progressBar.setMax(progressMax);
        return v;
    }


    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }
    public void closeDialog() {

        dismissAllowingStateLoss();
    }
    public void updateProgres(int i) {
        progressBar.setProgress(i);
        textView.setText(i + "/" + progressBar.getMax());
    }
    public void setMaxProgres(int i) {
        progressMax = i;
        progressBar.setMax(i);
    }

    public void downLoadFailed() {
        Toast.makeText(getActivity(), "loading failed, try again", Toast.LENGTH_LONG).show();
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = getActivity().getSharedPreferences(MainActivity.PREFS_NAME, MainActivity.MODE_PRIVATE);
        editor = settings.edit();
        editor.putBoolean(MainActivity.PREFS_KEY, true);
        editor.apply();

    }


}
