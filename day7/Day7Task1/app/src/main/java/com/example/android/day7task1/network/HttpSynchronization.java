package com.example.android.day7task1.network;

import com.example.android.day7task1.dialogs.DialogSynchronization;
import com.loopj.android.http.HttpGet;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class HttpSynchronization extends AbstractSynchronization {

    public HttpSynchronization(DialogSynchronization dialogSynchronization) {
        super(dialogSynchronization);
    }

    public  void getData() {
        BufferedReader reader;
        HttpClient httpclient = SingHttpClient.getInstance();
        HttpGet httpget = new HttpGet(HttpClientConnection.URL_QUERY);
        httpget.setHeader(HttpClientConnection.X_PARSE_ID, HttpClientConnection.X_PARSE_KEY);
        httpget.setHeader(HttpClientConnection.X_PARSE_REST,HttpClientConnection.X_PARSE_REST_KEY);
        InputStream is;
        HttpResponse response;
        StringBuilder sb;
        HttpEntity entity;
        try {
            response = httpclient.execute(httpget);
            entity = response.getEntity();
            is = entity.getContent();
            reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
                sb.append(line + "\n");
            String resString = sb.toString();
            downloadImage(new JSONParser().startParse(resString));
            is.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            is = null;
            response = null;
            sb = null;
            entity = null;
            reader = null;
        }
    }
}

