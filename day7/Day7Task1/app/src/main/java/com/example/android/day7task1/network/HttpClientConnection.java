package com.example.android.day7task1.network;

import android.content.Context;

import com.example.android.day7task1.active_db.Photos;
import com.example.android.day7task1.fragments.PreviewPhotoFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by android on 01.09.15.
 */
public class HttpClientConnection extends AbstractNetwork {

    private  Photos photos;

    public HttpClientConnection(Context context, PreviewPhotoFragment previewPhotoFragment, Photos photos) {
        super(context, previewPhotoFragment, photos);
        this.photos = photos;
           }

    public HttpPost getInitialize(URL url)  {
        HttpPost post = new HttpPost(String.valueOf(url));
        post.setHeader(X_PARSE_ID, X_PARSE_KEY);
        post.setHeader(X_PARSE_REST, X_PARSE_REST_KEY);
        post.setHeader(CONTENT_TYPE, JSON_APLICK);
        return post;
    }
    public void setInfoPhoto(String jsonPhoto) {
        java.net.URL url = null;
        HttpClient client;
        HttpPost post;
        StringEntity se;
        ResponseHandler responseHandler;
        try {
            url = new URL(URL);
            client = SingHttpClient.getInstance();
            post = getInitialize(url);
            se = new StringEntity(writeJSONInfoPhoto(jsonPhoto).toString());
            post.setEntity(se);
            responseHandler = new BasicResponseHandler();
            client.execute(post, responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            url = null;
            client = null;
            post = null;
            se = null;
            responseHandler = null;
        }
    }

    public void setPhoto() {
        java.net.URL url = null;
        HttpClient client;
        HttpPost post;
        File file;
        FileEntity reqEntity;
        ResponseHandler responseHandler;
        StringBuilder s;
        HttpResponse response;
        BufferedReader reader;
        try {
            url = new URL(URL_PHOTO);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        client = SingHttpClient.getInstance();
        post = getInitialize(url);
        file = new File(photos.getUrlPhoto());
        reqEntity = new FileEntity(file, IMAGE_JPEG);
        post.setEntity(reqEntity);
        try {
            response = client.execute(post);
            responseHandler = new BasicResponseHandler();
            client.execute(post, responseHandler);
            reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            setInfoPhoto(s.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            url = null;
            client = null;
            post = null;
            file  = null;
            reqEntity = null;
            responseHandler = null;
            s = null;
            response = null;
            reader = null;
        }
    }
}

