package com.example.android.day4task2.fragment;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.example.android.day4task2.R;

/**
 * Created by android on 8/13/15.
 */
public class FirstFragment extends Fragment {

    FragmentTransaction fTrans;
    private Button button;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment, container, false);


        button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goNextFragment();

            }
        });
        return view;
    }


    private void goNextFragment()
   {
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.fragment,new SecondFragment());
        fTrans.addToBackStack("1");
        fTrans.commit();


    }


}
