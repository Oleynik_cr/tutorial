package com.example.android.day4task2;



import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import android.support.v4.app.FragmentTransaction;

import com.example.android.day4task2.fragment.FirstFragment;

public class MainActivity extends FragmentActivity {


    FragmentTransaction fTrans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.fragment, new FirstFragment());
        fTrans.addToBackStack("0");
        fTrans.commit();


    }


}
