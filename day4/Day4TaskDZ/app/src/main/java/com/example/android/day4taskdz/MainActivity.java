package com.example.android.day4taskdz;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android.day4taskdz.fragment.Fragment1;
import com.example.android.day4taskdz.fragment.Fragment2;
import com.example.android.day4taskdz.fragment.Fragment3;


public class MainActivity extends AppCompatActivity {


    private int     position = 0;
    private Toolbar toolbar;
    private  Fragment1 fragment1;
    private  Fragment2 fragment2;
    private  Fragment3 fragment3;
    FragmentTransaction   fTrans;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        fragment3 = new Fragment3();

        setSupportActionBar(toolbar);

        getCorrectFragment(position++);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            if (position == 2) {
                getCorrectFragment(position);
                position = 0;
            } else{
                getCorrectFragment(position++);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private  void getCorrectFragment(int position)
    {
           fTrans = getSupportFragmentManager().beginTransaction();
        switch (position)
        {
            case 0:
                fTrans.replace(R.id.fragment1, fragment1);
                fTrans.replace(R.id.fragment2, fragment2);
                break;
            case 1:
                fTrans.replace(R.id.fragment1, fragment3);
                fTrans.replace(R.id.fragment2, fragment1);
                break;
            case 2:
                fTrans.replace(R.id.fragment1, fragment3);
                fTrans.replace(R.id.fragment2, fragment2);
                break;
            default:
                break;
        }
        fTrans.commit();
    }
}
