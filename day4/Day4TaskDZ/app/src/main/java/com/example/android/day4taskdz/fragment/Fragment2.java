package com.example.android.day4taskdz.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.day4taskdz.R;


public class Fragment2 extends Fragment {

    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment2, container, false);
        textView = (TextView) view.findViewById(R.id.textView);
        textView.setText("Fragment: 2");
        setRetainInstance(true);
        return view;
    }

}