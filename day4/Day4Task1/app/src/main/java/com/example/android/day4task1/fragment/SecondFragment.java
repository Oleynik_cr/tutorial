package com.example.android.day4task1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.TextView;

import com.example.android.day4task1.R;
import com.example.android.day4task1.SecondActivity;


public class SecondFragment extends Fragment {
    private TextView textView;
    private Button button;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.second_fragment, container, false);
        textView = (TextView) view.findViewById(R.id.textView);

        button = (Button) view.findViewById(R.id.btnSecondFragment);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SecondActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    public void setMessage(String message) {
        textView.setText(message);
    }
}
