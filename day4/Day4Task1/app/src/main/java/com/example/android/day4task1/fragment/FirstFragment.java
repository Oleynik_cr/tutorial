package com.example.android.day4task1.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.day4task1.OnHeadlineSelectedListener;
import com.example.android.day4task1.R;

/**
 * Created by android on 8/12/15.
 */
public class FirstFragment extends Fragment {
    private OnHeadlineSelectedListener onHeadlineSelectedListener;
    private EditText editText;
    private Button button;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment, container, false);
        editText = (EditText) view.findViewById(R.id.editText);

        button = (Button) view.findViewById(R.id.btnFirstFragment);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onHeadlineSelectedListener.sendText(editText.getText().toString());

            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onHeadlineSelectedListener = (OnHeadlineSelectedListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onHeadlineSelectedListener =null;
    }
}
