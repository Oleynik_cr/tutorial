package com.example.android.day4task1;

/**
 * Created by android on 8/13/15.
 */
public interface OnHeadlineSelectedListener {

    public void sendText(String message);
}
