package com.example.android.day4task1;


import android.app.FragmentTransaction;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


import com.example.android.day4task1.fragment.FirstFragment;
import com.example.android.day4task1.fragment.SecondFragment;

public class SecondActivity extends FragmentActivity implements OnHeadlineSelectedListener {



    private SecondFragment secondFragment;
    private FirstFragment  firstFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        secondFragment = new SecondFragment();
        firstFragment  = new FirstFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment1, firstFragment);

        fragmentTransaction.add(R.id.fragment2, secondFragment);
        fragmentTransaction.commit();



    }


   @Override
    public void sendText(String message) {
        secondFragment.setMessage(message);
    }
}

