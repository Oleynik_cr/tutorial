package com.example.android.day4task1;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;


import com.example.android.day4task1.fragment.SecondFragment;

public class MainActivity extends FragmentActivity implements OnHeadlineSelectedListener {



    SecondFragment secondFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       secondFragment = (SecondFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment2);
    }



    @Override
    public void sendText(String message) {
        secondFragment.setMessage(message);
    }
}
